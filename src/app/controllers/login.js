const bcrypt = require('bcrypt')
const uuidv4 = require('uuid/v4')
module.exports = function (userModel, pool){
    return{
        async checkLogin (ctx, next){
            const email = ctx.request.body.email
            const password = ctx.request.body.password
            const profilePictureUrl = []
            const [rows, field] = await pool.query('SELECT * FROM user where email =?',[email])
            if(!rows[0]){
              ctx.status = 400
              ctx.body = {status: 'wrong email'}
            }
            else{
              const comparePassword = await bcrypt.compare(password, rows[0].password)
                if(comparePassword){
                  ctx.session.userId = rows[0].id
                  const userId =  ctx.session.userId 
                     //profile user
                     const [rows3, fields3] = await pool.query(`SELECT 
                                                          u.id, u.email, u.firstname, u.lastname, u.birthday, u.gender, u.username,
                                                          COALESCE(p.bio, ' ') as bio
                                                                FROM user u
                                                                LEFT JOIN user_profile p
                                                                ON u.id = p.user_id
                                                                where u.id = ?`,[userId] )
                     //เลือกรูปโปรไฟล์ล่าสุด                                           
                    const [rows4, field]  = await pool.query(`SELECT profile_picture_url FROM  user_profile_picture
                                                              WHERE user_id =? 
                                                              AND id IN ( SELECT MAX(id) FROM  user_profile_picture
                                                                            GROUP BY user_id
                                                                          ) `,[userId]) 
                    if(!rows4[0]){ //ถ้าไม่มีรูปโปรไฟล์ ให้ใส่ค่าว่าง
                       profilePictureUrl.push(' ')
                    }
                    else{
                      profilePictureUrl.push(rows4[0].profile_picture_url)
                    } 
                   

                  ctx.status = 200                                            
                   ctx.body = { status: 'logged in',
                     id: userId, email: rows3[0].email, firstname: rows3[0].firstname, lastname: rows3[0].lastname,
                     birthday: rows3[0].birthday, gender: rows3[0].gender, username: rows3[0].username, 
                     profile_picture_url: profilePictureUrl[0] ,bio: rows3[0].bio
                   } 
                  // console.log(ctx.body)                                            
                }
                else{
                    ctx.status = 400
                    ctx.body = {status: 'wrong password'}
                }

            }
          }
    }
}