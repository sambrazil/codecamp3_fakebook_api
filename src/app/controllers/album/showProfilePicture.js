module.exports = function (userModel, pool){
    return{
        async showProfilePicture (ctx, next){
           const userId = ctx.session.userId
           const startRow = ctx.request.body.start_row
           const checkPerPage = ctx.request.body.per_page

           const publicProfilePictureTemp = require('../views/view_album/select_album/public_profile_picture')
           const publicProfilePictureController = publicProfilePictureTemp(null, pool)
           await publicProfilePictureController.publicProfilePicture(ctx, next, userId, startRow, checkPerPage)
        }
    }
}