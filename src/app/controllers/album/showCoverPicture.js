module.exports = function (userModel, pool){
    return{
        async showCoverPicture (ctx, next){
          
           const userId = ctx.session.userId
           const startRow = ctx.request.body.start_row
           const checkPerPage = ctx.request.body.per_page
            
           const publicCoverPictureTemp = require('../views/view_album/select_album/public_cover_picture')
           const publicCoverPictureController = publicCoverPictureTemp(null, pool)
           await publicCoverPictureController.publicCoverPicture(ctx, next, userId, startRow, checkPerPage)
        }
    }
}