module.exports = function (userModel, pool){
    return{
        async viewCoverPicture (ctx, next){
            const username = ctx.params.username
            const startRow = ctx.params.start_row
            const checkPerPage = ctx.params.per_page
            //ดึงค่า user_id
            const [checkUsername] = await pool.query(` SELECT id FROM user WHERE username =? `,[username])
            if(!checkUsername[0]){
                ctx.status = 400
                ctx.body = {status: 'not found username'}
                return
            }
            const userId = checkUsername[0].id
            const publicCoverPictureTemp = require('./select_album/public_cover_picture')
            const publicCoverPictureController = publicCoverPictureTemp(null, pool)
            await publicCoverPictureController.publicCoverPicture(ctx, next, userId, startRow, checkPerPage)
        }
    }
}