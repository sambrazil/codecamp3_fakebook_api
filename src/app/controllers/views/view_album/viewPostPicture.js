module.exports = function (userModel, pool){
    return{
        async viewPostPicture (ctx, next){
            
            const targetUsername = ctx.params.username
            const startRow =  ctx.params.start_row
            const perPage = ctx.params.per_page
            const [checkTargetUsername] = await pool.query(` SELECT id FROM user WHERE username =? `,[targetUsername])
            if(!checkTargetUsername[0]){
                ctx.status = 400
                ctx.body = {status: 'not found target username'}
                return
            }
            const targetUserId = checkTargetUsername[0].id
            const publicPostPictureTemp = require('./select_album/public_post_picture') 
            const publicPostPictureController = publicPostPictureTemp(null, pool, targetUserId, startRow, perPage)
            await publicPostPictureController.publicPostPicture(ctx, next) 
        }
    }
}