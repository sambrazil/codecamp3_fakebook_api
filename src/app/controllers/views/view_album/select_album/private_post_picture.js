module.exports = function (userModel, pool, userId, startRow, checkPerPage){
    return{
     async privatePostPicture (ctx, next){
        const perPage = []

        if(startRow < 1){
            ctx.status = 400
            ctx.body = {status: 'wrong row start. It begin with 1'}
            return 
        }
    
        const [rows2, fields2] = await pool.query(` SELECT id FROM feed_post WHERE user_id =? AND ( privacy =? OR privacy =? ) 
                                                     AND picture_url LIKE ?`
                                                   ,[userId, 'public', 'friend', '/%']) //check ว่ามี post picture ไหม
        if(!rows2[0]){
            ctx.status = 400
            ctx.body = {status: 'not found post picture'}
            return
        }
        const rowNumber = rows2.length  // row number
        const offsetStart = parseInt( (startRow - 1) ) // use in db query only begin with index 0
        const restRow = rowNumber - (startRow - 1)
         if (restRow < checkPerPage){
            perPage.push( parseInt(restRow))
        }      
       else if (restRow >= checkPerPage){
        perPage.push( parseInt(checkPerPage))
       }
       const [rows3, fields3] = await pool.query(`SELECT f.id as post_id, f.picture_url, f.caption, f.created_at, f.likes, f.privacy,
                                                 u.username, u.firstname, u.lastname, p.profile_picture_url
                                                 FROM feed_post f
                                                 INNER JOIN user u
                                                 ON f.user_id = u.id
                                                 LEFT JOIN user_profile_picture p
                                                 on u.id = p.user_id
                                                 WHERE ( p.id IN ( SELECT MAX(id) FROM  user_profile_picture
                                                                   GROUP BY user_id                      
                                                                  )
                                                         OR p.id IS NULL
                                                        )
                                                AND u.id = ?
                                                AND 
                                                ( privacy =? OR privacy =? )
                                                AND f.picture_url LIKE ?          
                                                ORDER BY f.id DESC  
                                                LIMIT ? OFFSET ? `,[userId, 'public', 'friend', '/%', perPage[0], offsetStart])
        if(!rows3[0]){
        ctx.status = 400
        ctx.body = {status: 'not found post picture'}
        return
        }
                  // นับจำนวน comment ใน post
         const [rows4, field4] = await pool.query(` SELECT p.id AS post_id ,COUNT(c.id) AS comment_number
                                                    FROM feed_post p
                                                    LEFT JOIN feed_comment c
                                                    ON p.id = c.post_id
                                                    WHERE p.user_id = ?
                                                    AND 
                                                    ( privacy = ? OR privacy = ? )
                                                    AND p.picture_url LIKE ?
                                                    GROUP BY p.id
                                                    ORDER BY p.id DESC
                                                    LIMIT ? OFFSET ?`,[userId,'public', 'friend','/%', perPage[0], offsetStart])
        if(!rows4[0]){
         ctx.status = 400
         ctx.body = {status: 'not found post picture'}
         return
          }
        const [lastestPostPic] = await pool.query(` SELECT picture_url FROM feed_post 
                                                    WHERE id IN ( SELECT MAX(id) FROM feed_post 
                                                                    WHERE picture_url LIKE ?
                                                                    AND ( privacy = ? OR privacy = ? )
                                                                    GROUP BY user_id
                                                                )
                                                   AND user_id =?
                                                    `,
                                                  ['/%', 'public', 'friend',userId ])
       if(!lastestPostPic[0]){
       ctx.status = 400
       ctx.body = {status: 'not found post picture '}
        return
        }   

       ctx.body = {  row_number: rowNumber, lastest_post_picture_url: lastestPostPic[0].picture_url, post_picture_album: rows3, comment: rows4 }        
    }
  }
}