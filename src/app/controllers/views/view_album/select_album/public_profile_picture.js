module.exports = function (userModel, pool){
    return{
        async publicProfilePicture (ctx, next, userId, startRow, checkPerPage){
            const perPage = []

        if(startRow < 1){
            ctx.status = 400
            ctx.body = {status: 'wrong row start. It begin with 1'}
            return 
        }
    
        const [rows2, fields2] = await pool.query(` SELECT id FROM user_profile_picture WHERE user_id =?`
                                                   ,[userId]) //check ว่ามีรูป profileไหม ไหม
        if(!rows2[0]){
            ctx.status = 400
            ctx.body = {status: 'not found profile picture'}
            return
        }
        const rowNumber = rows2.length  // row number
        const offsetStart = parseInt( (startRow - 1) ) // use in db query only begin with index 0
        const restRow = rowNumber - (startRow - 1)
         if (restRow < checkPerPage){
            perPage.push( parseInt(restRow))
        }      
       else if (restRow >= checkPerPage){
        perPage.push( parseInt(checkPerPage))
       }
       const [rows3, fields3] = await pool.query(` SELECT p.id AS picture_id, u.username, u.firstname, u.lastname, p.profile_picture_url, 
                                                   p.created_at
                                                   FROM user u
                                                   INNER JOIN user_profile_picture p
                                                   ON u.id = p.user_id
                                                   WHERE u.id = ? 
                                                   ORDER BY p.id DESC
                                                   LIMIT ? OFFSET ? `,
                                                [userId, perPage[0], offsetStart])
        if(!rows3[0]){
        ctx.status = 400
        ctx.body = {status: 'not found profile picture'}
        return
        }
     const [currentProfile] = await pool.query(` SELECT profile_picture_url FROM user_profile_picture 
                                                  WHERE id IN ( SELECT MAX(id) FROM user_profile_picture 
                                                                 GROUP BY user_id
                                                               )
                                                        AND user_id =? `,
                                                  [userId])
        if(!currentProfile[0]){
            ctx.status = 400
           ctx.body = {status: 'not found profile picture'}
           return
        } 
      ctx.body = {row_number: rowNumber, current_profile_picture_url: currentProfile[0].profile_picture_url,
                  profile_picture_album : rows3 }                                             

        }
    }
}