module.exports = function (userModel, pool){
    return{
        async viewProfilePicture (ctx, next){
            const username = ctx.params.username
            const startRow = ctx.params.start_row
            const checkPerPage = ctx.params.per_page
            //ดึงค่า user_id
            const [checkUsername] = await pool.query(` SELECT id FROM user WHERE username =? `,[username])
            if(!checkUsername[0]){
                ctx.status = 400
                ctx.body = {status: 'not found username'}
                return
            }
            const userId = checkUsername[0].id
            const publicProfliePictureTemp = require('./select_album/public_profile_picture')
            const publicProfilePictureController = publicProfliePictureTemp(null, pool)
            await publicProfilePictureController.publicProfilePicture(ctx, next, userId, startRow, checkPerPage)
        }
    }
}