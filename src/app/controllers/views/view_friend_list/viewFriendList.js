module.exports = function (userModel, pool){
    return{
      async viewFriendList (ctx, next){

          const targetUsername = ctx.params.username
          const startRow = ctx.params.start_row
          const checkPerPage = ctx.params.per_page 
        // console.log(targetUsername)
         // console.log(startRow)
          // console.log(checkPerPage)
          const [checkTargetUsername] = await pool.query(` SELECT id FROM user WHERE username =? `,[targetUsername])
          if(!checkTargetUsername[0]){
            ctx.status = 400
            ctx.body = {status: 'not found target username'}
            return
          }
         const targetUserId = checkTargetUsername[0].id
                                       
         const [checkPrivacy] = await pool.query(` SELECT privacy FROM friend_list_setting WHERE user_id =? `
                                                   ,[targetUserId])
         const friendPrivacy = checkPrivacy[0] ? checkPrivacy[0].privacy : 'public'
        // console.log(friendPrivacy)
         if( friendPrivacy === 'only me' || friendPrivacy === 'friend'){
             // only me ดูได้แค่เจ้าของ , friend ดูได้แค่คนที่เป็นเพื่อน
            ctx.status = 400
            ctx.body = {status: 'not found friend list'}
         }
        
        else if (friendPrivacy === 'public'){
            //public ดูได้ทุกคน
          const friendListTemp = require('./select_friend_list/friend_list')
          const friendListController = friendListTemp(null, pool, targetUserId, startRow, checkPerPage) 
          await friendListController.friendList(ctx, next) 
        }
       
      }
    }
}