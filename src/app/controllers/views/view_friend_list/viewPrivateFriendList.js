module.exports = function (userModel, pool){
    return{
      async viewPrivateFriendList (ctx, next){
          const userId = ctx.session.userId
          const targetUsername = ctx.request.body.username
          const startRow = ctx.request.body.start_row
          const checkPerPage = ctx.request.body.per_page 
         // console.log(targetUsername)
          // console.log(startRow)
          // console.log(checkPerPage)
          const [checkTargetUsername] = await pool.query(` SELECT id FROM user WHERE username =? `,[targetUsername])
          if(!checkTargetUsername[0]){
            ctx.status = 400
            ctx.body = {status: 'not found target username'}
            return
          }
         const targetUserId = checkTargetUsername[0].id

         if( userId === targetUserId){
            ctx.status = 400
            ctx.body = {status: 'can not put yourself username'}
            return
         }
         // เช็คว่าเป็นเพื่อนกันไหม
         const [checkFriend] = await pool.query(` SELECT id FROM friend_list WHERE user_id =? AND friend_user_id =? `
                                                ,[userId, targetUserId])
         const friendStatus = checkFriend[0] ? 'yes' : 'no'                                       
         
         const [checkPrivacy] = await pool.query(` SELECT privacy FROM friend_list_setting WHERE user_id =? `
                                                   ,[targetUserId])
         const friendPrivacy = checkPrivacy[0] ? checkPrivacy[0].privacy : 'public'
         console.log(friendStatus)
         console.log(friendPrivacy)
         if( friendPrivacy === 'only me'){
             // only me ดูได้แค่เจ้าของ
            ctx.status = 400
            ctx.body = {status: 'not found friend list'}
         }
        else if( friendPrivacy === 'friend' && friendStatus === 'no'){
            // friend ดูได้เฉพาะเพื่อน ไม่ใช่เพื่อนเลยดูไม่ได้
            ctx.status = 400
            ctx.body = {status: 'not found friend list'}
        }
        else if (friendPrivacy === 'public'){
            //public ดูได้ทุกคน
          const friendListTemp = require('./select_friend_list/friend_list')
          const friendListController = friendListTemp(null, pool, targetUserId, startRow, checkPerPage) 
          await friendListController.friendList(ctx, next) 
        }
       else if (friendPrivacy === 'friend' && friendStatus === 'yes'){
           // friend ดูได้เฉพาะเพื่อน เป็นเพื่อนกันดูได้
           const friendListTemp = require('./select_friend_list/friend_list')
           const friendListController = friendListTemp(null, pool, targetUserId, startRow, checkPerPage) 
           await friendListController.friendList(ctx, next) 
       }

      }
    }
}