module.exports = function (userModel, pool, userId, postId){
    return{
     async privatePostById (ctx, next){
        
        const [rows2, fields2] = await pool.query(` SELECT id FROM feed_post WHERE user_id =? AND ( privacy =? OR privacy =? ) 
                                                                             AND id =? `
                                                   ,[userId, 'public', 'friend', postId]) //check ว่ามี post ไหม
        if(!rows2[0]){
            ctx.status = 400
            ctx.body = {status: 'not found post'}
            return
        }
      
       const [rows3, fields3] = await pool.query(`SELECT f.id as post_id, f.picture_url, f.caption, f.created_at, f.likes, f.privacy,
                                                 u.username, u.firstname, u.lastname, p.profile_picture_url
                                                 FROM feed_post f
                                                 INNER JOIN user u
                                                 ON f.user_id = u.id
                                                 LEFT JOIN user_profile_picture p
                                                 on u.id = p.user_id
                                                 WHERE ( p.id IN ( SELECT MAX(id) FROM  user_profile_picture
                                                                   GROUP BY user_id                      
                                                                  )
                                                         OR p.id IS NULL
                                                        )
                                                AND u.id = ?
                                                AND 
                                                ( privacy =? OR privacy =? )
                                                AND f.id =?           
                                                ORDER BY f.id DESC  
                                                `,[userId, 'public', 'friend', postId])
        if(!rows3[0]){
        ctx.status = 400
        ctx.body = {status: 'not found post'}
        return
        }
                  // นับจำนวน comment ใน post
         const [rows4, field4] = await pool.query(` SELECT p.id AS post_id ,COUNT(c.id) AS comment_number
                                                    FROM feed_post p
                                                    LEFT JOIN feed_comment c
                                                    ON p.id = c.post_id
                                                    WHERE p.user_id = ?
                                                    AND 
                                                    ( 
                                                      privacy = ? OR privacy = ? 
                                                     ) 
                                                      AND p.id =?
                                                    GROUP BY p.id
                                                    ORDER BY p.id DESC
                                                    `,[userId,'public', 'friend', postId])
        if(!rows4[0]){
         ctx.status = 400
         ctx.body = {status: 'not found post'}
         return
          }
ctx.body = { data: rows3, comment: rows4 }        
    }
  }
}