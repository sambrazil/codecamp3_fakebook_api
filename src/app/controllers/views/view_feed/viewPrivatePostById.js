module.exports = function (userModel, pool){
    return{
        async viewPrivatePostById (ctx, next){
            const userId = ctx.session.userId
            const postId = ctx.request.body.post_id
            const [checkTarget] = await pool.query(`SELECT user_id FROM feed_post WHERE id =?`
                                                    ,[postId])
             if(!checkTarget[0]){
                 ctx.status = 400
                 ctx.body = {status: 'not found post'}
                 return
             }
            const targetUserId = checkTarget[0].user_id
            if(userId == targetUserId){
                ctx.status = 400
                ctx.body = {status:'can not view yourself'}
                return
            }

            //check ว่าเป็นเพื่อนกันไหม, ถ้าไม่เพื่อน ให้ดูได้แค่ public, ถ้าเป็นเพื่อนดูได้แค่ friend กับ public
            const [checkFriend] = await pool.query(` SELECT id FROM friend_list WHERE user_id =? AND friend_user_id =? `
                                                    ,[userId, targetUserId])
           if(!checkFriend[0]){
              const publicPostTemp = require('./select_feed/public_post_by_id')
              const publicPostController = publicPostTemp(null, pool, targetUserId, postId)
              await publicPostController.publicPostById(ctx,next)
           }
           else{
               const privatePostTemp = require('./select_feed/private_post_by_id')
               const privatePostController = privatePostTemp(null, pool, targetUserId, postId)
               await privatePostController.privatePostById(ctx,next)
           }                                         
        
        }
    }
}