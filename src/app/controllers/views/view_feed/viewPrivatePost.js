module.exports = function (userModel, pool){
    return{
        async viewPrivatePost (ctx, next){
            const userId = ctx.session.userId
            const targetUsername = ctx.request.body.username
            const startRow = ctx.request.body.start_row
            const perPage = ctx.request.body.per_page
            const [checkTargetUsername] = await pool.query(` SELECT id FROM user WHERE username =? `,[targetUsername])
            if(!checkTargetUsername[0]){
                ctx.status = 400
                ctx.body = {status: 'not found target username'}
                return
            }
            const targetUserId = checkTargetUsername[0].id
            if(userId == targetUserId){
                ctx.status = 400
                ctx.body = {status:'can not view yourself'}
                return
            }

            //check ว่าเป็นเพื่อนกันไหม, ถ้าไม่เพื่อน ให้ดูได้แค่ public, ถ้าเป็นเพื่อนดูได้แค่ friend กับ public
            const [checkFriend] = await pool.query(` SELECT id FROM friend_list WHERE user_id =? AND friend_user_id =? `
                                                    ,[userId, targetUserId])
           if(!checkFriend[0]){
              const publicPostTemp = require('./select_feed/public_post')
              const publicPostController = publicPostTemp(null, pool, targetUserId, startRow, perPage)
              await publicPostController.publicPost(ctx,next)
           }
           else{
               const privatePostTemp = require('./select_feed/private_post')
               const privatePostController = privatePostTemp(null, pool, targetUserId, startRow, perPage)
               await privatePostController.privatePost(ctx,next)
           }                                         
        
        }
    }
}