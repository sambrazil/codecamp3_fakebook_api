module.exports = function (userModel, pool){
    return{
     async viewPost (ctx, next){
        const username = ctx.params.username 
        const startRow = ctx.params.start_row
        const checkPerPage = ctx.params.per_page
        const perPage = []
        if(username ===''){
            ctx.status = 400
            ctx.body = {status: 'please input username'}
            return 
        } 
        if(startRow < 1){
            ctx.status = 400
            ctx.body = {status: 'wrong row start. It begin with 1'}
            return 
        }
        const [rows] = await pool.query(` SELECT id FROM user WHERE username =? `,[username]) //ดึงค่า user id ของคนที่เราเข้าไปดู
        if(!rows[0]){
            ctx.status = 400
            ctx.body = {status: 'not found username'}
            return 
        }
        const userId = rows[0].id
        const [rows2, fields2] = await pool.query(` SELECT id FROM feed_post WHERE user_id =? AND privacy =? `
                                                   ,[userId, 'public']) //check ว่ามี post ไหม
        if(!rows2[0]){
            ctx.status = 400
            ctx.body = {status: 'not found post'}
            return
        }
        const rowNumber = rows2.length  // row number
        const offsetStart = parseInt( (startRow - 1) ) // use in db query only begin with index 0
        const restRow = rowNumber - (startRow - 1)
         if (restRow < checkPerPage){
            perPage.push( parseInt(restRow))
        }      
       else if (restRow >= checkPerPage){
        perPage.push( parseInt(checkPerPage))
       }
       const [rows3, fields3] = await pool.query(`SELECT f.id as post_id, f.picture_url, f.caption, f.created_at, f.likes, f.privacy,
                                                 u.username, u.firstname, u.lastname, p.profile_picture_url
                                                 FROM feed_post f
                                                 INNER JOIN user u
                                                 ON f.user_id = u.id
                                                 LEFT JOIN user_profile_picture p
                                                 on u.id = p.user_id
                                                 WHERE ( p.id IN ( SELECT MAX(id) FROM  user_profile_picture
                                                                   GROUP BY user_id                      
                                                                  )
                                                         OR p.id IS NULL
                                                        )
                                                AND u.id = ?
                                                AND privacy =?           
                                                ORDER BY f.id DESC  
                                                LIMIT ? OFFSET ? `,[userId, 'public', perPage[0], offsetStart])
        if(!rows3[0]){
        ctx.status = 400
        ctx.body = {status: 'not found post'}
        return
        }
                  // นับจำนวน comment ใน post
         const [rows4, field4] = await pool.query(` SELECT p.id AS post_id ,COUNT(c.id) AS comment_number
                                                    FROM feed_post p
                                                    LEFT JOIN feed_comment c
                                                    ON p.id = c.post_id
                                                    WHERE p.user_id = ?
                                                    AND privacy = ?
                                                    GROUP BY p.id
                                                    ORDER BY p.id DESC
                                                    LIMIT ? OFFSET ?`,[userId,'public',perPage[0], offsetStart])
        if(!rows4[0]){
         ctx.status = 400
         ctx.body = {status: 'not found post'}
         return
          }
ctx.body = {  row_number: rowNumber, data: rows3, comment: rows4 }        
    }
  }
}