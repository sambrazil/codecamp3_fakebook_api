module.exports = function (userModel, pool){
    return{
        async viewPrivateProfile (ctx, next){
            const userId = ctx.session.userId
           const anotherUsername = ctx.request.body.username
           const [checkTarget] = await pool.query(` SELECT id, username, firstname, lastname FROM user WHERE username =? `,[anotherUsername])
           if(!checkTarget[0]){
               ctx.status = 400
               ctx.body = {status: 'not found target username'}
               return
           }
           const targetUsername = checkTarget[0].username
           const tagetFirstname = checkTarget[0].firstname
           const targetLastname = checkTarget[0].lastname
           const targetUserId = checkTarget[0].id
           if( userId == targetUserId){
               ctx.status = 400
               ctx.body = {status: 'can not view yourself'}
               return
           }
           // เช็คว่าเป็นเพื่อนกันไหม ถ้าเป็นให้เลือกข้อมูล plublic friend , ถ้าไม่เป็น ให้เลือกข้อมูล public
           const[checkFriend] = await pool.query(` SELECT id FROM friend_list WHERE user_id =? and friend_user_id =? `
                                                ,[userId, targetUserId])
           if(!checkFriend[0]){
             const publicProfileTemp = require('../view_profile/select_profile/public_profile')
             const publicProfileController = publicProfileTemp(null, pool)
             await publicProfileController.publicProfile(ctx, next, targetUserId, targetUsername, tagetFirstname, targetLastname)
           }
           else{
            const privateProfileTemp = require('../view_profile/select_profile/private_profile')
            const privateProfileController = privateProfileTemp(null, pool)
            await privateProfileController.privateProfile(ctx,next,targetUserId,targetUsername, tagetFirstname, targetLastname) 
           }                                     
        }
    }
}