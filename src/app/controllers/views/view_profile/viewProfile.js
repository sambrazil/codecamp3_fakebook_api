module.exports = function (userModel, pool){
    return {
      async viewProfile (ctx, next){
           const username = ctx.params.username
           //เช็คว่ามี username นี้ไหม 
          const[check] = await pool.query(` SELECT id ,firstname, lastname, username FROM user WHERE username = ?`,[username])
          if(!check[0]){
            ctx.status = 400
            ctx.body = {status: 'not found username'}
            return
          }
          const userId = check[0].id
         //ดึงข้อมูล profile แต่ละค่า ,เฉพาะที่เป็น public
         //1. bio
         const [checkBio] = await pool.query(` SELECT p.bio  
                                               FROM user_profile p 
                                               LEFT JOIN profile_setting s
                                               ON p.user_id = s.user_id
                                               WHERE p.user_id =? 
                                                AND (s.bio =? OR s.bio IS NULL)`
                                                ,[userId,'public'])
       const bio = !checkBio[0] ? '' : checkBio[0].bio
       // 2. lives
       const [checkLives] = await pool.query(` SELECT p.lives  
                                            FROM user_profile p 
                                            LEFT JOIN profile_setting s
                                            ON p.user_id = s.user_id
                                            WHERE p.user_id =? 
                                            AND (s.lives =? OR s.lives IS NULL)`
                                            ,[userId,'public'])
      const lives = !checkLives[0] ? '' : checkLives[0].lives
      // 3. come_from
      const [checkComeFrom] = await pool.query(` SELECT p.come_from  
                                            FROM user_profile p 
                                            LEFT JOIN profile_setting s
                                            ON p.user_id = s.user_id
                                            WHERE p.user_id =? 
                                            AND (s.come_from =? OR s.come_from IS NULL)`
                                            ,[userId,'public'])
      const comeFrom = !checkComeFrom[0] ? '' : checkComeFrom[0].come_from
      // 4. work_place
      const [checkWorkPlace] = await pool.query(` SELECT p.work_place  
                                            FROM user_profile p 
                                            LEFT JOIN profile_setting s
                                            ON p.user_id = s.user_id
                                            WHERE p.user_id =? 
                                            AND (s.work_place =? OR s.work_place IS NULL)`
                                            ,[userId,'public'])
      const workPlace = !checkWorkPlace[0] ? '' : checkWorkPlace[0].work_place
      // 5. education
      const [checkEducation] = await pool.query(` SELECT p.education  
                                            FROM user_profile p 
                                            LEFT JOIN profile_setting s
                                            ON p.user_id = s.user_id
                                            WHERE p.user_id =? 
                                            AND (s.education =? OR s.education IS NULL)`
                                            ,[userId,'public'])
      const education = !checkEducation[0] ? '' : checkEducation[0].education
    // 6. relationship_status
    const [checkRelationshipStatus] = await pool.query(` SELECT p.relationship_status  
                                                FROM user_profile p 
                                                LEFT JOIN profile_setting s
                                                ON p.user_id = s.user_id
                                                WHERE p.user_id =? 
                                                AND (s.relationship_status =? OR s.relationship_status IS NULL)`
                                                ,[userId,'public'])
    const relationshipStatus = !checkRelationshipStatus[0] ? '' : checkRelationshipStatus[0].relationship_status 
    // 7. hobbies
    const [checkHobbies] = await pool.query(` SELECT p.hobbies  
                                             FROM user_profile p 
                                             LEFT JOIN profile_setting s
                                             ON p.user_id = s.user_id
                                             WHERE p.user_id =? 
                                             AND (s.hobbies =? OR s.hobbies IS NULL)`
                                            ,[userId,'public'])
    const hobbies = !checkHobbies[0] ? '' : checkHobbies[0].hobbies
  // 8. birthday
     const [checkBirthday] = await pool.query(` SELECT u.birthday 
                                              FROM user u 
                                              LEFT JOIN profile_setting s
                                              ON  u.id = s.user_id
                                              WHERE u.id =? 
                                              AND (s.birthday =? OR s.birthday IS NULL)`
                                             ,[userId,'public'])
  const birthday = !checkBirthday[0] ? '' : checkBirthday[0].birthday
// 9. email
  const [checkEmail] = await pool.query(` SELECT u.email
                                             FROM user u 
                                             LEFT JOIN profile_setting s
                                             ON  u.id = s.user_id
                                             WHERE u.id =? 
                                             AND (s.email =? OR s.email IS NULL)`
                                             ,[userId,'public'])
const email = !checkEmail[0] ? '' : checkEmail[0].email
// 9. gender
  const [checkGender] = await pool.query(` SELECT u.gender FROM user u WHERE u.id =?` ,[userId,])
  const gender = !checkGender[0] ? '' : checkGender[0].gender
      // รูป profile
     const [profilePic] = await pool.query(`SELECT * FROM user_profile_picture WHERE user_id =? 
                                                     AND id IN ( SELECT MAX(id) FROM user_profile_picture
                                                                    GROUP BY user_id 
                                                                  )`,[userId])
     const profilePicture = !profilePic[0]? ' ' : profilePic[0].profile_picture_url
       //รูป  cover
     const [coverPic] = await pool.query(`SELECT * FROM user_cover_picture WHERE user_id =? 
                                                      AND id IN ( SELECT MAX(id) FROM user_cover_picture
                                                                  GROUP BY user_id 
                                                                 ) `,[userId])
    const coverPicture = !coverPic[0] ? ' ' : coverPic[0].cover_picture_url

    ctx.body = { username: check[0].username, firstname: check[0].firstname, lastname: check[0].lastname 
                  ,bio: bio, lives: lives, come_from: comeFrom, work_place: workPlace, education: education,
                  relationship_status: relationshipStatus, hobbies: hobbies, birthday: birthday, email: email, gender: gender, 
                 profile_picture_url: profilePicture, cover_picture_url:coverPicture
                }                                                             
                                                                  
        }
    }
}