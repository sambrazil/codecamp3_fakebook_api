module.exports = function (userModel, pool){
    return {
        async searchLastname(ctx, next, lastname, startRow, checkPerPage){
            const perPage = []
        
            if(startRow < 1){
                ctx.status = 400
                ctx.body = {status: 'wrong row start. It begin with 1'}
                return  
            }
           const [row] = await pool.query(`SELECT id FROM user WHERE lastname LIKE ?`, lastname)
           if(row[0]){ //ถ้าพบข้อมูลให้เอาค่ามาแสดงตาม limit
            const rowNumber = row.length  // row number
            const offsetStart = parseInt( (startRow - 1) ) // use in db query only begin with index 0
            const restRow = rowNumber - (startRow - 1)
            if (restRow < checkPerPage){
               perPage.push( parseInt(restRow))
                }      
            else if (restRow >= checkPerPage){
           perPage.push( parseInt(checkPerPage))
               }
           
             // หาตาม  lastname 
         const [searchByLastName] = await pool.query(` SELECT u.username, u.firstname, u.lastname, p.profile_picture_url
                                                       FROM user u
                                                       LEFT JOIN user_profile_picture p
                                                       ON u.id = p.user_id
                                                        WHERE 
                                                      ( p.id IN ( SELECT MAX(id) FROM user_profile_picture
                                                                  GROUP BY user_id
                                                                 )
                                                         OR       
                                                        p.id IS NULL
                                                       )
                                                     AND
                                                   u.lastname LIKE ? 
                                                  LIMIT ? OFFSET ? `
                                                   ,[lastname, perPage[0], offsetStart]) 
               if(searchByLastName[0]){ //ถ้าหาเจอให้หยุดการทำงาน
                ctx.body = { row_number:rowNumber ,data: searchByLastName }
                  return 'ok'
                }          
          }
                 return 'fail'
        }
    }
}