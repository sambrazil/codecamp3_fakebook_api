module.exports = function (userModel, pool){
    return {
        async searchFirstname(ctx, next, firstname, startRow, checkPerPage){
            const perPage = []
        
            if(startRow < 1){
                ctx.status = 400
                ctx.body = {status: 'wrong row start. It begin with 1'}
                return  
            }
           const [row] = await pool.query(`SELECT id FROM user WHERE firstname LIKE ?`, firstname)
           if(row[0]){ //มีข้อมูลให้แสดงตาม ที่ limit
            const rowNumber = row.length  // row number
         const offsetStart = parseInt( (startRow - 1) ) // use in db query only begin with index 0
         const restRow = rowNumber - (startRow - 1)
         if (restRow < checkPerPage){
            perPage.push( parseInt(restRow))
             }      
         else if (restRow >= checkPerPage){
        perPage.push( parseInt(checkPerPage))
            }
        
          // หาตาม  firstname 
      const [searchByFirstName] = await pool.query(` SELECT u.username, u.firstname, u.lastname, p.profile_picture_url
                                                    FROM user u
                                                    LEFT JOIN user_profile_picture p
                                                    ON u.id = p.user_id
                                                     WHERE 
                                                   ( p.id IN ( SELECT MAX(id) FROM user_profile_picture
                                                               GROUP BY user_id
                                                              )
                                                      OR       
                                                     p.id IS NULL
                                                    )
                                                  AND
                                                u.firstname LIKE ? 
                                               LIMIT ? OFFSET ? `
                                                ,[firstname, perPage[0], offsetStart]) 
            if(searchByFirstName[0]){
                ctx.body = { row_number:rowNumber ,data: searchByFirstName } //หาเจอแล้ว หยุดการค้นหา
                return 'ok'
             }      
           }
              return 'fail'
        }
    }
}