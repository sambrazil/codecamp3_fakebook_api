module.exports = function (userModel, pool){
    return {
        async searchFullname(ctx, next, fullname, startRow, checkPerPage){
            const perPage = []
        
            if(startRow < 1){
                ctx.status = 400
                ctx.body = {status: 'wrong row start. It begin with 1'}
                return  
            }
           const [row] = await pool.query(`SELECT id FROM user WHERE CONCAT (firstname, lastname) LIKE ?`, fullname)
           if(row[0]){ //ถ้าพบข้อมูลให้แสดงตาม limit
            const rowNumber = row.length  // row number
            const offsetStart = parseInt( (startRow - 1) ) // use in db query only begin with index 0
           const restRow = rowNumber - (startRow - 1)
           if (restRow < checkPerPage){
            perPage.push( parseInt(restRow))
             }      
         else if (restRow >= checkPerPage){
        perPage.push( parseInt(checkPerPage))
            }
        
          // หาตาม full name 
      const [searchByFullName] = await pool.query(` SELECT u.username, u.firstname, u.lastname, p.profile_picture_url
                                                    FROM user u
                                                    LEFT JOIN user_profile_picture p
                                                    ON u.id = p.user_id
                                                     WHERE 
                                                   ( p.id IN ( SELECT MAX(id) FROM user_profile_picture
                                                               GROUP BY user_id
                                                              )
                                                      OR       
                                                     p.id IS NULL
                                                    )
                                                  AND
                                               CONCAT (u.firstname, u.lastname) LIKE ? 
                                               LIMIT ? OFFSET ? `
                                                ,[fullname, perPage[0], offsetStart]) 
            if(searchByFullName[0]){
                ctx.body = { row_number:rowNumber ,data: searchByFullName } //หาข้อมูลเจอแล้ว หยุดการค้นหา
                return 'ok'
             }             
           }
             return 'fail'
        }
    }
}