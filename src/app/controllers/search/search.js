module.exports = function (userModel, pool){
    return {
      async search (ctx, next){
       const startRow = ctx.request.body.start_row
       const checkPerPage = ctx.request.body.per_page
       const fullname = ctx.request.body.fullname 
       const firstname = fullname? fullname.split(' ')[0] : ''
       const checkLastname =  fullname? fullname.split(' ')[1] : ''
       const lastname = checkLastname ? checkLastname : ''
       const concatFullname =  `${firstname}${lastname}`
       const searchFullName = `%${firstname}${lastname}%`
       const searchFirstname =  `%${firstname}%`
       const searchLastname = `%${lastname}%`
       
       //controller
       const userSearchFullnameTemp = require('./select_search/search_fullname')
       const userSearchFullnameController = userSearchFullnameTemp(null, pool)
       const userSearchFirstnameTemp = require('./select_search/search_firstname')
       const userSearchFirstnameController = userSearchFirstnameTemp(null,pool)
       const userSearchLastnameTemp = require('./select_search/search_lastname')
       const userSearchLastnameController = userSearchLastnameTemp(null, pool)

       if( (firstname === '' && lastname ==='')  ||  firstname === ''){
        ctx.status = 400
        ctx.body = {status: 'please in put fullname or firstname'}
        return
       }
       
        //search full name
        
      if( firstname !== '' && lastname !== '' ){   
       // console.log('**************')
       // console.log('0')      
      const resFullname = await userSearchFullnameController.searchFullname(ctx, next, concatFullname, startRow, checkPerPage) //fullname
       if(resFullname === 'ok'){ //status 200 หยุดค้นหา
         return
       }  
      // console.log('1')
      const resFirstname = await userSearchFirstnameController.searchFirstname(ctx, next, firstname, startRow, checkPerPage) // firstname 
       if(resFirstname === 'ok'){
         return
       }
     // console.log('2')
      const resLastname = await userSearchLastnameController.searchLastname(ctx, next, lastname, startRow, checkPerPage) //lastname 
       if(resLastname === 'ok'){
         return
       } 
      // console.log('3')
      const resFullname2 = await userSearchFullnameController.searchFullname(ctx, next, searchFullName, startRow, checkPerPage) // %fullname%
       if(resFullname2 === 'ok'){
         return
       }
     // console.log('4')
      const resFirstname2 = await userSearchFirstnameController.searchFirstname(ctx, next, searchFirstname, startRow, checkPerPage) // %firstname% 
      if(resFirstname2=== 'ok'){
        return
      }
     // console.log('5')
      const resLastname2 = await userSearchLastnameController.searchLastname(ctx, next, searchLastname, startRow, checkPerPage) // %lastname% 
      if(resLastname2 === 'ok'){
        return
      }
      if(resLastname2 === 'fail'){
        ctx.status = 400
        ctx.body = {status: 'not found'}
      }
     // console.log('6')
      
      
      } 
      //search firstname only
     else if ( firstname !== '' && lastname === ''){
      // console.log('**********')
       // console.log('b 0')
      const resFirstname3 = await userSearchFirstnameController.searchFirstname(ctx, next, firstname, startRow, checkPerPage) // firstname 
      if(resFirstname3 === 'ok'){
        return
      }
     // console.log('b 1')
      const resFirstname4 =  await userSearchFirstnameController.searchFirstname(ctx, next, searchFirstname, startRow, checkPerPage) // %firstname% 
      if(resFirstname4 === 'ok'){
        return
      }
      if(resFirstname4 === 'fail'){
        ctx.status = 400
        ctx.body = {status: 'not found'}
      }
      // console.log('b 2')
     }
      }                                   
  }
}