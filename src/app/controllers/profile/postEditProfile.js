const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')
const path = require('path')
module.exports = function (userModel, pool){
    return{
        async postEditProfile(ctx,next){
                const userId = ctx.session.userId
                const profilePicture = ctx.request.files.profile_picture ? ctx.request.files.profile_picture.name : ''
                const coverPicture = ctx.request.files.cover_picture ? ctx.request.files.cover_picture.name : ''
                const bio = ctx.request.body.bio ? ctx.request.body.bio : ''
                const lives = ctx.request.body.lives ? ctx.request.body.lives : '' 
                const comeFrom = ctx.request.body.come_from ? ctx.request.body.come_from : ''
                const workPlace = ctx.request.body.work_place ? ctx.request.body.work_place : ''
                const education = ctx.request.body.education ? ctx.request.body.education : ''
                const relationshipStatus = ctx.request.body.relationship_status ? ctx.request.body.relationship_status : ''
                const hobbies = ctx.request.body.hobbies ? ctx.request.body.hobbies : ''
                
               // add ข้อมูลที่เป็น text ลง DB
                                                                               
              if(bio !== ''){ //1 bio
                const [checkBio] = await pool.query(`SELECT user_id FROM user_profile WHERE user_id =?`,[userId]) 
                if(!checkBio[0]){  //ถ้าไม่เคยใส่ profile (ข้อมูลที่เป็นtext) ให้ insert ลง database
                   
                 const [myBio] = await pool.query(`INSERT INTO user_profile 
                  (user_id, bio, lives, come_from, work_place, education, relationship_status, hobbies)
                  VALUES
                  (?,?,?,?,?,?,?,?)`,[userId, bio, '', '', '', '', '', ''])
                 
                } 
                else{ // ถ้าเคยใส่ profile (ข้อมูลที่เป็นtext) ให้ update ของเก่า
                 const [myBio2] = await pool.query(` UPDATE user_profile SET bio =? WHERE user_id =? `,[bio, userId])   
                }
              }

              if(lives !== ''){ // 2 lives
                const [checkLives] = await pool.query(`SELECT user_id FROM user_profile WHERE user_id =?`,[userId]) 
                if(!checkLives[0]){  //ถ้าไม่เคยใส่ profile (ข้อมูลที่เป็นtext) ให้ insert ลง database
                   
                 const [myLives] = await pool.query(`INSERT INTO user_profile 
                  (user_id, bio, lives, come_from, work_place, education, relationship_status, hobbies)
                  VALUES
                  (?,?,?,?,?,?,?,?)`,[userId, '', lives, '', '', '', '', ''])
                 
                } 
                else{ // ถ้าเคยใส่ profile (ข้อมูลที่เป็นtext) ให้ update ของเก่า
                 const [myLives2] = await pool.query(` UPDATE user_profile SET lives =? WHERE user_id =? `,[lives, userId])   
                }

              }
              
             if(comeFrom !== ''){ // 3 come from
                const [checkComeFrom] = await pool.query(`SELECT user_id FROM user_profile WHERE user_id =?`,[userId]) 
                if(!checkComeFrom[0]){  //ถ้าไม่เคยใส่ profile (ข้อมูลที่เป็นtext) ให้ insert ลง database
                   
                 const [myComeFrom] = await pool.query(`INSERT INTO user_profile 
                  (user_id, bio, lives, come_from, work_place, education, relationship_status, hobbies)
                  VALUES
                  (?,?,?,?,?,?,?,?)`,[userId, '', '', comeFrom, '', '', '', ''])
                 
                } 
                else{ // ถ้าเคยใส่ profile (ข้อมูลที่เป็นtext) ให้ update ของเก่า
                 const [myComeFrom2] = await pool.query(` UPDATE user_profile SET come_from =? WHERE user_id =? `,[comeFrom, userId])   
                }
             }
             
           if(workPlace !== ''){ // 4 work place
            const [checkWorkPlace] = await pool.query(`SELECT user_id FROM user_profile WHERE user_id =?`,[userId]) 
            if(!checkWorkPlace[0]){  //ถ้าไม่เคยใส่ profile (ข้อมูลที่เป็นtext) ให้ insert ลง database
               
             const [myWorkPlace] = await pool.query(`INSERT INTO user_profile 
              (user_id, bio, lives, come_from, work_place, education, relationship_status, hobbies)
              VALUES
              (?,?,?,?,?,?,?,?)`,[userId, '', '', '', workPlace, '', '', ''])
             
            } 
            else{ // ถ้าเคยใส่ profile (ข้อมูลที่เป็นtext) ให้ update ของเก่า
             const [myWorkPlace2] = await pool.query(` UPDATE user_profile SET work_place =? WHERE user_id =? `,[workPlace, userId])   
            }
           } 
         if(education !== ''){ // 5 education
            const [checkEducation] = await pool.query(`SELECT user_id FROM user_profile WHERE user_id =?`,[userId]) 
               if(!checkEducation[0]){  //ถ้าไม่เคยใส่ profile (ข้อมูลที่เป็นtext) ให้ insert ลง database
                  
                const [myEducation] = await pool.query(`INSERT INTO user_profile 
                 (user_id, bio, lives, come_from, work_place, education, relationship_status, hobbies)
                 VALUES
                 (?,?,?,?,?,?,?,?)`,[userId, '', '', '', '', education, '', ''])
                
               } 
               else{ // ถ้าเคยใส่ profile (ข้อมูลที่เป็นtext) ให้ update ของเก่า
                const [myEducation2] = await pool.query(` UPDATE user_profile SET education =? WHERE user_id =? `,[education, userId])   
               }

             }
         
        if(relationshipStatus !== ''){ //6 relationship status
            const [checkRelationshipStatus] = await pool.query(`SELECT user_id FROM user_profile WHERE user_id =?`,[userId]) 
               if(!checkRelationshipStatus[0]){  //ถ้าไม่เคยใส่ profile (ข้อมูลที่เป็นtext) ให้ insert ลง database
                  
                const [myRelationshipStatus] = await pool.query(`INSERT INTO user_profile 
                 (user_id, bio, lives, come_from, work_place, education, relationship_status, hobbies)
                 VALUES
                 (?,?,?,?,?,?,?,?)`,[userId, '', '', '', '', '', relationshipStatus, ''])
                
               } 
               else{ // ถ้าเคยใส่ profile (ข้อมูลที่เป็นtext) ให้ update ของเก่า
                const [myRelationshipStatus2] = await pool.query(` UPDATE user_profile SET relationship_status =? WHERE user_id =? `
                                                                   ,[relationshipStatus,userId])   
               }
           }
        
        if(hobbies !== ''){ // 7 hobbies
            const [checkHobbies] = await pool.query(`SELECT user_id FROM user_profile WHERE user_id =?`,[userId]) 
            if(!checkHobbies[0]){  //ถ้าไม่เคยใส่ profile (ข้อมูลที่เป็นtext) ให้ insert ลง database
               
             const [myHobbies] = await pool.query(`INSERT INTO user_profile 
              (user_id, bio, lives, come_from, work_place, education, relationship_status, hobbies)
              VALUES
              (?,?,?,?,?,?,?,?)`,[userId, '', '', '', '', '', '', hobbies])
             
            } 
            else{ // ถ้าเคยใส่ profile (ข้อมูลที่เป็นtext) ให้ update ของเก่า
             const [myHobbies2] = await pool.query(` UPDATE user_profile SET hobbies =? WHERE user_id =? `,[hobbies, userId])   
            }   

         }    
            
            const dbTime = Date.now()
              // upload รูป   
             if(profilePicture !== ''){ //insertรูป profile ลง database
                  // profile picture  สุ่มชื่อใหม่ และย้ายรูปไปเก็บไว้ใน folder
                const profilePictureName = uuidv4() + dbTime + profilePicture
                const profilePictureFolder = path.join(__dirname,'../../../assets/user_profile') //แก้ path แล้ว
                const profilePicturePath = path.join(profilePictureFolder, profilePictureName)
                await fs.rename(ctx.request.files.profile_picture.path, profilePicturePath)
                const profilePictureUrl = "/user_profile/" + profilePictureName

                const [rows5, fields5] = await pool.query(` INSERT INTO user_profile_picture (user_id, profile_picture_url, created_at)
                                                          VALUES (?,?,?) `,[userId, profilePictureUrl, dbTime])    
             }
             if(coverPicture !== ''){  // insert รูปหน้าปก ลง database
                 // cover picture  สุ่มชื่อใหม่ และย้ายรูปไปเก็บไว้ใน folder
             const coverPictureName = uuidv4() + dbTime + coverPicture
             const coverPictureFolder = path.join(__dirname,'../../../assets/user_cover') //แก้ path แล้ว 
             const coverPicturePath = path.join(coverPictureFolder, coverPictureName)
             await fs.rename(ctx.request.files.cover_picture.path, coverPicturePath)
             const coverPictureUrl = "/user_cover/" + coverPictureName
                const [rows6, fields6] = await pool.query(` INSERT INTO user_cover_picture (user_id, cover_picture_url, created_at)
                                                             VALUES (?,?,?) `,[userId, coverPictureUrl, dbTime])    
                }
                // update compltete  
                ctx.status = 200
                ctx.body = {status: 'update profile complete'}
             
                
            }

    }
}