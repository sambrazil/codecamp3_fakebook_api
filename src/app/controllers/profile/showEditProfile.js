module.exports = function (userModel, pool){
    return{
        async showEditProfile (ctx, next){ 
         
            const userId = ctx.session.userId
            let profilePictureUrl = []
            let coverPictureUrl = []
            let bio = []
            let lives = []
            let comeFrom = []
            let workPlace = []
            let education = []
            let relationshipStatus = []
            let hobbies = []
             
          const [profileData, fields] = await pool.query(`SELECT * FROM user_profile WHERE user_id =?`,[userId])
          
          if(profileData.length > 0){
            bio.push(profileData[0].bio)  
            lives.push(profileData[0].lives)
            comeFrom.push(profileData[0].come_from)
            workPlace.push(profileData[0].work_place) 
            education.push(profileData[0].education)
            relationshipStatus.push(profileData[0].relationship_status)
            hobbies.push(profileData[0].hobbies)  
          }
          else{
            bio.push(' ')  
            lives.push(' ')
            comeFrom.push(' ')
            workPlace.push(' ') 
            education.push(' ')
            relationshipStatus.push(' ')
            hobbies.push(' ') 

          }
        
          
          const [profilePic, fields2] = await pool.query(`SELECT * FROM user_profile_picture WHERE user_id =? 
                                                          AND id IN ( SELECT MAX(id) FROM user_profile_picture
                                                                       GROUP BY user_id 
                                                                     )`,[userId])
        if(profilePic.length > 0){
            profilePictureUrl.push(profilePic[0].profile_picture_url)
        }
        else{

            profilePictureUrl.push(' ')  
        }                                            
         
        const [coverPic, fields3] = await pool.query(`SELECT * FROM user_cover_picture WHERE user_id =? 
                                                      AND id IN ( SELECT MAX(id) FROM user_cover_picture
                                                                  GROUP BY user_id 
                                                                 ) `,[userId])
        if(coverPic.length > 0){
            coverPictureUrl.push(coverPic[0].cover_picture_url)
        }  
        else{
            coverPictureUrl.push(' ')
        }
         
         ctx.body = {
              profile_picture_url: profilePictureUrl[0], cover_picture_url: coverPictureUrl[0],
              bio: bio[0], lives: lives[0], come_from: comeFrom[0], 
              work_place: workPlace[0], education: education[0], 
              relationship_status: relationshipStatus[0], hobbies: hobbies[0]
          }
   
        }
    }
}