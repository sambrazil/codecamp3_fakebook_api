module.exports = function (userModel, pool){
    return{
        async showFeedPostSetting (ctx,next){
          const userId = ctx.session.userId
        // ดึงค่า setting จาก db , ถ้าไม่มีให้ set ค่า default เป็น public
        const [checkPrivacy] = await pool.query( ` SELECT privacy FROM feed_post_setting WHERE user_id =? `
                                                ,[userId])
        if(!checkPrivacy[0]){
            ctx.body = {privacy: 'public'}
            return
        }
        ctx.body = { privacy: checkPrivacy[0].privacy }                                        
      }
    }
}