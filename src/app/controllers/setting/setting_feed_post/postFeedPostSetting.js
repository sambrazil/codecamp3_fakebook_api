module.exports = function (userModel, pool){
    return{
        async postFeedPostSetting (ctx,next){
            const userId = ctx.session.userId
            const privacy = ctx.request.body.privacy
            if( privacy !== 'public' && privacy !== 'friend' && privacy !== 'only me'){
                ctx.status = 400
                ctx.body = {status: 'wrong privacy, please input public, friend, only me'}
                return
            }
            // check ว่ามีข้อมูลการ setting ใน db แล้วรึยัง ถ้าไม่มี insert ไป , ถ้ามีแล้วให้update
            const [check] = await pool.query(` SELECT id FROM feed_post_setting WHERE user_id =?`, [userId])
            if(!check[0]){
                const [insertSetting] = await pool.query(` INSERT INTO feed_post_setting (user_id, privacy) VALUES (?,?) `
                                                           ,[userId, privacy])
                ctx.body = {status: 'setting feed post complete'}
                return                                           
            }
               const [updateSetting] = await pool.query(` UPDATE feed_post_setting SET privacy =? WHERE user_id =? `
                                                         ,[privacy, userId])
               ctx.body = {status: 'setting feed post complete'}                                          
        }
    }
}