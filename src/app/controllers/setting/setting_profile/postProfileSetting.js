module.exports = function (userModel, pool){
    return{
        async postProfileSetting (ctx, next){
            const userId = ctx.session.userId
            const bio = ctx.request.body.bio ? ctx.request.body.bio : ''
            const lives = ctx.request.body.lives ? ctx.request.body.lives : ''
            const comeFrom = ctx.request.body.come_from ? ctx.request.body.come_from : ''
            const workPlace = ctx.request.body.work_place ? ctx.request.body.work_place : ''
            const education = ctx.request.body.education ?  ctx.request.body.education : ''
            const relationshipStatus = ctx.request.body.relationship_status ? ctx.request.body.relationship_status : ''
            const hobbies = ctx.request.body.hobbies ? ctx.request.body.hobbies : ''
            const birthday = ctx.request.body.birthday ?  ctx.request.body.birthday : ''
            const email = ctx.request.body.email ? ctx.request.body.email : ''
            //1. bio
            if(bio !== ''){ //ตรวจก่อนว่ามีค่า setting ในdb ไหม ถ้าไม่มีให้ insert เข้าไป , ถ้ามีแล้ว update ของเก่า
            const [checkBio] = await pool.query(` SELECT id FROM profile_setting WHERE user_id =? `,[userId])
               if(!checkBio[0]){
                   const [insertBio] = await pool.query(`INSERT INTO profile_setting (bio,user_id) VALUE (?,?) `,[bio,userId])
               }
               else{
                   const [updateBio] = await pool.query(` UPDATE profile_setting SET bio =? WHERE user_id =? `,[bio, userId])
               }
                
            }
            // 2. lives 
            if(lives !== ''){ //ตรวจก่อนว่ามีค่า setting ในdb ไหม ถ้าไม่มีให้ insert เข้าไป , ถ้ามีแล้ว update ของเก่า
                const [checkLives] = await pool.query(` SELECT id FROM profile_setting WHERE user_id =? `,[userId])
                   if(!checkLives[0]){
                       const [insertLives] = await pool.query(`INSERT INTO profile_setting (lives,user_id) VALUE (?,?) `,[lives,userId])
                   }
                   else{
                       const [updateLives] = await pool.query(` UPDATE profile_setting SET lives =? WHERE user_id =? `,[lives, userId])
                   }
                    
                }
                // 3. come_from 
            if(comeFrom !== ''){ //ตรวจก่อนว่ามีค่า setting ในdb ไหม ถ้าไม่มีให้ insert เข้าไป , ถ้ามีแล้ว update ของเก่า
                const [checkComeFrom] = await pool.query(` SELECT id FROM profile_setting WHERE user_id =? `,[userId])
                   if(!checkComeFrom[0]){
                       const [insertComeFrom] = await pool.query(`INSERT INTO profile_setting (come_from,user_id) VALUE (?,?) `
                                                                  ,[comeFrom, userId])
                   }
                   else{
                       const [updateComeFrom] = await pool.query(` UPDATE profile_setting SET come_from =? WHERE user_id =? `
                                                                   ,[comeFrom, userId])
                   }
                    
                }
            // 4. work_place 
           if(workPlace !== ''){ //ตรวจก่อนว่ามีค่า setting ในdb ไหม ถ้าไม่มีให้ insert เข้าไป , ถ้ามีแล้ว update ของเก่า
            const [checkWorkPlace] = await pool.query(` SELECT id FROM profile_setting WHERE user_id =? `,[userId])
               if(!checkWorkPlace[0]){
                   const [insertWorkPlace] = await pool.query(`INSERT INTO profile_setting (work_place,user_id) VALUE (?,?) `
                                                               ,[workPlace,userId])
               }
               else{
                   const [updateWorkPlace] = await pool.query(` UPDATE profile_setting SET work_place =? WHERE user_id =? `
                                                               ,[workPlace, userId])
               }
                
            }
            // 5. education
            if(education !== ''){ //ตรวจก่อนว่ามีค่า setting ในdb ไหม ถ้าไม่มีให้ insert เข้าไป , ถ้ามีแล้ว update ของเก่า
                const [checkEducation] = await pool.query(` SELECT id FROM profile_setting WHERE user_id =? `,[userId])
                   if(!checkEducation[0]){
                       const [insertEducation] = await pool.query(`INSERT INTO profile_setting (education,user_id) VALUE (?,?) `
                                                                  ,[education,userId])
                   }
                   else{
                       const [updateEducation] = await pool.query(` UPDATE profile_setting SET education =? WHERE user_id =? `
                                                                   ,[education, userId])
                   }
                    
                }
            // 6. relationship_status
            if(relationshipStatus !== ''){ //ตรวจก่อนว่ามีค่า setting ในdb ไหม ถ้าไม่มีให้ insert เข้าไป , ถ้ามีแล้ว update ของเก่า
                const [checkRelationshipStatus] = await pool.query(` SELECT id FROM profile_setting WHERE user_id =? `,[userId])
                   if(!checkRelationshipStatus[0]){
                       const [insertRelationshipStatus] = await pool.query(`INSERT INTO profile_setting 
                                                                         (relationship_status,user_id) VALUE (?,?) `
                                                                           ,[relationshipStatus,userId])
                   }
                   else{
                       const [updateRelationshipStatus] = await pool.query(` UPDATE profile_setting SET relationship_status =? 
                                                                             WHERE user_id =? `
                                                                           ,[relationshipStatus, userId])
                   }
                    
                }
                 // 7. hobbies
            if(hobbies !== ''){ //ตรวจก่อนว่ามีค่า setting ในdb ไหม ถ้าไม่มีให้ insert เข้าไป , ถ้ามีแล้ว update ของเก่า
                const [checkHobbies] = await pool.query(` SELECT id FROM profile_setting WHERE user_id =? `,[userId])
                   if(!checkHobbies[0]){
                       const [insertHobbies] = await pool.query(`INSERT INTO profile_setting (hobbies,user_id) VALUE (?,?) `
                                                                           ,[hobbies,userId])
                   }
                   else{
                       const [updateHobbies] = await pool.query(` UPDATE profile_setting SET hobbies =? 
                                                                             WHERE user_id =? `
                                                                           ,[hobbies, userId])
                   }
                    
                }
            // 8. birthday
            if(birthday !== ''){ //ตรวจก่อนว่ามีค่า setting ในdb ไหม ถ้าไม่มีให้ insert เข้าไป , ถ้ามีแล้ว update ของเก่า
                const [checkBirthday] = await pool.query(` SELECT id FROM profile_setting WHERE user_id =? `,[userId])
                   if(!checkBirthday[0]){
                       const [insertBirthday] = await pool.query(`INSERT INTO profile_setting (birthday,user_id) VALUE (?,?) `
                                                                           ,[birthday,userId])
                   }
                   else{
                       const [updateBirthday] = await pool.query(` UPDATE profile_setting SET birthday =? 
                                                                             WHERE user_id =? `
                                                                           ,[birthday, userId])
                   }
                    
                }
              // 9. email
            if( email !== ''){ //ตรวจก่อนว่ามีค่า setting ในdb ไหม ถ้าไม่มีให้ insert เข้าไป , ถ้ามีแล้ว update ของเก่า
                const [checkEmail] = await pool.query(` SELECT id FROM profile_setting WHERE user_id =? `,[userId])
                   if(!checkEmail[0]){
                       const [insertEmail] = await pool.query(`INSERT INTO profile_setting (email,user_id) VALUE (?,?) `
                                                                           ,[email,userId])
                   }
                   else{
                       const [updateBirthday] = await pool.query(` UPDATE profile_setting SET email =? 
                                                                             WHERE user_id =? `
                                                                           ,[email, userId])
                   }
                    
                }
                ctx.body = {status: 'setting privacy complete'}                    

        }
    }
}