module.exports = function (userModel, pool){
    return{
        async showProfileSetting (ctx, next){
           const userId = ctx.session.userId
           const [check] = await pool.query(` SELECT COALESCE(bio, 'public') AS bio , COALESCE(lives, 'public') AS lives, 
                                            COALESCE(come_from, 'public') AS come_from, COALESCE(work_place, 'public') AS work_place, 
                                            COALESCE(education, 'public') AS education, 
                                            COALESCE(relationship_status, 'public') AS relationship_status, 
                                            COALESCE(hobbies, 'public') AS hobbies, COALESCE(birthday, 'public') AS birthday,
                                            COALESCE(email, 'public') AS email 
                                             FROM profile_setting WHERE user_id =? `,[userId]) 
           if(!check[0]){ //ถ้าไม่พบการตั้งค่า ให้ตั้งค่า default เป็น public
              ctx.body = {  bio: 'public', lives: 'public', come_from: 'public', work_place: 'public', education: 'public',
                            relationship_status: 'public', hobbies: 'public', birthday: 'public', email: 'public'
                         }
              return            
           }
           
           ctx.body =  {  bio: check[0].bio,  lives: check[0].lives, come_from: check[0].come_from, work_place: check[0].work_place, 
                           education: check[0].education, relationship_status: check[0].relationship_status, hobbies: check[0].hobbies,
                           birthday: check[0].birthday, email: check[0].email
                       }

        }
    }
}


