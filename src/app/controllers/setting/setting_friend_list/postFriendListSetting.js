module.exports = function (userModel, pool){
    return {
     async postFriendListSetting (ctx, next){
    const userId = ctx.session.userId
     const privacy = ctx.request.body.privacy
     if( privacy !== 'public' && privacy !== 'friend' && privacy !== 'only me'){
         ctx.status = 400
         ctx.body = { status: ' wrong privacy, please input public, friend, only me'}
         return
     } 
     // check ว่ามีข้อมูลใน dbไหม ถ้าไม่มีให้insert , มีแล้วให้ update
     const [check] =await pool.query(` SELECT id FROM friend_list_setting WHERE user_id =? `,[userId])
     if(!check[0]){
       const [insertData] = await pool.query(`INSERT INTO friend_list_setting (user_id, privacy) VALUES (?,?)`
                                              ,[userId, privacy])
       ctx.body = {status: 'setting friend list privacy complete'}
       return                                       
     }
     const [updateData] = await pool.query(` UPDATE friend_list_setting SET privacy =? WHERE user_id =? `,[privacy, userId])
     ctx.body = {status: 'setting friend list privacy complete'}
    }

   }
}