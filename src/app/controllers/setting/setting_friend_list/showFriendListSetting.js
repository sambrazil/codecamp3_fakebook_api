module.exports = function (userModel, pool){
    return {
     async showFriendListSetting (ctx, next){
           const userId = ctx.session.userId
           //check ว่ามีค่า setting ในdb ไหม , ถ้าไม่มีให้แสดงค่า default เป็น public
           const [check] = await pool.query(` SELECT privacy FROM friend_list_setting WHERE user_id =? `,[userId])
           if(!check[0]){
               ctx.body = {privacy: 'public'}
               return
           }
           ctx.body = {privacy: check[0].privacy}
     }        
  }
}