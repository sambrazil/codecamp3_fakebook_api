const path = require('path')
const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')

module.exports = function (userModel, pool){
    return{
       async createFeedPost (ctx, next){
            const userId = ctx.session.userId
            const picture = ctx.request.files.picture ? ctx.request.files.picture.name : ''
            const caption = ctx.request.body.caption
            const privacy = ctx.request.body.privacy
            
            if(privacy !== 'public' && privacy !== 'friend' && privacy !== 'only me'){
                ctx.status = 400
                ctx.body = {status: 'wrong privacy, please in put friend, public, only me'}
                return
            }        
        const dbTime = Date.now()

            if(picture === '' && caption === ''){
                ctx.status = 400
                ctx.body = {status : 'please input caption or picture'}
            }
            else if(picture === '' && caption !== ''){
            const [rows, fields] = await pool.query(`INSERT INTO feed_post (user_id, caption, picture_url, created_at, likes, privacy) 
                                                   VALUES (?,?,?,?,?,?)`,[userId, caption, '', dbTime, '0',privacy])
                 ctx.body = {status: 'post caption complete'}
            }
                
            else if(picture !== '' && caption === ''){
               const pictureName = uuidv4() + dbTime + picture
               const pictureFolder = path.join(__dirname, '../../../assets/feed_post') //แก้ path แล้ว
               const pictureDir = path.join(pictureFolder, pictureName)
               await fs.rename(ctx.request.files.picture.path, pictureDir)
               const pictureUrl = '/feed_post/' + pictureName

                const [rows2, fields2] = await pool.query(`INSERT INTO feed_post(user_id, caption, picture_url, created_at, likes, privacy) 
                VALUES (?,?,?,?,?,?)`,[userId, '', pictureUrl, dbTime, '0',privacy])
                 ctx.body = {status: 'post picture complete'}
            }
          
         else if (picture !== '' && caption !== ''){
            const pictureName = uuidv4() + dbTime + picture
            const pictureFolder = path.join(__dirname, '../../../assets/feed_post') //แก้ path แล้ว
            const pictureDir = path.join(pictureFolder, pictureName)
            await fs.rename(ctx.request.files.picture.path, pictureDir)
            const pictureUrl = '/feed_post/' + pictureName

             const [rows3, fields3] = await pool.query(`INSERT INTO feed_post (user_id, caption, picture_url, created_at, likes, privacy) 
             VALUES (?,?,?,?,?,?)`,[userId, caption, pictureUrl, dbTime, '0', privacy])
              ctx.body = {status: 'post caption and picture complete'}
           
        }
     }
    }
}