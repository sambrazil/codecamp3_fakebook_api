const path = require('path')
const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')
module.exports = function (userModel, pool){
    return{
        async removeComment (ctx, next){
            const commentId = ctx.request.body.comment_id
            const username = ctx.request.body.username
            const oldPictureUrl = ctx.request.body.picture_url
            const  oldPictureSlash = oldPictureUrl.substr(0,1) 
            const [checkUsername] = await pool.query(` SELECT id FROM user WHERE username =? `,[username]) // ดึงค่า user id
            if(!checkUsername[0]){
                ctx.status = 400
                ctx.body = {status : 'not found username'}
                return
            }
            const userId = checkUsername[0].id
           const [checkComment] = await pool.query(` SELECT id FROM feed_comment WHERE id =? AND user_id =? `
                                                    ,[commentId, userId]) // check ว่ามี comment ไหม 
           if(!checkComment[0]){
            ctx.status = 400
            ctx.body = {status : 'not found comment'}
            return
           } 
          
           const [removeComment] = await pool.query(` UPDATE feed_comment SET picture_url =?, comment =?, created_at =?, likes =? 
                                                      WHERE id =? AND user_id =? `,['','', '','', commentId, userId])
           if(oldPictureUrl !== '' &&  oldPictureSlash === '/')
           {  //ลบไฟล์รูปเก่า
             const subStringUrl = oldPictureUrl.substr(1, oldPictureUrl.lenght)
             const oldPictureDir = path.join(__dirname, '../../../assets')
             const oldPicturePath = path.join(oldPictureDir, subStringUrl)
             await fs.remove(oldPicturePath)
           }                                                     
           ctx.body = {status: 'remove comment complete'}                                                                                   
        }
    }
}