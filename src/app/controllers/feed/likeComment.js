module.exports = function (userModel, pool){
    return{
       async likeComment (ctx, next){
            const userId = ctx.session.userId
           const commentId = ctx.request.body.comment_id
           const status = ctx.request.body.status
           const likeRow = []
           const dbTime = Date.now()
           
              if(status ==='' || commentId ===''){
                  ctx.status = 400
                  ctx.body = {status: 'not found input'}
                  return
              }
              if( status !== 'like' && status !== 'unlike'){
                ctx.status = 400
                ctx.body = {status: 'wrong status'}
                return
              }
              // check comment ว่ามี ใน database ไหม
              const [checkComment] = await pool.query(` SELECT id, user_id, post_id FROM feed_comment WHERE id = ? `,[commentId])
              if(!checkComment[0]){
                  ctx.status = 400
                  ctx.body = {status: 'not found comment'}
                  return
              }
              const postId = checkComment[0].post_id
              const OwnerComment = checkComment[0].user_id  // ownerComment
             ///ดึง user_id
              const [checkPost] = await pool.query(`SELECT user_id FROM feed_post WHERE id =?`,[postId])
                   if(!checkPost[0]){
                        status = 400
                        ctx.body = {status: 'not found post'}
                        return
                   }
             const OwnerPost = checkPost[0].user_id       

              if(status === 'like'){
                  
                  //check ก่อน ว่ามีข้อมูลใน database ไหม , ถ้าไม่มีให้ insert ลง database
               const [checkData] = await pool.query(`SELECT id FROM like_comment WHERE comment_id = ? AND user_id =?`
                                                     ,[commentId, userId]) 
                if(!checkData[0]){
                    const [insertData] = await pool.query(`INSERT INTO like_comment (comment_id, user_id) VALUES (?,?)`
                                                        ,[commentId, userId])

                if(OwnerComment !== userId){  //add notification ของคนอื่น
                  //console.log(OwnerPost)
                  //console.log(OwnerComment)
                const [addNotification] = await pool.query(` INSERT INTO notification
                (type, post_id, comment_id, post_owner_user_id, comment_owner_user_id, action_user_id, created_at) 
                             VALUES
                             (?,?,?,?,?,?,?)`,
                             ['like_comment', postId, commentId, OwnerPost, OwnerComment, userId, dbTime])
                    }        
 
                    ctx.body = {status: 'like', message: 'insert new row in database'}
                }
                else{
                    ctx.status = 400
                    ctx.body = {status: 'already like'}
                }                                       
              }
            else if(status === 'unlike'){
                //check ว่ามีข้อมูลในdatase ไหม ถ้ามีให้ลบออก
            const [checkData2] = await pool.query(`SELECT id FROM like_comment WHERE comment_id =? AND user_id =?`
                                                  ,[commentId, userId])
              if(!checkData2[0]){
                ctx.status = 400
                ctx.body = {status: 'can not unlike'}
              }
              else{
                  const [deleteData] = await pool.query(`DELETE FROM like_comment WHERE comment_id =? AND user_id =?`
                                                         ,[commentId, userId])
                   if(OwnerComment !== userId){
                       const[deleteNotification] = await pool.query(` DELETE FROM notification 
                                                                      WHERE comment_id =? AND action_user_id =? `,
                                                                      [commentId,userId])
                   }                                       
                 ctx.body = {status: 'unlike', message:'delete row in database'}                                        
              }                                       
            } 
               // นับจำนวนlike ตามcomment_id แล้ว update ลง table feed_post
             const [likeCount] = await pool.query(`SELECT id FROM like_comment WHERE comment_id =?`,[commentId])
              if(!likeCount[0]){
                  likeRow.push(parseInt(0))
              }
              else{
                  likeRow.push(parseInt(likeCount.length))
              }
              const [updateData] = await pool.query(` UPDATE feed_comment SET likes =? WHERE id =? `, [likeRow, commentId])              
           
       }
    }
}