module.exports = function (userModel, pool){
    return{
       async checkLikeComment (ctx, next){
          const userId = ctx.session.userId
          const commentId = ctx.request.body.comment_id
          const likeStatus = []
          
              //check comment ว่ามี  ใน database ไหม
          const [checkComment] = await pool.query(` SELECT id FROM feed_comment WHERE id = ? `,[commentId])
          if(!checkComment[0]){
              ctx.status =400
              ctx.body = {status: 'not found comment'}
              return
           }
          //check ว่า user ได้กด like comment ไหม 
         const [checkLike] =  await pool.query(` SELECT id FROM like_comment WHERE user_id = ? AND comment_id =? `,[userId, commentId])
         if(!checkLike[0]){ //ถ้าไม่พบข้อมูล ให้ใส่ unlike ไป
            likeStatus.push('unlike')
         }
         else{ //ถ้ามีข้อมูล ให้ใส่ like
            likeStatus.push('like') 
         }     
         ctx.body = {status : likeStatus[0] }

        }
    }
}