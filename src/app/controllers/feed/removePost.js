const path = require('path')
const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')
module.exports = function (userModel, pool){
    return{
        async removePost (ctx,next){
         const postId = ctx.request.body.post_id
         const username = ctx.request.body.username
         const oldPictureUrl = ctx.request.body.picture_url
         const  oldPictureSlash = oldPictureUrl.substr(0,1) 
         const [checkUsername] = await pool.query(` SELECT id FROM user WHERE username =?`,[username]) //ดึงuser id
         if(!checkUsername[0]){
            ctx.status = 400
            ctx.body = {status: 'not found username'}
            return
         }
         const userId = checkUsername[0].id
         const [checkPost] = await pool.query(` SELECT id FROM feed_post WHERE id = ? AND user_id = ? `
                                               ,[postId, userId]) // เช็คว่ามี post นี้อยู่ไหม ถ้าไม่มี remove ไม่ได้
         if(!checkPost[0]){
            ctx.status = 400
            ctx.body = {status: 'not found post'}
            return 
         } 
         
        const [removePost] = await pool.query(` UPDATE feed_post SET picture_url =?, caption =?, created_at =?, likes =?, privacy =?
                                                WHERE id =? AND user_id =? `,['','','', '', '', postId, userId])  // update เป็นค่าว่าง
      if(oldPictureUrl !== '' &&  oldPictureSlash === '/')
      { //ลบไฟล์รูปเก่า
        const subStringUrl = oldPictureUrl.substr(1, oldPictureUrl.lenght)
       const oldPictureDir = path.join(__dirname, '../../../assets')
       const oldPicturePath = path.join(oldPictureDir, subStringUrl)
       await fs.remove(oldPicturePath)
      }
          ctx.body = {status: 'remove post complete'}                                       
        }

    }
}