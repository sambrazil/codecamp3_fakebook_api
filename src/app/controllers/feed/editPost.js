const path = require('path')
const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')
module.exports = function (userModel, pool){
    return{
        async editPost (ctx, next){
            const postId = ctx.request.body.post_id
            const username = ctx.request.body.username
            const caption = ctx.request.body.caption
            const dbTime = Date.now()
            
           if(caption ==='' ){
                ctx.status = 400
                ctx.body = {status: 'please input data'}
                return
            }
            const [checkUsername] = await pool.query(` SELECT id FROM user WHERE username =? `,[username]) //ดึงuser id
            if(!checkUsername[0]){
                ctx.status = 400
                ctx.body = {status: 'not found username'}
                return
            }
            const userId = checkUsername[0].id
            const [checkPost] = await pool.query(` SELECT id FROM feed_post WHERE id =? AND user_id =? `,[postId, userId]) //เช็คว่ามีpost นี้ไหม
            if(!checkPost[0]){
                ctx.status = 400
                ctx.body = {status: 'not found post'}
                return
            } 
 
           
            const [editPost2] = await pool.query(` UPDATE feed_post SET caption =?, created_at =?
                                                 WHERE id =? AND user_id =?`, [caption, dbTime, postId, userId])
           
            ctx.body = {status: 'edit post complete'}
            
        }
    }
}