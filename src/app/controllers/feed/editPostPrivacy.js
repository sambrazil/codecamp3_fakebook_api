module.exports = function (userModel,pool){
    return {
      async editPostPrivacy (ctx, next){
        const postId = ctx.request.body.post_id  
        const username = ctx.request.body.username
        const privacy = ctx.request.body.privacy
        
        if( privacy !== 'public' && privacy !== 'friend' && privacy !=='only me' ){
            ctx.status = 400
            ctx.body = {status: 'wrong privacy, please input public, friend, only me'}
            return
        }
        //check ว่ามี username นี้ไหม
        const [checkUsername] = await pool.query(` SELECT id FROM user WHERE username =? `,[username])
        if(!checkUsername[0]){
            ctx.status = 400
            ctx.body = {status: 'not found username'}
            return
        }
       const userId = checkUsername[0].id
       //เช็คว่ามีpost นี้ไหม
       const [checkPost] = await pool.query(` SELECT id FROM feed_post WHERE id =? AND user_id =?`
                                            ,[postId, userId])
       if(!checkPost[0]){
           ctx.status = 400
           ctx.body = {status: 'not found post'}
           return
       } 
       const [updatePrivacy] = await pool.query(` UPDATE feed_post SET privacy =? WHERE id =? AND user_id =? `
                                                 ,[privacy, postId, userId])
       ctx.body = {status: 'edit post privacy complete'}
      }
    }
}