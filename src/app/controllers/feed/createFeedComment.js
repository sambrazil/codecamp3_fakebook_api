const path = require('path')
const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')

module.exports = function (userModel, pool){
    return{
       async createFeedComment (ctx, next){
           const userId = ctx.session.userId
           const postId = ctx.request.body.post_id
            const picture = ctx.request.files.picture ? ctx.request.files.picture.name : ''
            const comment = ctx.request.body.comment
            const dbTime = Date.now()
            const [checkPostId] = await pool.query(`SELECT id, user_id FROM feed_post WHERE id =?`,[postId])
            if(!checkPostId[0]){
                ctx.status = 400
                ctx.body = {status: 'not found this post'}
                return
            }
            const OwnerPost = checkPostId[0].user_id 

            if(picture === '' && comment === ''){
                ctx.status = 400
                ctx.body = {status : 'please input comment or picture'}
            }
            else if(picture === '' && comment !== ''){
                const [rows, fields] = await pool.query(`INSERT INTO feed_comment (post_id, user_id, comment, picture_url, created_at, likes) 
                VALUES (?,?,?,?,?,?)`,[postId, userId, comment, '', dbTime, '0'])
                 //add notification
                 const commentId = rows.insertId
                 //  owner comment
                 const [checkOwnerComment] = await pool.query(` SELECT user_id FROM feed_comment 
                                                                WHERE id =? `,[commentId])
                 const ownerComment = checkOwnerComment[0].user_id                                               
                 // console.log(OwnerPost)
                 // console.log(commentId)
                const [addNotification] = await pool.query(` INSERT INTO notification
                          (type, post_id, comment_id, post_owner_user_id, comment_owner_user_id, action_user_id, created_at) 
                                                             VALUES
                                                             (?,?,?,?,?,?,?)`,
                                               ['comment', postId, commentId, OwnerPost, ownerComment, userId, dbTime])
                 ctx.body = {status: 'comment complete'}
            }
                
            else if(picture !== '' && comment === ''){
               const pictureName = uuidv4() + dbTime + picture
               const pictureFolder = path.join(__dirname, '../../../assets/feed_comment')  //แก้path แล้ว
               const pictureDir = path.join(pictureFolder, pictureName)
               await fs.rename(ctx.request.files.picture.path, pictureDir)
               const pictureUrl = '/feed_comment/' + pictureName

                const [rows2, fields2] = await pool.query(`INSERT INTO feed_comment(post_id, user_id, comment, picture_url, created_at, likes) 
                VALUES (?,?,?,?,?,?)`,[postId, userId, '', pictureUrl, dbTime, '0'])
                 //add notification
                 const commentId = rows2.insertId
                 // console.log(OwnerPost)
                 // console.log(commentId)
                 //  owner comment
                 const [checkOwnerComment] = await pool.query(` SELECT user_id FROM feed_comment 
                                                                WHERE id =? `,[commentId])
                 const ownerComment = checkOwnerComment[0].user_id      
                const [addNotification] = await pool.query(` INSERT INTO notification
                            (type, post_id, comment_id, post_owner_user_id, comment_owner_user_id, action_user_id, created_at) 
                                                             VALUES
                                                             (?,?,?,?,?,?,?)`,
                                                ['comment', postId, commentId, OwnerPost, ownerComment, userId, dbTime])
                 ctx.body = {status: 'comment complete'}
            }
          
         else if (picture !== '' && comment !== ''){
            const pictureName = uuidv4() + dbTime + picture
            const pictureFolder = path.join(__dirname, '../../../assets/feed_comment') //แก้ว path แล้ว
            const pictureDir = path.join(pictureFolder, pictureName)
            await fs.rename(ctx.request.files.picture.path, pictureDir)
            const pictureUrl = '/feed_comment/' + pictureName

             const [rows3, fields3] = await pool.query(`INSERT INTO feed_comment (post_id, user_id, comment, picture_url, created_at, likes) 
             VALUES (?,?,?,?,?,?)`,[postId, userId, comment, pictureUrl, dbTime, '0'])
              //add notification
              const commentId = rows3.insertId
              // console.log(OwnerPost)
              // console.log(commentId)
              //  owner comment
              const [checkOwnerComment] = await pool.query(` SELECT user_id FROM feed_comment 
                                                         WHERE id =? `,[commentId])
              const ownerComment = checkOwnerComment[0].user_id        
             const [addNotification] = await pool.query(` INSERT INTO notification
                                             (type, post_id, comment_id, post_owner_user_id, comment_owner_user_id, action_user_id, created_at) 
                                                          VALUES
                                                          (?,?,?,?,?,?,?)`,
                                                          ['comment', postId, commentId, OwnerPost, ownerComment, userId, dbTime])
              ctx.body = {status: 'comment complete'}
         
        }
     }
    }
}