module.exports = function (userModel, pool){
    return{
       async likePost (ctx, next){
           const userId = ctx.session.userId
           const dbTime = Date.now()
           const likeRow = []
           const postId = ctx.request.body.post_id
           const status = ctx.request.body.status
              if(status ==='' || postId ===''){
                  ctx.status = 400
                  ctx.body = {status: 'not found input'}
                  return
              }
              if( status !== 'like' && status !== 'unlike'){
                ctx.status = 400
                ctx.body = {status: 'wrong status'}
                return
              }
               // check post ว่ามี ใน database ไหม
               const [checkPost] = await pool.query(` SELECT id, user_id FROM feed_post WHERE id = ? `,[postId])
               if(!checkPost[0]){
                   ctx.status = 400
                   ctx.body = {status: 'not found post'}
                   return
               }
               const OwnerPost = checkPost[0].user_id 
              if(status === 'like'){
                 
                  //check ก่อน ว่ามีข้อมูลใน database ไหม ,ถ้าไม่มีให้ insert ลง database
               const [checkData] = await pool.query(`SELECT id FROM like_post WHERE post_id = ? AND user_id =?`
                                                     ,[postId, userId]) 
                if(!checkData[0]){
                    const [insertData] = await pool.query(`INSERT INTO like_post (post_id, user_id) VALUES (?,?)`
                                                        ,[postId, userId])
                   
                   if(OwnerPost !== userId){ //add notification ที่คนอื่นกด like post
                 // console.log(OwnerPost)
                 // console.log(commentId)
                const [addNotification] = await pool.query(` INSERT INTO notification
                                                (type, post_id, post_owner_user_id, action_user_id, created_at) 
                                                             VALUES
                                                             (?,?,?,?,?)`,
                                                             ['like_post', postId, OwnerPost, userId, dbTime])
                   }
                    ctx.body = {status: 'like', message: 'insert new row in database'}

                }
                else{
                    ctx.status = 400
                    ctx.body = {status: 'already like'}
                }                                       
              }
            else if(status === 'unlike'){
                //check ว่ามีข้อมูลในdatase ไหม ถ้ามีให้ลบออก
            const [checkData2] = await pool.query(`SELECT id FROM like_post WHERE post_id =? AND user_id =?`
                                                  ,[postId, userId])
              if(!checkData2[0]){
                ctx.status = 400
                ctx.body = {status: 'can not unlike'}
              }
              else{
                  const [deleteData] = await pool.query(`DELETE FROM like_post WHERE post_id =? AND user_id =?`
                                                         ,[postId, userId])
                 //ลบ nofiacation ของคนอื่น ถ้ากด unlike
                  if(OwnerPost !== userId){                                        
                  const [deleteNotification] = await pool.query(` DELETE FROM notification 
                                                                  WHERE post_id =? AND action_user_id =? `,
                                                                  [postId, userId]) 
                  }
                 ctx.body = {status: 'unlike', message:'delete row in database'}                                        
              }                                       
            } 
               // นับจำนวนlike ตามpost_id แล้ว update ลง table feed_post
             const [likeCount] = await pool.query(`SELECT id FROM like_post WHERE post_id =?`,[postId])
              if(!likeCount[0]){
                  likeRow.push(parseInt(0))
              }
              else{
                  likeRow.push(parseInt(likeCount.length))
              }
              const [updateData] = await pool.query(` UPDATE feed_post SET likes =? WHERE id =? `, [likeRow, postId])              
           }
    }
}