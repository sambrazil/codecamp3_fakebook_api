const path = require('path')
const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')
module.exports = function (userModel, pool){
    return{
        async editComment (ctx, next){
            const commentId = ctx.request.body.comment_id
            const username = ctx.request.body.username
            const comment = ctx.request.body.comment   
            const dbTime = Date.now()
            
           if(comment ===''){
                ctx.status = 400
                ctx.body = {status: 'please input data'}
                return
            }
            const [checkUsername] = await pool.query(` SELECT id FROM user WHERE username =? `,[username]) //ดึงuser id
            if(!checkUsername[0]){
                ctx.status = 400
                ctx.body = {status: 'not found username'}
                return
            }
            const userId = checkUsername[0].id
            const [checkComment] = await pool.query(` SELECT id FROM feed_comment WHERE id =? AND user_id =? `,[commentId, userId]) //เช็คว่ามีcomment นี้ไหม
            if(!checkComment[0]){
                ctx.status = 400
                ctx.body = {status: 'not found comment'}
                return
            } 
               
               const [editComment2] = await pool.query(` UPDATE feed_comment SET  comment =?, created_at =?
                                                 WHERE id =? AND user_id =?`, [comment, dbTime, commentId, userId])
         
            ctx.body = {status: 'edit comment complete'}
            
        }
    }
}
