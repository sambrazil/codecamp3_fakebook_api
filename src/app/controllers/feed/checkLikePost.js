module.exports = function (userModel, pool){
    return{
       async checkLikePost (ctx, next){
          const userId = ctx.session.userId
          const postId = ctx.request.body.post_id
          const likeStatus = []
          
              //check ว่ามี post ใน database ไหม
          const [checkPost] = await pool.query(` SELECT id FROM feed_post WHERE id = ? `,[postId])
          if(!checkPost[0]){
              ctx.status =400
              ctx.body = {status: 'not found post'}
              return
           }
          //check ว่า user ได้กด like post ไหม 
         const [checkLike] =  await pool.query(` SELECT id FROM like_post WHERE user_id = ? AND post_id =? `,[userId, postId])
         if(!checkLike[0]){ //ถ้าไม่พบข้อมูล ให้ใส่ unlike ไป
            likeStatus.push('unlike')
         }
         else{ //ถ้ามีข้อมูล ให้ใส่ like
            likeStatus.push('like') 
         }     
         ctx.body = {status : likeStatus[0] }

        }
    }
}