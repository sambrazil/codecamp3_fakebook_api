module.exports = function (userModel, pool) {
    return{
        async  showUserFeedComment (ctx,next){
            const postId = ctx.request.body.post_id  //แก้ method เป็น post เอา query string ออก
            const startRow =  ctx.request.body.start_row
            const checkPerPage =  ctx.request.body.per_page
            const perPage = []
            const [rows2, fields2] = await pool.query(`SELECT * FROM feed_comment WHERE post_id =? `,[postId])
            if(!rows2[0]){
                ctx.status = 400
                ctx.body = {status: 'not found comment'}
                return
            }
            const rowNumber = rows2.length  // row number
            const offsetStart = parseInt( (startRow - 1) ) // use in db query only begin with index 0
            const restRow = rowNumber - (startRow - 1)
            if (restRow < checkPerPage){
                perPage.push( parseInt(restRow))
            }      
           else if (restRow >= checkPerPage){
            perPage.push( parseInt(checkPerPage))
           }

          const [rows3, fields3] = await pool.query(`SELECT f.id as comment_id, f.post_id, f.picture_url, f.comment, f.created_at, 
                                                      f.likes, u.username, u.firstname, u.lastname, p.profile_picture_url
                                                     FROM feed_comment  f 
                                                     INNER JOIN user u
                                                     ON f.user_id = u.id
                                                     LEFT JOIN user_profile_picture p
                                                     ON u.id = p.user_id
                                                     WHERE ( p.id IN ( SELECT MAX(id) FROM user_profile_picture
                                                                     GROUP BY user_id
                                                                     )
                                                           OR p.id IS NULL 
                                                           )
                                                           AND f.post_id = ?
                                                           ORDER BY f.id          
                                                      LIMIT ? OFFSET ? `,[postId, perPage[0], offsetStart])
        if(!rows3[0]){
         ctx.status = 400
         ctx.body = {status: 'not found comment'}
        return
         }
         ctx.body = {
             row_number: rowNumber, data: rows3
         }                                                 

        }
    }
}