const bcrypt = require('bcrypt')
module.exports = function (userModel, pool){
    return{

        async  registerUser (ctx, next){
            const firstname = ctx.request.body.firstname
            const lastname = ctx.request.body.lastname
            const email = ctx.request.body.email
            const password = ctx.request.body.password
            const hashedPassword = await bcrypt.hash(password, 10)
            const birthday = ctx.request.body.birthday
            const gender = ctx.request.body.gender
            const checkUsername = firstname + '.' + lastname
            const insertUsername = []
            const d = new Date()
            const time = d.getDate() + '' + d.getMonth() +''+ d.getFullYear() + '' + d.getHours() + '' +d.getMinutes()+ '' + d.getSeconds() + '' + d.getMilliseconds()
            
            if( (firstname==='') || (lastname==='') || (email==='') || (password==='') || (birthday==='') || (gender==='') ){
              ctx.status = 400 
              ctx.body = {status: 'plase input all data'}
              return;
            }

            //check username ว่ามีในระบบไหม
            const [username] = await pool.query(`SELECT id FROM user WHERE username =?`,[checkUsername])
            if(!username[0]){
              insertUsername.push(checkUsername)
            }
            else{
              insertUsername.push(checkUsername + '.' + time)
            }
          
           const [rows, fields] = await pool.query(`SELECT email FROM user WHERE email =? `,[email])
           if(rows[0]){
            ctx.status = 400
            ctx.body ={status: 'email already exit'}
          }
          
           else {
           const sql =`INSERT INTO user(firstname, lastname, email, password, birthday, gender, username) VALUES (?,?,?,?,?,?,?)`
           const [rows2, field2] = await pool.query(sql,[firstname, lastname, email, hashedPassword, birthday, gender, insertUsername[0] ])
           ctx.status = 200
             ctx.body = {
                        status: 'register complete', email:email
                      }
          } 
          } 
    }
}