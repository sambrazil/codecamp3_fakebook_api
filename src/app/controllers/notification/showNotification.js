module.exports = function (userModel, pool) {
    return{
        async  showNotification (ctx,next){
           const userId = ctx.session.userId
            const startRow = ctx.request.body.start_row
            const checkPerPage = ctx.request.body.per_page
            const perPage = []
            const [rows2, fields2] = await pool.query(` SELECT id FROM notification 
                                WHERE ( type =? AND post_owner_user_id =? AND action_user_id NOT IN (?)  )
                                OR    ( type =? AND post_owner_user_id =? AND action_user_id NOT IN (?)  ) 
                                OR    ( type =? AND comment_owner_user_id =? AND action_user_id NOT IN (?) ) `
                                      ,['comment',userId, userId,
                                         'like_post', userId, userId,
                                         'like_comment', userId,userId])
            if(!rows2[0]){
                ctx.status = 400
                ctx.body = {status: 'not found notification'}
                return
            }
            const rowNumber = rows2.length  // row number
            const offsetStart = parseInt( (startRow - 1) ) // use in db query only begin with index 0
            const restRow = rowNumber - (startRow - 1)
            if (restRow < checkPerPage){
                perPage.push( parseInt(restRow))
            }      
           else if (restRow >= checkPerPage){
            perPage.push( parseInt(checkPerPage))
           }

         const [rows3, fields3] = await pool.query(` SELECT n.id, n.post_owner_user_id, n.type, n.post_id, n.comment_id, n.read_status, 
                                                             n.created_at,
                                                              u.username, u.firstname, u.lastname,
                                                              p.profile_picture_url
                                                        FROM notification n
                                                        INNER JOIN user u
                                                        ON n.action_user_id = u.id
                                                        LEFT JOIN user_profile_picture p
                                                        ON u.id = p.user_id
                                                        WHERE
                                                        ( p.id IN ( SELECT MAX(id) FROM user_profile_picture
                                                                     GROUP BY user_id
                                                                   )
                                                            OR
                                                          p.id IS NULL   
                                                        )
                                                AND (

                                                       ( type =? AND post_owner_user_id =? AND action_user_id NOT IN (?)  )
                                                     OR    ( type =? AND post_owner_user_id =? AND action_user_id NOT IN (?) ) 
                                                        OR    ( type =? AND comment_owner_user_id =? AND action_user_id NOT IN (?) )
                                                    )    
                                                       ORDER BY n.id DESC
                                                      LIMIT ? OFFSET ? `
                                                      ,['comment',userId, userId,
                                                      'like_post', userId, userId,
                                                      'like_comment', userId, userId,
                                                       perPage[0], offsetStart])
         if(!rows3[0]){
         ctx.status = 400
         ctx.body = {status: 'not found notification'}
         return
         }                                              
                                                      
          // นับ notifaication_number
          const [rows4, field4] = await pool.query(` SELECT id FROM notification 
                                                      WHERE 
                                                    (  
                                                      ( type =? AND post_owner_user_id =? AND action_user_id NOT IN (?)  )
                                                      OR    ( type =? AND post_owner_user_id =? AND action_user_id NOT IN (?)  ) 
                                                      OR    ( type =? AND comment_owner_user_id =? AND action_user_id NOT IN (?) ) 
                                                     ) 
                                                      AND see_status IS NULL `
                                                      ,['comment',userId, userId,
                                                         'like_post', userId, userId,
                                                         'like_comment', userId,userId])
          const noficationNumber = !rows4[0] ? 0 : rows4.length
         ctx.body = {
             my_user_id: userId, notification_number: noficationNumber, row_number: rowNumber, data: rows3
         }                                                 

        }
    }
}