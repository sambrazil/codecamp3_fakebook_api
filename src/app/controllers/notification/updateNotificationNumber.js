module.exports = function (userModel, pool){
    return{
        async updateNotificationNumber (ctx, next){
          const userId = ctx.session.userId
            // นับ notifaication_number
          const [notification] = await pool.query(` SELECT id FROM notification 
                                                      WHERE 
                                                    (  
                                                      ( type =? AND post_owner_user_id =? AND action_user_id NOT IN (?)  )
                                                      OR    ( type =? AND post_owner_user_id =? AND action_user_id NOT IN (?)  ) 
                                                      OR    ( type =? AND comment_owner_user_id =? AND action_user_id NOT IN (?) ) 
                                                     ) 
                                                      AND see_status IS NULL `
                                                      ,['comment',userId, userId,
                                                         'like_post', userId, userId,
                                                         'like_comment', userId,userId])
           if(!notification[0]){
               ctx.body = {status:'no update notification number is 0 '}
               return
           }

        const [updateNotification] = await pool.query(` UPDATE notification SET see_status =? 
                                                        WHERE 
                                            (  
                                                    ( type =? AND post_owner_user_id =? AND action_user_id NOT IN (?)  )
                                             OR    ( type =? AND post_owner_user_id =? AND action_user_id NOT IN (?)  ) 
                                             OR    ( type =? AND comment_owner_user_id =? AND action_user_id NOT IN (?) ) 
                                            ) 
                                             AND see_status IS NULL `
                                           ,['yes',
                                             'comment',userId, userId,
                                             'like_post', userId, userId,
                                             'like_comment', userId,userId])
          ctx.body = {status: 'notification number update complete'}
        }
    }
}