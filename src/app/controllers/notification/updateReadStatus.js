module.exports = function (userModel, pool){
    return{
        async updateReadStatus(ctx,next){
            const userId = ctx.session.userId
            const notificationId = ctx.request.body.notification_id
            const [checkNotification] = await pool.query(` SELECT read_status FROM notification 
                                                            WHERE 
                                            (  
                                                   ( type =? AND post_owner_user_id =? AND action_user_id NOT IN (?)  )
                                                  OR ( type =? AND post_owner_user_id =? AND action_user_id NOT IN (?)  ) 
                                                  OR ( type =? AND comment_owner_user_id =? AND action_user_id NOT IN (?) ) 
                                            ) 
                                            AND id =? `
                                           ,['comment',userId, userId,
                                             'like_post', userId, userId,
                                             'like_comment', userId,userId,
                                              notificationId])
           if(!checkNotification[0]){
               ctx.status = 400
               ctx.body = {status: 'not found notification'}
               return
           } 
           const readStatus = checkNotification[0].read_status
           if(readStatus === 'yes'){
               ctx.body ={status:'no update read status is yes'}
               return
           }
           const [updateReadStatus] = await pool.query(` UPDATE notification SET read_status =? WHERE id =? `
                                                         ,['yes', notificationId])
              ctx.body = {status: 'read status update complete'}                                                                              
        }
    }
}