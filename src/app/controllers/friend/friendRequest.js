module.exports = function (userModel, pool){
    return{
        async friendRequest (ctx, next){
              const userId = ctx.session.userId
              const responseUsername = ctx.request.body.response_username
              const status = ctx.request.body.status
             //check ค่า form
                if(responseUsername ==='' || status ===''){
                    ctx.status = 400
                    ctx.body = {status: 'please input all data'}  
                    return
                 }
                 if(status !== 'add' && status !== 'cancel'){
                    ctx.status = 400
                    ctx.body = {status: 'input wrong status'}  
                    return
                 }
             const [resUser] = await pool.query(` SELECT id FROM user WHERE username = ? `,[responseUsername])
             if(!resUser[0]){
                ctx.status = 400
                ctx.body = {status: 'not found user'}  
                return
             }
             const resUserId = resUser[0].id // request user id
            // console.log(resUserId + ' ' +userId)
             //ห้ามใส่ username ตัวเอง เขียนไว้กันบัค
             if(userId === resUserId){
                ctx.status = 400
                ctx.body = {status: 'can not put self username'}  
                return
             }
           
            
           
           // check ว่าเป็นเพื่อนกันไหม
           const [checkFriend] = await pool.query(` SELECT id FROM friend_list WHERE user_id =? AND friend_user_id = ?`
           ,[userId, resUserId])
            if(checkFriend.length > 0){ // ถ้าเป็นเพื่อนแล้ว ให้แสดง error message
                ctx.status = 400
                ctx.body = {status: 'this user alreay friend'}
                return 
            }
            if(status === 'add'){ 
                //check ว่ามีคนส่งคำขอแอดเพื่อนเรายัง ถ้ามีแล้ว แอดไม่ได้ ปล. ป้องกันข้อมูลซ้ำซ้อน
           const [checkAlreadyRequest] = await pool.query(` SELECT id FROM friend_request WHERE request_user_id = ? AND response_user_id = ? `
           ,[resUserId, userId])
           if(checkAlreadyRequest.length > 0){
               ctx.status = 400
               ctx.body = {status: 'another user already request to you'}
               return
           }
                
                //ถ้ากด add มา ให้เช็คก่อนว่ามี request ในdbไหม ถ้าไม่มีให้ insert ลง db
             const [checkRequest] = await pool.query(` SELECT id FROM friend_request WHERE request_user_id = ? AND response_user_id = ? `
                                                    ,[userId, resUserId])

               if(!checkRequest[0]){
                   const [insertRequest] = await pool.query(` INSERT INTO friend_request (request_user_id, response_user_id) 
                                                                VALUES (?,?)`,[userId, resUserId])
                   ctx.body = {status: 'request sent'}                                             
               } 
               else{
                ctx.status = 400
                ctx.body = {status: ' alreay request'}
               }        
          
            }  
         
         else if(status === 'cancel'){ //ถ้ากด cancel ให้ดูว่ามี request ใน db ไหม ถ้ามีให้ลบ
             const [checkRequest2] = await pool.query(` SELECT id FROM friend_request WHERE request_user_id = ? AND response_user_id = ? `
                                                       ,[userId, resUserId])
             if(checkRequest2.length > 0){
                 const [deleteRequest] = await pool.query(` DELETE FROM friend_request WHERE request_user_id = ? AND response_user_id = ?`
                                                        ,[userId, resUserId])
               ctx.body = {status: 'request canceled'}
             }
             else{
                ctx.status = 400
                ctx.body = {status: 'no request'}
             }                                          
            
            }
        }
    }
}