module.exports = function (userModel, pool){
    return{
        async checkMutualFriend (ctx, next){
            const userId = ctx.session.userId
            const username = ctx.request.body.username
            const startRow = ctx.request.body.start_row
            const checkPerPage = ctx.request.body.per_page
            const perPage = []
            const [checkUsername] = await pool.query(` SELECT id FROM user WHERE username =? `,[username])
             if(!checkUsername[0]){
                 ctx.status = 400
                 ctx.body = { status: 'not found target username'}
                 return
             }
             const targetUserId = checkUsername[0].id
             if(targetUserId == userId){
                ctx.status = 400
                ctx.body = { status: 'can not put yourself username'}
                return
             }
           const [countMualFriend] = await pool.query(` SELECT id FROM friend_list WHERE user_id =?
                                                         AND friend_user_id IN ( SELECT friend_user_id FROM friend_list
                                                                                  WHERE user_id =? AND friend_user_id NOT IN (?) 
                                                                                )`,[userId, targetUserId, userId])
          const rowNumber = countMualFriend.length  // row number
          const offsetStart = parseInt( (startRow - 1) ) // use in db query only begin with index 0
          const restRow = rowNumber - (startRow - 1)
          if(!countMualFriend[0]){
            ctx.body = {mutual_friend: 'no',message: 'you not have mutual friend'}
            return 
          }
         
          if (restRow < checkPerPage){
          perPage.push( parseInt(restRow))
         }      
         else if (restRow >= checkPerPage){
          perPage.push( parseInt(checkPerPage))
         }
           const [checkMutualFriend] = await pool.query(` SELECT u.username ,u.firstname, u.lastname, p.profile_picture_url  
                                                           FROM friend_list f
                                                           INNER JOIN user u
                                                           ON f.friend_user_id = u.id
                                                           LEFT JOIN user_profile_picture p
                                                           ON u.id = p.user_id
                                                           WHERE 
                                                           ( p.id IN ( SELECT MAX(id) FROM user_profile_picture
                                                                             GROUP BY user_id
                                                                         )
                                                                 OR p.id IS NULL         
                                                           )
                                                           AND f.user_id =?   
                                                           AND f.friend_user_id IN ( SELECT friend_user_id FROM friend_list
                                                                                     WHERE user_id =? AND friend_user_id NOT IN (?) 
                                                                                    )
                                                           ORDER BY f.id 
                                                           LIMIT ? OFFSET ? `
                                                           ,[userId, targetUserId, userId,perPage[0], offsetStart])
          if(!checkMutualFriend[0]){
              ctx.body = {mutual_friend: 'no',message: 'you not have mutual friend'}
              return
          }                                                   
           ctx.body = {mutual_friend: 'yes', mutual_friend_number: rowNumber , mutual_friend_data: checkMutualFriend}                                              
        }
    }
}