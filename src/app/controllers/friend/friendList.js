module.exports = function (userModel, pool){
    return{
        async friendList (ctx, next){
        const userId = ctx.session.userId
        const rowStart = ctx.request.body.row_start
        const checkPerPage = ctx.request.body.per_page
        const perPage = []
        if(rowStart === '' || checkPerPage === ''){
            ctx.status = 400
            ctx.body = {status : 'please in put all data'}
            return
        }        
        // check ว่ามี friend list ไหม
        const [checkFriend] = await pool.query(`SELECT id FROM friend_list WHERE user_id = ?`,[userId])
        if(!checkFriend[0]){
            ctx.status = 400
            ctx.body = { status: 'no friend'}
            return
        }
        const rowNumber = checkFriend.length
        const restRow = rowNumber - (rowStart - 1)
        if(rowStart <= 0){
            ctx.status = 400
            ctx.body = {status : 'wrong row start'}
            return
        }
       
        if(restRow < checkPerPage){
           perPage.push(parseInt(restRow))  

       } 
       else if (restRow >= checkPerPage){

          perPage.push(parseInt(checkPerPage))
       } 
     const [data] = await pool.query(`SELECT u.username, u.firstname, u.lastname,  p.profile_picture_url
                                      FROM  user_profile_picture p 
                                      RIGHT JOIN friend_list f
                                      ON p.user_id = f.friend_user_id
                                      JOIN user u
                                      ON f.friend_user_id = u.id
                                      WHERE ( p.id IN (SELECT max(id) as id
                                                      FROM user_profile_picture GROUP BY user_id)
                                             OR p.id IS NULL
                                            )          
                                      AND f.user_id = ?
                                      ORDER BY f.id
                                      LIMIT ?,?
                                      `,[userId, (rowStart -1), perPage[0]] )
      if(!data[0]){
          ctx.status = 400
          ctx.body = {status : 'no friend'}
          return
      }                                
     ctx.body = {row_number: rowNumber, data:data} 


  }
 }
}