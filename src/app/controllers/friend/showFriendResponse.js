module.exports = function (userModel, pool){
    return{
        async showFriendResponse (ctx, next){
            const userId = ctx.session.userId
            const rowStart = ctx.request.body.row_start   
            const checkPerPage = ctx.request.body.per_page
            const perPage = []   
            if ( rowStart === '' || checkPerPage === ''){
                ctx.status = 400
                ctx.body = {status: 'please in put all data'}
                return
            }  
            const [data] = await pool.query(` SELECT id FROM  friend_request WHERE response_user_id = ? `,[userId])
                if(!data[0]){
                    ctx.status = 400
                    ctx.body = {status: 'no request'}
                    return
                } 
                const rowNumber = data.length
                const restRow = rowNumber - (rowStart - 1)
                if(rowStart <= 0){
                    ctx.status = 400
                    ctx.body = {status: 'wrong row start'}
                    return
                 }        
                 if(restRow <= 0){
                     ctx.status = 400
                     ctx.body = {status: 'wrong row start'}
                     return
                  }    
                else if( restRow < checkPerPage){
                     perPage.push(parseInt(restRow))
                }
                else if(restRow >= checkPerPage){
                     perPage.push(parseInt(checkPerPage)) 
                }
                const [data2] = await pool.query(` SELECT u.username, u.firstname, u.lastname, upp.profile_picture_url
                FROM  user_profile_picture  upp 
                RIGHT JOIN friend_request f
                ON upp.user_id = f.request_user_id
                INNER JOIN user u
                 ON  f.request_user_id = u.id
                 WHERE 
                 (
                  upp.id IN (SELECT MAX(id) AS id FROM user_profile_picture
                              GROUP BY user_id)
                  OR upp.id IS NULL            
                 )
                 AND f.response_user_id = ?
                 ORDER BY f.id DESC
                 LIMIT ?,? `
                 ,[userId, (rowStart - 1), perPage[0]])

                if(!data2[0]){
                    ctx.status =400
                    ctx.body = {status: 'no request'}
                    return
                  }
                 ctx.body = {row_number: rowNumber, data: data2 }
            
        }
    }
}