module.exports = function (userModel, pool){
    return{
       async friendResponse (ctx, next){
         const userId = ctx.session.userId
         const requestUsername = ctx.request.body.request_username
         const status = ctx.request.body.status
         //check ค่า form
         if( requestUsername === '' || status === ''){
             ctx.status = 400
             ctx.body = {status: 'please input all data'}
             return
         }
         else if( status !== 'confirm' && status !== 'delete'){
            ctx.status = 400
            ctx.body = {status: 'wrong status'}
            return
         }

         const [reqUser] = await pool.query(` SELECT id FROM user WHERE username = ?`,[requestUsername])
         if(!reqUser[0]){
             ctx.status = 400
             ctx.body = {status: ' username not found'}
             return
         }
         const requestUserId = reqUser[0].id // request user id
         //check ว่าเป็นเพื่อนกันยัง (กันบัค)
         const [checkFriend] = await pool.query(`SELECT id FROM friend_list WHERE user_id = ? AND friend_user_id =? `
                                                ,[userId, requestUserId]) 
          if(checkFriend.length > 0){
            ctx.status = 400
            ctx.body = {status: 'already friend'}
            return
          }

        if(status === 'confirm'){ //กดรับแอดเพื่อน
             //check ว่ามี request ใน db ไหม ถ้ามีให้แอดเป็นเพื่อน และลบrequest ออก , ถ้าไม่มี error
        const [checkRequest] = await pool.query(` SELECT id FROM friend_request WHERE request_user_id = ? AND response_user_id = ? `
                                                ,[requestUserId, userId])
           if(checkRequest.length > 0){
        const [deleteRequest] = await pool.query(` DELETE FROM friend_request WHERE request_user_id = ? AND response_user_id = ? `
                                                  ,[requestUserId, userId])       
        const [insertFriend] = await pool.query(` INSERT INTO friend_list (user_id, friend_user_id) 
                                                         VALUES (?,?)`,[userId, requestUserId])
        const [insertFriend2] = await pool.query(` INSERT INTO friend_list (user_id, friend_user_id) 
                                                         VALUES (?,?)`,[requestUserId, userId]) 
           ctx.body = {status:' request confirmed'}                                                                                       
           } 
         else{
             ctx.status = 400
             ctx.body = {status : 'no request'}
             return
         } 
        }

        else if(status === 'delete'){ //ไม่รับเป็นเพื่อน
//check ว่ามี request ใน db ไหม ถ้ามีและลบrequest ออก , ถ้าไม่มี error
        const [checkRequest2] = await pool.query(` SELECT id FROM friend_request WHERE request_user_id = ? AND response_user_id = ? `
                                                ,[requestUserId, userId])
            if(checkRequest2.length > 0){
            const [deleteRequest2] = await pool.query(` DELETE FROM friend_request WHERE request_user_id = ? AND response_user_id = ? `
                                                  ,[requestUserId, userId])
            ctx.body = {status: 'request deleted'}     
            }
            else{
                ctx.status = 400
                ctx.body = {status : 'no request'}
                return
            } 

        }
                 
        }
    }
}