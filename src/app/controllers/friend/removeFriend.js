module.exports = function (userModel, pool){
    return{
        async removeFriend (ctx, next){
           const userId = ctx.session.userId
           const friendUsername = ctx.request.body.username //ดึง id เพื่อน
           const [checkFriend] = await pool.query(`SELECT id FROM user WHERE username = ?`,[friendUsername])
           if(!checkFriend[0]){
               ctx.status = 400
               ctx.body = {status: 'not found friend username'}
               return
           } 
           const friendUserId = checkFriend[0].id
           //เช็คว่าเป็นเพื่อนกันไหม
           const [checkFriendStatus] = await pool.query(`SELECT id FROM friend_list WHERE user_id =? AND friend_user_id =?`
                                                   ,[userId, friendUserId])
             if(!checkFriendStatus[0]){
                 ctx.status = 400
                 ctx.body = {status: 'this username is not your friend'}
                 return
             }

            // ลบเพื่อน
           const [removeFreind] = await pool.query(`DELETE FROM friend_list WHERE user_id =? AND friend_user_id =?`
                                                    ,[userId, friendUserId])
           const [removeFriend2] = await pool.query(` DELETE FROM friend_list WHERE user_id =? AND friend_user_id =?`
                                                    ,[friendUserId, userId])                                         
          ctx.body = {status : 'remove friend complete'}      
        }
    }
}