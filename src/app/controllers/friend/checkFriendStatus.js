module.exports = function(userModel, pool){
    return{
       async checkFriendStatus (ctx, next){
          const userId = ctx.session.userId
          const username = ctx.request.body.username
          const [checkUsername] = await pool.query(` SELECT id FROM user WHERE username =? `,[username])  //ดึงค่า user id
          if(!checkUsername[0]){
            ctx.status = 400
            ctx.body = {status: 'not found target username'}
            return
          }
          const targetUserId = checkUsername[0].id
          if(targetUserId == userId){
            ctx.status = 400
            ctx.body = { status: 'can not put yourself username'}
            return
         }
          const [checkFriend] = await pool.query(`SELECT id FROM friend_list WHERE user_id =? AND friend_user_id =?`
                                                ,[userId, targetUserId]) //เช็คว่าเป็นเพื่อนกันไหม
           if(checkFriend[0]){
               ctx.body ={status: 'friend'}
               return
           } 
         const[checkRequest] = await pool.query(` SELECT id FROM friend_request WHERE request_user_id =? AND response_user_id =? `
                                                  ,[userId, targetUserId])  // เช็คว่า เราขอเป็นเพื่อนไปแล้วหรือยัง
          if(checkRequest[0]){
              ctx.body = {status: 'requesting'}
              return
          }
         const [checkResponse] = await pool.query(` SELECT id FROM friend_request WHERE request_user_id =? AND response_user_id =? `
                                          ,[targetUserId, userId])  //เช็คว่า มีคนกดขอเราเป็นเพื่อนแล้วหรือยัง                              
          if(checkResponse[0]){
              ctx.body = {status: 'pending'}
              return
          }  
          ctx.body = {status: 'none'} //ไม่ได้เป็นเพื่อน และ ไม่ได้ขอใครเป็นเพื่อน และ ไม่มีใครขอเป็นเพื่อน                                
        }
    }
}