module.exports = function (userModel, pool){
    return{
       async friendRecommend (ctx, next){
         const userId = ctx.session.userId
         const rowStart = ctx.request.body.row_start
         const checkPerPage = ctx.request.body.per_page
         const perPage = []
         if(rowStart ==='' || checkPerPage ===''){
            ctx.status = 400
            ctx.body = {status: 'please in put all data'}
            return
         }

        const[check] = await pool.query(`  SELECT id FROM  user
                                         WHERE
                                      id NOT IN ( SELECT DISTINCT request_user_id FROM  friend_request
                                                  WHERE 	response_user_id = ?
                                                 )
                                AND id NOT IN ( SELECT DISTINCT  response_user_id  FROM  friend_request
                                                 WHERE request_user_id = ?
                                               )
                                AND id NOT IN ( SELECT DISTINCT friend_user_id FROM friend_list
                                                WHERE user_id = ?
                                              )                      
                              AND id NOT IN (?) `,[userId, userId, userId, userId])
         if(!check[0]){
              ctx.status = 400
              ctx.body = {status: 'no friend recommend'}
              return
         }
       const rowNumber = check.length
       const restRow = rowNumber - ( rowStart - 1)
       if(rowStart <= 0){
           ctx.status = 400
           ctx.body = {status: 'wrong row start'}
           return
       }
     
      if(restRow < checkPerPage){
          perPage.push(parseInt(restRow))
      }
      else if (restRow >= checkPerPage){
          perPage.push(parseInt(checkPerPage))
      }
        const [data] = await pool.query(`  SELECT u.username, u.firstname, u.lastname, p.profile_picture_url  
                                                           FROM user_profile_picture p
                                                           RIGHT JOIN  user u 
                                                           ON u.id = p.user_id  
                                                            WHERE
                                                            ( p.id IN ( SELECT MAX(id) AS id FROM user_profile_picture
                                                                        GROUP BY user_id
                                                                       )
                                                              OR p.id IS NULL 
                                                            )  
                                                              AND u.id NOT IN ( SELECT DISTINCT request_user_id FROM  friend_request
                                                                                WHERE 	response_user_id = ?
                                                                            )
                                                             AND u.id NOT IN ( SELECT DISTINCT  response_user_id  FROM  friend_request
                                                                                WHERE request_user_id = ?
                                                                             )
                                                             AND u.id NOT IN ( SELECT DISTINCT friend_user_id FROM friend_list
                                                                                WHERE user_id = ?
                                                                              )                      
                                                             AND u.id NOT IN (?)
                                                             ORDER BY u.id DESC
                                                             LIMIT ?,? `,[userId, userId, userId, userId, (rowStart - 1), perPage[0] ])
        if(!data[0]){
          ctx.status = 400
          ctx.body = {status: 'no friend recommend'}
          return
        }                                                     
       ctx.body = {row_nuber: rowNumber, data: data}
 

        }
    }
}