const mysql = require('mysql2/promise')
const pool = mysql.createPool({
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'codecamp3_project'
})

module.exports = pool