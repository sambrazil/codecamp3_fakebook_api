const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const IO = require('koa-socket-2')
const koaBody = require('koa-body') 
const session = require('koa-session')
//const bcrypt = require('bcrypt')
//const uuidv4 = require('uuid/v4')
//const fs = require('fs-extra')
const cors = require('@koa/cors')
const pool = require('./src/app/lib/db')

const app = new Koa()
const router = new Router()
const io = new IO()
//const server = require('http').createServer(app.callback())
//session
const sessionStore = {}
app.keys = ['some secret hurr'];
const CONFIG = {
key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
/** (number || 'session') maxAge in ms (default is 1 days) */
/** 'session' will result in a cookie that expires when session/browser is closed */
/** Warning: If a session cookie is stolen, this cookie will never expire */
maxAge: 86400000,
autoCommit: true, /** (boolean) automatically commit headers (default true) */
overwrite: true, /** (boolean) can overwrite or not (default true) */
httpOnly: true, /** (boolean) httpOnly or not (default true) */
signed: true, /** (boolean) signed or not (default true) */
rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
get(key, maxAge, { rolling }){
   return sessionStore[key]
},
set(key, sess, maxAge, { rolling, changed }){
   sessionStore[key] = sess
},
destroy(key){
delete sessionStore[key]
}
};
app.use(session(CONFIG, app))

 //อันบน ไว้สำหรับ test ในเครื่องผมเอง ถ้าเอาออกให้ ใส่ comment 
//const host = 'http://localhost:3000' 
const host = process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : 'http://34.87.41.71:3000'

app.use(cors({
    origin: host,
    credentials:true,
    allowMethods: [ 'POST','GET'],
    allowHeaders: ['Content-Type'],
    maxAge: 300
   }))
   // login logout register
 const registerTemp = require('./src/app/controllers/register') //แก้แล้ว
 const registerController = registerTemp(null, pool)
 const loginTemp = require('./src/app/controllers/login')  //แก้แล้ว
 const loginController = loginTemp(null, pool)
 const logoutTemp = require('./src/app/controllers/logout') //แก้แล้ว
 const logoutController = logoutTemp(null, pool)
// profile
 const postEditProfileTemp = require('./src/app/controllers/profile/postEditProfile') //แก้แล้ว
 const postEditProfileController = postEditProfileTemp(null,pool)
 const showEditProfileTemp = require('./src/app/controllers/profile/showEditProfile') //แก้แล้ว
 const showEditProfileController = showEditProfileTemp(null, pool)
 
 //feed
 const createFeedPostTemp = require('./src/app/controllers/feed/createFeedPost') //แก้แล้ว
 const createFeedPostController = createFeedPostTemp(null, pool)
 const showUserFeedPostTemp = require('./src/app/controllers/feed/showUserFeedPost') //แก้แล้ว
 const showUserFeedPostController = showUserFeedPostTemp(null, pool)
 const createFeedCommentTemp = require('./src/app/controllers/feed/createFeedComment') //แก้แล้ว
 const createFeedCommentController = createFeedCommentTemp(null, pool)
 const showUserFeedCommentTemp = require('./src/app/controllers/feed/showUserFeedComment')  //แก้แล้ว
 const showUserFeedCommentController = showUserFeedCommentTemp(null, pool)
 const removePostTemp = require('./src/app/controllers/feed/removePost')
 const removePostController = removePostTemp(null, pool)
 const removeCommentTemp = require('./src/app/controllers/feed/removeComment')
 const removeCommentController = removeCommentTemp(null, pool)
 const editPostTemp = require('./src/app/controllers/feed/editPost')
 const editPostController = editPostTemp(null, pool)
 const editCommentTemp = require('./src/app/controllers/feed/editComment')
 const editCommentController = editCommentTemp(null, pool)
 const editPostPrivacyTemp = require('./src/app/controllers/feed/editPostPrivacy')
 const editPostPrivacyController = editPostPrivacyTemp(null, pool)
 const showAllFeedPostTemp = require('./src/app/controllers/feed/showAllFeedPost')
 const showAllFeedPostController = showAllFeedPostTemp(null, pool)
 //feed like
 const likePostTemp = require('./src/app/controllers/feed/likePost') //แก้แล้ว
 const likePostController = likePostTemp(null, pool)
 const likeCommentTemp = require('./src/app/controllers/feed/likeComment') //แก้แล้ว
 const likeCommentController = likeCommentTemp(null, pool)
 const checkLikePostTemp = require('./src/app/controllers/feed/checkLikePost') //แก้แล้ว
 const checkLikePostController = checkLikePostTemp(null, pool)
 const checkLikeCommentTemp = require('./src/app/controllers/feed/checkLikeComment')  //แก้แล้ว
 const checkLikeCommentController = checkLikeCommentTemp(null, pool)
 //show post by id
 const showPostByIdTemp = require('./src/app/controllers/feed/showPostById')
 const showPostByIdController = showPostByIdTemp(null, pool)
  //friend
 const friendRequestTemp = require('./src/app/controllers/friend/friendRequest') //แก้แล้ว
 const friendRequestController = friendRequestTemp(null, pool)
 const friendResponseTemp = require('./src/app/controllers/friend/friendResponse') //แก้แล้ว
 const friendResponseController = friendResponseTemp(null ,pool)
 const showFriendResponseTemp = require('./src/app/controllers/friend/showFriendResponse') //แก้แล้ว
 const showFriendResponseController = showFriendResponseTemp(null, pool)
 const showFriendRequestTemp = require('./src/app/controllers/friend/showFriendRequest') //แก้แล้ว
 const showFriendRequestController = showFriendRequestTemp(null, pool)
 const friendListTemp = require('./src/app/controllers/friend/friendList') //แก้แล้ว
 const friendListController = friendListTemp(null, pool)
 const friendRecommendTemp = require('./src/app/controllers/friend/friendRecommend') //แก้แล้ว
 const friendRecommendController = friendRecommendTemp(null, pool)
 const removeFriendTemp = require('./src/app/controllers/friend/removeFriend')
 const removeFriendController = removeFriendTemp(null, pool)
 const checkFriendStatusTemp = require('./src/app/controllers/friend/checkFriendStatus')
 const checkFriendStatusController = checkFriendStatusTemp(null, pool)
 const checkMutualFriendTemp = require('./src/app/controllers/friend/checkMutualFriend')
 const checkMutualFriendControler = checkMutualFriendTemp(null,pool)
 // album
 const showProfilePictureTemp = require('./src/app/controllers/album/showProfilePicture') 
 const showProfilePictureController = showProfilePictureTemp(null, pool)
 const showCoverPictureTemp = require('./src/app/controllers/album/showCoverPicture')
 const showCoverPictureController = showCoverPictureTemp(null, pool)
 const showPostPictureTemp = require('./src/app/controllers/album/showPostPicture')
 const showPostPictureController = showPostPictureTemp(null, pool)
// view profile
 const viewProfileTemp = require('./src/app/controllers/views/view_profile/viewProfile') //แก้แล้ว
 const viewProfileController = viewProfileTemp(null, pool)
 const viewPrivateProfileTemp = require('./src/app/controllers/views/view_profile/viewPrivateProfile')
 const viewPrivateProfileController = viewPrivateProfileTemp(null, pool)
 // view feed post
 const viewPostTemp = require('./src/app/controllers/views/view_feed/viewPost')
 const viewPostController = viewPostTemp(null, pool)
 const viewPrivatePostTemp = require('./src/app/controllers/views/view_feed/viewPrivatePost')
 const viewPrivatePostController = viewPrivatePostTemp(null, pool)
 const viewPrivatePostByIdTemp = require('./src/app/controllers/views/view_feed/viewPrivatePostById')
 const viewPrivatePostByIdController = viewPrivatePostByIdTemp(null, pool)
 // view friend list
 const viewPrivateFriendListTemp = require('./src/app/controllers/views/view_friend_list/viewPrivateFriendList')
 const viewPrivateFriendListController = viewPrivateFriendListTemp(null, pool)
 const viewFriendListTemp = require('./src/app/controllers/views/view_friend_list/viewFriendList')
 const viewFriendListController = viewFriendListTemp(null, pool)
//view album
const viewProfilePictureTemp = require('./src/app/controllers/views/view_album/viewProfilePicture')
const viewProfilePictureController = viewProfilePictureTemp(null, pool)
const viewCoverPictureTemp = require('./src/app/controllers/views/view_album/viewCoverPicture')
const viewCoverPictureController = viewCoverPictureTemp(null, pool)
const viewPostPictureTemp = require('./src/app/controllers/views/view_album/viewPostPicture')
const viewPostPictureController = viewPostPictureTemp(null, pool)
const viewPrivatePostPictureTemp = require('./src/app/controllers/views/view_album/viewPrivatePostPicture') 
const viewPrivatePostPictureController = viewPrivatePostPictureTemp(null, pool)
//setting
 const showProfileSettingTemp = require('./src/app/controllers/setting/setting_profile/showProfileSetting')
 const showProfileSettingControlller = showProfileSettingTemp(null, pool)
 const postProfileSettingTemp = require('./src/app/controllers/setting/setting_profile/postProfileSetting')
 const postProfileSettingController = postProfileSettingTemp(null, pool)
 const showFeedPostSettingTemp = require('./src/app/controllers/setting/setting_feed_post/showFeedPostSetting')
 const showFeedPostSettingController = showFeedPostSettingTemp(null,pool)
 const postFeedPostSettingTemp = require('./src/app/controllers/setting/setting_feed_post/postFeedPostSetting')
 const postFeedPostSettingController = postFeedPostSettingTemp(null,pool)
 const showFriendListSettingTemp = require('./src/app/controllers/setting/setting_friend_list/showFriendListSetting')
 const showFriendListSettingController = showFriendListSettingTemp(null, pool)
 const postFriendListSettingTemp = require('./src/app/controllers/setting/setting_friend_list/postFriendListSetting')
 const postFriendListSettingController = postFriendListSettingTemp(null, pool) 
 //search
 const searchTemp = require('./src/app/controllers/search/search')
 const  serachController = searchTemp(null, pool)
//notification
const showNotificationTemp = require('./src/app/controllers/notification/showNotification')
const showNotificationController = showNotificationTemp(null, pool)
const updateNotificationNumberTemp = require('./src/app/controllers/notification/updateNotificationNumber')
const updateNotificationNumberController = updateNotificationNumberTemp(null, pool)
const updateReadStatusTemp = require('./src/app/controllers/notification/updateReadStatus')
const updateReadStatusController = updateReadStatusTemp(null, pool)

 // login logout rigster
router.post('/api/register',registerController.registerUser) // http://localhost:3001/api/register //แก้แล้ว
router.post('/api/login',loginController.checkLogin) // http://localhost:3001/api/login //แก้แล้ว
router.post('/api/logout',logoutController.logout)  // http://localhost:3001/api/logout //แก้แล้ว
 // profile
router.post('/api/post/edit_profile',postEditProfileController.postEditProfile) // http://localhost:3001/api/post/edit_profile //แก้แล้ว
router.post('/api/post/show_edit_profile', showEditProfileController.showEditProfile) // http://localhost:3001/api/post/show_edit_profile  //แก้เป็นpost

// feed
router.post('/api/post/create_feed_post', createFeedPostController.createFeedPost) // http://localhost:3001/api/post/create_feed_post //แก้แล้ว
router.post('/api/post/show_user_feed_post', showUserFeedPostController.showUserFeedPost) // http://localhost:3001/api/post/show_user_feed_post //แก้เป็น post
router.post('/api/post/create_feed_comment', createFeedCommentController.createFeedComment) // http://localhost:3001/api/post/create_feed_comment  //แก้แล้ว
router.post('/api/post/show_user_feed_comment',showUserFeedCommentController.showUserFeedComment) // http://localhost:3001/api/post/show_user_feed_comment แก้เป็น post
router.post('/api/post/remove_post', removePostController.removePost) // http://localhost:3001/api/post/remove_post
router.post('/api/post/remove_comment', removeCommentController.removeComment) // http://localhost:3001/api/post/remove_comment
router.post('/api/post/edit_post', editPostController.editPost)  // http://localhost:3001/api/post/edit_post
router.post('/api/post/edit_comment', editCommentController.editComment) // http://localhost:3001/api/post/edit_comment
router.post('/api/post/edit_post_privacy', editPostPrivacyController.editPostPrivacy) // http://localhost:3001/api/post/edit_post_privacy
router.post('/api/post/show_all_feed_post', showAllFeedPostController.showAllFeedPost) // http://localhost:3001/api/post/show_all_feed_post
// feed like
router.post('/api/post/like_post', likePostController.likePost) //   http://localhost:3001/api/post/like_post //แก้แล้ว
router.post('/api/post/like_comment', likeCommentController.likeComment) //  http://localhost:3001/api/post/like_comment //แก้แล้ว
router.post('/api/post/check_like_post', checkLikePostController.checkLikePost) // http://localhost:3001/api/post/check_like_post //แก้แล้ว
router.post('/api/post/check_like_comment', checkLikeCommentController.checkLikeComment) // http://localhost:3001/api/post/check_like_comment //แก้แล้ว
// show post by id
router.post('/api/post/show_post_by_id', showPostByIdController.showPostById) //  http://localhost:3001/api/post/show_post_by_id
//friend 
router.post('/api/post/friend_request', friendRequestController.friendRequest) // http://localhost:3001/api/post/friend_request //แก้แล้ว
router.post('/api/post/friend_response', friendResponseController.friendResponse) // http://localhost:3001/api/post/friend_response //แก้แล้ว
router.post('/api/post/show_friend_response', showFriendResponseController.showFriendResponse) // http://localhost:3001/api/post/show_friend_response //แก้แล้ว
router.post('/api/post/show_friend_request', showFriendRequestController.showFriendRequest) // http://localhost:3001/api/post/show_friend_request //แก้แล้ว
router.post('/api/post/friend_list', friendListController.friendList) // http://localhost:3001/api/post/friend_list  //แก้แล้ว
router.post('/api/post/friend_recommend', friendRecommendController.friendRecommend)  // http://localhost:3001/api/post/friend_recommend //แก้แล้ว
router.post('/api/post/remove_friend',removeFriendController.removeFriend) // http://localhost:3001/api/post/remove_friend
router.post('/api/post/check_friend_status', checkFriendStatusController.checkFriendStatus) // http://localhost:3001/api/post/check_friend_status
router.post('/api/post/check_mutual_friend', checkMutualFriendControler.checkMutualFriend) // http://localhost:3001/api/post/check_mutual_friend
//album
router.post('/api/post/show_profile_picture', showProfilePictureController.showProfilePicture) // http://localhost:3001/api/post/show_profile_picture
router.post('/api/post/show_cover_picture', showCoverPictureController.showCoverPicture) // http://localhost:3001/api/post/show_cover_picture
router.post('/api/post/show_post_picture', showPostPictureController.showPostPicture) // http://localhost:3001/api/post/show_post_picture
//view album
router.get('/api/get/view_profile_picture/:username/:start_row/:per_page', viewProfilePictureController.viewProfilePicture)  // http://localhost:3001/api/get/view_profile_picture/:username/:start_row/:per_page
router.get('/api/get/view_cover_picture/:username/:start_row/:per_page', viewCoverPictureController.viewCoverPicture)  // http://localhost:3001/api/get/view_cover_picture/:username/:start_row/:per_page
router.get('/api/get/view_post_picture/:username/:start_row/:per_page', viewPostPictureController.viewPostPicture)  // http://localhost:3001/api/get/view_post_picture/:username/:start_row/:per_page
router.post('/api/post/view_private_post_picture', viewPrivatePostPictureController.viewPrivatePostPicture) // http://localhost:3001/api/post/view_private_post_picture
//view profile
router.get('/api/get/view_profile/:username',viewProfileController.viewProfile) // http://localhost:3001/api/get/view_profile/:username  //แก้แล้ว เปลี่ยนแค่ชื่อ
router.post('/api/post/view_private_profile', viewPrivateProfileController.viewPrivateProfile) // http://localhost:3001/api/post/view_private_profile
//view feed
router.get('/api/get/view_post/:username/:start_row/:per_page', viewPostController.viewPost) // http://localhost:3001/api/get/view_post/:username/:start_row/:per_page
router.post('/api/post/view_private_post', viewPrivatePostController.viewPrivatePost) // http://localhost:3001/api/post/view_private_post
router.post('/api/post/view_private_post_by_id', viewPrivatePostByIdController.viewPrivatePostById) // http://localhost:3001/api/post/view_private_post_by_id
//view friend list
router.post('/api/post/view_private_friend_list', viewPrivateFriendListController.viewPrivateFriendList) // http://localhost:3001/api/post/view_private_friend_list
router.get('/api/get/view_friend_list/:username/:start_row/:per_page', viewFriendListController.viewFriendList) //  http://localhost:3001/api/get/view_friend_list/:username/:start_row/:per_page
//setting
router.post('/api/post/show_profile_setting', showProfileSettingControlller.showProfileSetting) // http://localhost:3001/api/post/show_profile_setting
router.post('/api/post/post_profile_setting', postProfileSettingController.postProfileSetting)  // http://localhost:3001/api/post/post_profile_setting
router.post('/api/post/show_feed_post_setting', showFeedPostSettingController.showFeedPostSetting) // http://localhost:3001/api/post/show_feed_post_setting
router.post('/api/post/post_feed_post_setting', postFeedPostSettingController.postFeedPostSetting) // http://localhost:3001/api/post/post_feed_post_setting
router.post('/api/post/show_friend_list_setting', showFriendListSettingController.showFriendListSetting) // http://localhost:3001/api/post/show_friend_list_setting
router.post('/api/post/post_friend_list_setting', postFriendListSettingController.postFriendListSetting) // http://localhost:3001/api/post/post_friend_list_setting
//search
router.post('/api/post/search', serachController.search) //  http://localhost:3001/api/post/search
//notification
router.post('/api/post/show_notification', showNotificationController.showNotification) // http://localhost:3001/api/post/show_notification
router.post('/api/post/update_notification_number', updateNotificationNumberController.updateNotificationNumber) // http://localhost:3001/api/post/update_notification_number
router.post('/api/post/update_read_status', updateReadStatusController.updateReadStatus) // http://localhost:3001/api/post/update_read_status
 
async function checkLogin(ctx, next){
   if( ctx.path ==='/api/post/edit_profile' || ctx.path ==='/api/post/show_edit_profile'

      || ctx.path ==='/api/post/create_feed_post' || ctx.path ==='/api/post/show_user_feed_post' || ctx.path ==='/api/post/create_feed_comment' 
      || ctx.path ==='/api/post/show_user_feed_comment' || ctx.path ==='/api/post/remove_post' || ctx.path ==='/api/post/remove_comment' 
      || ctx.path ==='/api/post/edit_post' || ctx.path ==='/api/post/edit_comment' || ctx.path ==='/api/post/edit_post_privacy' 
      || ctx.path ==='/api/post/show_all_feed_post'
      
      || ctx.path ==='/api/post/like_post' || ctx.path ==='/api/post/like_comment' || ctx.path ==='/api/post/check_like_post' 
      || ctx.path ==='/api/post/check_like_comment'
      
      || ctx.path ==='/api/post/show_post_by_id'

      || ctx.path ==='/api/post/friend_request' || ctx.path ==='/api/post/friend_response' || ctx.path ==='/api/post/show_friend_response'
      || ctx.path ==='/api/post/show_friend_request' || ctx.path ==='/api/post/friend_list' || ctx.path ==='/api/post/friend_recommend'
      || ctx.path ==='/api/post/remove_friend' || ctx.path ==='/api/post/check_friend_status' || ctx.path ==='/api/post/check_mutual_friend'

      || ctx.path ==='/api/post/show_profile_picture' || ctx.path ==='/api/post/show_cover_picture' || ctx.path ==='/api/post/show_post_picture'

       || ctx.path ==='/api/post/view_private_post_picture'

       || ctx.path ==='/api/post/view_private_profile'
      
       || ctx.path ==='/api/post/view_private_post' 
      || ctx.path ==='/api/post/view_private_post_by_id'

      || ctx.path ==='/api/post/view_private_friend_list' 

      || ctx.path ==='/api/post/show_profile_setting' || ctx.path ==='/api/post/post_profile_setting' 
      || ctx.path ==='/api/post/show_feed_post_setting' || ctx.path ==='/api/post/post_feed_post_setting' 
      || ctx.path ==='/api/post/show_friend_list_setting' || ctx.path ==='/api/post/post_friend_list_setting'
      
      || ctx.path ==='/api/post/search'

      || ctx.path ==='/api/post/show_notification' || ctx.path ==='/api/post/update_notification_number' 
      || ctx.path ==='/api/post/update_read_status'
      
      ){

         if(!ctx.session || !ctx.session.userId){
             ctx.status = 404
             ctx.body = {session:'not found session'}
             console.log(ctx.body)
         }
         else{

               await next()
         }

   }

   else{
      await next()
   }
}



io.attach(app)
io.on('connect', socket =>{
  console.log('user connected')
      io.on('disconnect', socket =>{
      console.log('user disconnected')
    })
})

app.use(koaBody({multipart:true}))
app.use(checkLogin)
app.use(serve(path.join(__dirname, 'src/assets')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3001)
