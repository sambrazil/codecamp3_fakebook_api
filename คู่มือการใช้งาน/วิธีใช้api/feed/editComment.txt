//แก้ไข comment และลบรูปเก่าใน folder 
url: http://localhost:3001/api/post/edit_comment
method: post
input: { comment_id:'',
         username:'',
         comment: ''
       }

output:
* status 404
    * {session:'not found session'}
*status 400
- {status: 'please input data'}
-{status: 'not found username'}
-{status: 'not found comment'}

*status 200
-{status: 'edit comment complete'}