// ตั้งค่าว่า post ในอนาคต จะให้ใครดู , insert/update ข้อมูลการตั้งค่าลง db
url: http://localhost:3001/api/post/post_feed_post_setting
method: post
input : { 
        privacy:'' //ต้องมีค่าเป็น public, friend, only me เท่านั้น
        }
output :
*status 404
- {session:'not found session'}

*status 400
-{"status":"wrong privacy, please input public, friend, only me"}

*status 200
- {"status":"setting feed post complete"}