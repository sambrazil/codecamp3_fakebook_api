// save ข้อมูลการตั้งค่า privacy ของ friend list ลงdb
url: http://localhost:3001/api/post/post_friend_list_setting
method: post
input : {  
           privacy:'only me' //มีได้3ค่า คือ public, friend, only me
        }
output:
*status 404
- {session:'not found session'}

*status 400
- {"status":" wrong privacy, please input public, friend, only me"}

*status 200
-  {"status":"setting friend list privacy complete"}