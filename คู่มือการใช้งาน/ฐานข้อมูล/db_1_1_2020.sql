-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 01, 2020 at 05:03 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codecamp3_project`
--

-- --------------------------------------------------------
create database codecamp3_project;
use codecamp3_project;
--
-- Table structure for table `feed_comment`
--

CREATE TABLE `feed_comment` (
  `id` int(255) NOT NULL,
  `post_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `picture_url` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `likes` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feed_comment`
--

INSERT INTO `feed_comment` (`id`, `post_id`, `user_id`, `picture_url`, `comment`, `created_at`, `likes`) VALUES
(10, 22, 55, '', '', '', ''),
(11, 22, 55, '/feed_comment/6f04836d-db80-4ffc-b4f4-8a96429b5f65231120192141384141.jpg', '', '2019 December 23 at 21:41', '0'),
(12, 22, 55, '/feed_comment/1738ff41-4e12-4b66-8d04-e447e4286211231120192141413941.jpg', '22222', '2019 December 23 at 21:41', '0'),
(13, 30, 59, '', '3000000000000', '2019 December 31 at 19:03', '0'),
(14, 30, 59, '/feed_comment/d6552c5d-b935-44d0-ae79-3e42658a4ead3111201919323651shot(10-05-19)15;43;53.jpg', '3000000000000', '2019 December 31 at 19:03', '0'),
(15, 30, 59, '/feed_comment/361d3032-9765-46a4-b707-f66618459d783111201919332665shot(10-05-19)15;43;53.jpg', '30', '2019 December 31 at 19:03', '0'),
(16, 31, 59, '/feed_comment/48fbe21a-d00c-4085-8029-a5cd76dc721d311120191934170shot(10-05-19)15;43;53.jpg', '31111111111111', '2019 December 31 at 19:03', '0'),
(17, 31, 59, '/feed_comment/ad26c218-da99-4405-8028-62fa96e0c657311120191934647shot(10-05-19)15;36;56.jpg', '31111111111111111', '2019 December 31 at 19:03', '0');

-- --------------------------------------------------------

--
-- Table structure for table `feed_post`
--

CREATE TABLE `feed_post` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `picture_url` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `privacy` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `likes` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feed_post`
--

INSERT INTO `feed_post` (`id`, `user_id`, `picture_url`, `caption`, `privacy`, `created_at`, `likes`) VALUES
(22, 48, '', '', 'only me', '', ''),
(23, 48, '', '', '', '', ''),
(24, 48, '', '', 'public', '', ''),
(25, 55, '', '', '', '', ''),
(26, 55, '', '', '', '', ''),
(27, 48, '', '5555', 'public', '2019 December 29 at 9:35', '0'),
(28, 62, '', '1111111', 'public', '2019 December 29 at 9:43', '0'),
(30, 48, '/feed_post/8b29d028-eec8-4f01-9cff-db6684b0350e31112019133817540shot(10-05-19)15;36;56.jpg', 'dsaasd', 'friend', '2019 December 31 at 13:38', '0'),
(31, 48, '/feed_post/20ed92a2-9680-4822-9fd9-58d12bc936d531112019133827272shot(10-05-19)15;36;56.jpg', 'dsaasd', 'public', '2019 December 31 at 13:38', '0'),
(32, 48, '/feed_post/224b001b-2ad0-4906-b9d0-aa15bd21504e3111201913383529shot(10-05-19)15;36;56.jpg', 'dsaasd', 'only me', '2019 December 31 at 13:38', '0'),
(33, 48, '', '1', 'public', '2019 December 31 at 13:39', '0'),
(34, 48, '', '2', 'friend', '2019 December 31 at 13:39', '0'),
(35, 48, '', '2', 'only me', '2019 December 31 at 13:39', '0'),
(36, 48, '/feed_post/44d8b72d-9751-41f3-acb9-f234615e813e31112019134019470shot(10-05-19)15;36;56.jpg', '', 'public', '2019 December 31 at 13:40', '0'),
(37, 48, '/feed_post/77271926-05e2-46c0-8805-7a3aa16dd0e831112019134023390shot(10-05-19)15;36;56.jpg', '', 'friend', '2019 December 31 at 13:40', '0'),
(38, 48, '/feed_post/eb55df6b-6dac-44d5-ab75-8461e578345f31112019134031558shot(10-05-19)15;36;56.jpg', '', 'only me', '2019 December 31 at 13:40', '0'),
(39, 49, '', 'only me', 'only me', '2020 January 1 at 9:32', '0'),
(40, 49, '', 'public', 'public', '2020 January 1 at 9:32', '0'),
(41, 49, '', 'friend', 'friend', '2020 January 1 at 9:33', '0');

-- --------------------------------------------------------

--
-- Table structure for table `feed_post_setting`
--

CREATE TABLE `feed_post_setting` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `privacy` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feed_post_setting`
--

INSERT INTO `feed_post_setting` (`id`, `user_id`, `privacy`) VALUES
(2, 55, 'friend'),
(4, 48, 'friend'),
(5, 62, 'public');

-- --------------------------------------------------------

--
-- Table structure for table `friend_list`
--

CREATE TABLE `friend_list` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `friend_user_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `friend_list`
--

INSERT INTO `friend_list` (`id`, `user_id`, `friend_user_id`) VALUES
(60, 58, 59),
(61, 59, 58),
(62, 48, 49),
(63, 49, 48),
(64, 58, 54),
(65, 54, 58),
(66, 58, 50),
(67, 50, 58),
(68, 58, 48),
(69, 48, 58),
(70, 58, 55),
(72, 55, 58),
(73, 59, 54),
(74, 54, 59),
(75, 59, 50),
(76, 50, 59),
(77, 59, 48),
(78, 48, 59),
(79, 59, 55),
(80, 55, 59);

-- --------------------------------------------------------

--
-- Table structure for table `friend_list_setting`
--

CREATE TABLE `friend_list_setting` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `privacy` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `friend_request`
--

CREATE TABLE `friend_request` (
  `id` int(255) NOT NULL,
  `request_user_id` int(255) NOT NULL,
  `response_user_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `friend_request`
--

INSERT INTO `friend_request` (`id`, `request_user_id`, `response_user_id`) VALUES
(13, 49, 51),
(15, 54, 51),
(18, 49, 53);

-- --------------------------------------------------------

--
-- Table structure for table `like_comment`
--

CREATE TABLE `like_comment` (
  `id` int(255) NOT NULL,
  `comment_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `like_comment`
--

INSERT INTO `like_comment` (`id`, `comment_id`, `user_id`) VALUES
(1, 9, 48),
(3, 8, 48);

-- --------------------------------------------------------

--
-- Table structure for table `like_post`
--

CREATE TABLE `like_post` (
  `id` int(255) NOT NULL,
  `post_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `like_post`
--

INSERT INTO `like_post` (`id`, `post_id`, `user_id`) VALUES
(4, 20, 48);

-- --------------------------------------------------------

--
-- Table structure for table `login_token`
--

CREATE TABLE `login_token` (
  `id` varchar(255) NOT NULL,
  `user_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_token`
--

INSERT INTO `login_token` (`id`, `user_id`) VALUES
('0b8c7723-d258-4977-9252-7531606b144425112019102547268', 59),
('245d2c2e-f51a-4694-b315-2753060d20c23011201916558232', 48),
('2534f860-3375-4692-ac18-00c1a4b633886112019175433382', 49),
('2ec65fb5-9e36-456d-88d7-4ffa058ae4222911201994252448', 62),
('3355951b-ebbb-440e-ab29-b9ec525f53f42911201993221859', 58),
('687ce212-2319-4755-a0f7-2bf644e9cd822211201913563265', 56),
('c3237080-7bb8-4297-81ce-877b326768601111201916360189', 55),
('c654eb76-0be8-4db4-98d8-bf32a6737f926112019175439652', 50),
('dd975291-1843-4646-b01e-04bf6df2caaa22112019947864', 51),
('e2a9db73-ffee-43f9-8cf4-94e5cd4df9b22411201922182842', 57),
('e3d3d9bd-3f2e-4b17-a71a-689ed47a8d9025112019222323476', 53),
('fe2d304f-e933-49ce-8cca-3ce2bc01bb6110112019124315475', 54);

-- --------------------------------------------------------

--
-- Table structure for table `profile_setting`
--

CREATE TABLE `profile_setting` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `bio` varchar(255) DEFAULT NULL,
  `lives` varchar(255) DEFAULT NULL,
  `come_from` varchar(255) DEFAULT NULL,
  `work_place` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `relationship_status` varchar(255) DEFAULT NULL,
  `hobbies` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_setting`
--

INSERT INTO `profile_setting` (`id`, `user_id`, `bio`, `lives`, `come_from`, `work_place`, `education`, `relationship_status`, `hobbies`, `birthday`, `email`) VALUES
(27, 48, 'friend', 'friend', 'friend', 'friend', 'friend', 'friend', 'friend', 'friend', 'friend');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `username`, `password`, `firstname`, `lastname`, `birthday`, `gender`) VALUES
(48, '1@1', 'sam.nob', '$2b$10$CGokdXLlpINfl7vhZ1Gos.as/Vsk/u/9cnE/QZV7V72TgUfn6UTPu', 'sam', 'nob', '11/11/11', 'male'),
(49, '2@2', 'sam.nob2', '$2b$10$4JvfY05u7uhP9qq3tiIhZ.FAw.wYIlu9KL.GShWVU6D4qO4OzLKnq', 'sam', 'nob2', '22/22/22', 'male'),
(50, '3@3', 'sam.c', '$2b$10$L9KRgRJpG/QMHCUSv5O15OCpgF0Qqsd8TNCRX18uv2k7Ssq8mlLXu', 'sam', 'c', '11/11/11', 'M'),
(51, '4@4', 'sam.c2', '$2b$10$YS0Ccgx4xKE50.04A5e1RuXsAPb2MJbKl3hhseV3TzunVfuL7WqJC', 'sam', 'c2', '11/11/11', 'M'),
(53, 'a@a', 'a.a', '$2b$10$dxhtE3eSt66Zx/lWLXABI.EiIYrIfepL4Kd60cCgsa9fIb/NpdFc2', 'a', 'a', '99/99/99', 'ไม่บอก'),
(54, 'b@b', 'b.b', '$2b$10$K1TDRlU4/zB4N9inrvYKkuMLQntOMETrjXxqQlv9jFhKsgUhAOz0S', 'b', 'b', '11/11/11222', 'M'),
(55, 'c@c', 'c.c', '$2b$10$iJKFe7XOjPn4VswTUiEjlO0pDrGIt7Wo7MIOXOffJxQA9b7fZyIby', 'c', 'c', '11/77/444', 'm'),
(56, 'z@z', 'ab.normal', '$2b$10$5xgsMNKsPzc8onyZaZxOqu5nbQDWG7/xSxSQng9CJ8yUV/5a24Bse', 'ab', 'normal', '99/99/99', 'men'),
(57, '007@gmail.com', '007.007', '$2b$10$HAh1CNI/3d97I3c5RtnzRenvLDDRNPb.HSv64odFkqU0lLAJfq/i6', '007', '007', '11/11/11', 'men'),
(58, '11@11', '11.11', '$2b$10$IIphA3D9uVviUnF2GqtuX.4kWqsCDllkiZsFl0zMypXPBGBEyZaGq', '11', '11', '11/11/11', 'M'),
(59, '22@22', '22.22', '$2b$10$6DxllZFJvwWki9ysyio0YuFzFpWqTnbhBJEu4BSHHMrf6/Aq0fb7i', '22', '22', '22/22/22', 'M'),
(60, '33@33', '33.33', '$2b$10$T3Rey8FeZoafF18wnXywrOz.1B8Ia75tHsG9ACmgRi0EuNh3pw5ZS', '33', '33', '11/11/2003', 'M'),
(61, '888@888', '888.888', '$2b$10$Sspv/u23HpG6kFgZTC8BGuVzHKbUunzFqBXOkZgNEDWpCYHmab.vm', '888', '888', '2019-12-31', 'female'),
(62, '99@99', '99.999', '$2b$10$/hLpkN1pFoKCIOqAlLux2uOItVs7KjfO.g7RrCvEffFZEqR9NnLNO', '99', '999', '2019-12-31', 'female');

-- --------------------------------------------------------

--
-- Table structure for table `user_cover_picture`
--

CREATE TABLE `user_cover_picture` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `cover_picture_url` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_cover_picture`
--

INSERT INTO `user_cover_picture` (`id`, `user_id`, `cover_picture_url`, `created_at`) VALUES
(1, 48, '/user_cover/4d9af45c-57b9-424d-ab56-83832a3aef3d231120192142323682.jpg', '2019 December 23 at 21:42'),
(2, 48, '/user_cover/1e7b53c3-aa47-45ea-a25d-3ec5ad6c0d8a23112019214334862.jpg', '2019 December 23 at 21:43'),
(3, 54, '/user_cover/426ba2db-3c90-4f5a-80a8-54ed761f939d231120192145337602.jpg', '2019 December 23 at 21:45'),
(4, 54, '/user_cover/2ca27278-ac3a-42b4-b3b1-e1f23d1ad9ff231120192145362932.jpg', '2019 December 23 at 21:45'),
(5, 54, '/user_cover/45f9ac0b-7439-4444-bde2-7cbb3321833f231120192145401152.jpg', '2019 December 23 at 21:45');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `bio` varchar(255) NOT NULL,
  `lives` varchar(255) NOT NULL,
  `come_from` varchar(255) NOT NULL,
  `work_place` varchar(255) NOT NULL,
  `education` varchar(255) NOT NULL,
  `relationship_status` varchar(255) NOT NULL,
  `hobbies` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`id`, `user_id`, `bio`, `lives`, `come_from`, `work_place`, `education`, `relationship_status`, `hobbies`) VALUES
(15, 53, '1', '2', '3', '4', '5', '6', '7'),
(16, 51, '', '', '', '', '', '', ''),
(17, 52, 'dfsdfs', 'dsfdfs', 'dgfsdgfs', 'dgffdg', '', '', ''),
(25, 56, '1', '2', '3', '4', '5', '6', '7'),
(26, 55, 'bioha', '', '', '', '', '', ''),
(27, 48, 'bio', 'lives', 'come from', 'work place', 'education', 'relationship', 'hobbies');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile_picture`
--

CREATE TABLE `user_profile_picture` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `profile_picture_url` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_profile_picture`
--

INSERT INTO `user_profile_picture` (`id`, `user_id`, `profile_picture_url`, `created_at`) VALUES
(5, 48, '/user_profile/0693f358-c150-4888-87c4-615d7d54d89d231120192142323681.jpg', '2019 December 23 at 21:42'),
(6, 48, '/user_profile/a616cfd4-a518-45a5-b445-6e541c99f36623112019214334861.jpg', '2019 December 23 at 21:43'),
(7, 54, '/user_profile/d307ab1c-af54-4b39-8913-10ecb9ee339f231120192145337601.jpg', '2019 December 23 at 21:45'),
(8, 54, '/user_profile/18e39883-f9c2-4b5d-a7f2-ce18eb9d03ff231120192145362931.jpg', '2019 December 23 at 21:45'),
(9, 54, '/user_profile/697f8f9f-a75d-4217-b703-59425d92b123231120192145401151.jpg', '2019 December 23 at 21:45'),
(10, 50, '/user_profile/803fb12f-6622-4ba5-8f78-0c0512f3c7242511201910248147ดาวน์โหลด.jpg', '2019 December 25 at 10:24'),
(11, 48, '/user_profile/0c8176f1-676d-4d3d-a76c-4f5ea385faef2911201993617879shot(10-05-19)15;36;56.jpg', '2019 December 29 at 9:36'),
(12, 48, '/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg', '2019 December 29 at 9:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `feed_comment`
--
ALTER TABLE `feed_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feed_post`
--
ALTER TABLE `feed_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feed_post_setting`
--
ALTER TABLE `feed_post_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friend_list`
--
ALTER TABLE `friend_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friend_list_setting`
--
ALTER TABLE `friend_list_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friend_request`
--
ALTER TABLE `friend_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `like_comment`
--
ALTER TABLE `like_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `like_post`
--
ALTER TABLE `like_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_token`
--
ALTER TABLE `login_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_setting`
--
ALTER TABLE `profile_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_cover_picture`
--
ALTER TABLE `user_cover_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_profile_picture`
--
ALTER TABLE `user_profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `feed_comment`
--
ALTER TABLE `feed_comment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `feed_post`
--
ALTER TABLE `feed_post`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `feed_post_setting`
--
ALTER TABLE `feed_post_setting`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `friend_list`
--
ALTER TABLE `friend_list`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `friend_list_setting`
--
ALTER TABLE `friend_list_setting`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `friend_request`
--
ALTER TABLE `friend_request`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `like_comment`
--
ALTER TABLE `like_comment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `like_post`
--
ALTER TABLE `like_post`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `profile_setting`
--
ALTER TABLE `profile_setting`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `user_cover_picture`
--
ALTER TABLE `user_cover_picture`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `user_profile_picture`
--
ALTER TABLE `user_profile_picture`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
