-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2020 at 07:43 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codecamp3_project`
--

-- --------------------------------------------------------
create database codecamp3_project;
use codecamp3_project;
--
-- Table structure for table `feed_comment`
--

CREATE TABLE `feed_comment` (
  `id` int(255) NOT NULL,
  `post_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `picture_url` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `likes` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feed_comment`
--

INSERT INTO `feed_comment` (`id`, `post_id`, `user_id`, `picture_url`, `comment`, `created_at`, `likes`) VALUES
(47, 52, 48, '', '1111', '1579510877293', '2'),
(48, 52, 48, '/feed_comment/a3d7a874-d4e5-426f-98bb-d3fd56c20c6615795108830911.jpg', '1111', '1579510883091', '0'),
(49, 52, 48, '/feed_comment/c386a16c-5ebe-4fcb-a90b-5c0469879c0b15795108868851.jpg', '', '1579510886885', '0'),
(50, 52, 50, '', 'xxxxxxx', '1579511039773', '0'),
(51, 52, 50, '/feed_comment/1f431cc0-4b60-4a03-b69c-f6e8e0eb61ac15795110456681.jpg', 'yyyy', '1579511045668', '0'),
(52, 52, 50, '/feed_comment/321c83d4-4b15-4275-a35d-92c9238a4c8615795110640241.jpg', '', '1579511064024', '0'),
(55, 55, 48, '/feed_comment/08dd4e0b-1f9c-4018-8f57-118f4381642115795134383051.jpg', '48', '1579513438305', '2');

-- --------------------------------------------------------

--
-- Table structure for table `feed_post`
--

CREATE TABLE `feed_post` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `picture_url` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `privacy` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `likes` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feed_post`
--

INSERT INTO `feed_post` (`id`, `user_id`, `picture_url`, `caption`, `privacy`, `created_at`, `likes`) VALUES
(52, 48, '/feed_post/3b1f5259-0390-45ec-8926-0a70b9ac818115795103648031.jpg', 'id 48 post1', 'public', '1579510364803', '2'),
(53, 48, '', 'id 48 post 2', 'friend', '1579510405075', '0'),
(54, 48, '/feed_post/c6fdabbd-7468-4f53-a80e-c64a40d5225315795104125641.jpg', '4', 'public', '1580191527409', '0'),
(55, 49, '/feed_post/9635f1eb-7608-403a-a65a-5e73ba48fc3515795104593671.jpg', 'id 49 post 1', 'public', '1579510459367', '1'),
(57, 49, '/feed_post/3aeb9d87-1076-4bfd-9f40-06e04807b13615795105137041.jpg', '', 'friend', '1579510513704', '0'),
(58, 49, '', 'id 49 post3', 'only me', '1579510537013', '0'),
(59, 48, '', 'beckham', 'public', '1580191638354', '0'),
(60, 48, '', 'assasin cri', 'public', '1580191710043', '0'),
(61, 49, '/feed_post/06c4568c-b4bf-45fc-824c-e58c17ff518415801937231021.jpg', 'dsadsadsadsadasdas', 'public', '1580193723102', '0');

-- --------------------------------------------------------

--
-- Table structure for table `feed_post_setting`
--

CREATE TABLE `feed_post_setting` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `privacy` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feed_post_setting`
--

INSERT INTO `feed_post_setting` (`id`, `user_id`, `privacy`) VALUES
(2, 55, 'friend'),
(4, 48, 'friend'),
(5, 62, 'public');

-- --------------------------------------------------------

--
-- Table structure for table `friend_list`
--

CREATE TABLE `friend_list` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `friend_user_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `friend_list`
--

INSERT INTO `friend_list` (`id`, `user_id`, `friend_user_id`) VALUES
(60, 58, 59),
(61, 59, 58),
(62, 48, 49),
(63, 49, 48),
(64, 58, 54),
(65, 54, 58),
(66, 58, 50),
(67, 50, 58),
(68, 58, 48),
(69, 48, 58),
(70, 58, 55),
(72, 55, 58),
(73, 59, 54),
(74, 54, 59),
(75, 59, 50),
(76, 50, 59),
(77, 59, 48),
(78, 48, 59),
(79, 59, 55),
(80, 55, 59);

-- --------------------------------------------------------

--
-- Table structure for table `friend_list_setting`
--

CREATE TABLE `friend_list_setting` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `privacy` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `friend_request`
--

CREATE TABLE `friend_request` (
  `id` int(255) NOT NULL,
  `request_user_id` int(255) NOT NULL,
  `response_user_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `friend_request`
--

INSERT INTO `friend_request` (`id`, `request_user_id`, `response_user_id`) VALUES
(15, 54, 51),
(18, 49, 53);

-- --------------------------------------------------------

--
-- Table structure for table `like_comment`
--

CREATE TABLE `like_comment` (
  `id` int(255) NOT NULL,
  `comment_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `like_comment`
--

INSERT INTO `like_comment` (`id`, `comment_id`, `user_id`) VALUES
(15, 47, 48),
(16, 55, 48),
(17, 55, 62),
(18, 47, 62);

-- --------------------------------------------------------

--
-- Table structure for table `like_post`
--

CREATE TABLE `like_post` (
  `id` int(255) NOT NULL,
  `post_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `like_post`
--

INSERT INTO `like_post` (`id`, `post_id`, `user_id`) VALUES
(15, 52, 48),
(16, 52, 62),
(17, 55, 48);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `post_id` int(255) DEFAULT NULL,
  `comment_id` int(255) DEFAULT NULL,
  `post_owner_user_id` int(255) NOT NULL,
  `comment_owner_user_id` int(255) DEFAULT NULL,
  `action_user_id` int(255) NOT NULL,
  `see_status` varchar(255) DEFAULT NULL,
  `read_status` varchar(255) DEFAULT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `type`, `post_id`, `comment_id`, `post_owner_user_id`, `comment_owner_user_id`, `action_user_id`, `see_status`, `read_status`, `created_at`) VALUES
(30, 'comment', 52, 47, 48, 48, 48, NULL, NULL, '1579510877293'),
(31, 'comment', 52, 48, 48, 48, 48, NULL, NULL, '1579510883091'),
(32, 'comment', 52, 49, 48, 48, 48, NULL, NULL, '1579510886885'),
(33, 'comment', 52, 50, 48, 50, 50, NULL, NULL, '1579511039773'),
(34, 'comment', 52, 51, 48, 50, 50, NULL, NULL, '1579511045668'),
(35, 'comment', 52, 52, 48, 50, 50, NULL, NULL, '1579511064024'),
(38, 'comment', 55, 55, 49, 48, 48, NULL, NULL, '1579513438305'),
(39, 'like_post', 52, NULL, 48, NULL, 62, NULL, NULL, '1579513570172'),
(40, 'like_post', 55, NULL, 49, NULL, 48, NULL, NULL, '1579513610156'),
(41, 'like_comment', 55, 55, 49, 48, 62, NULL, NULL, '1579513933836'),
(42, 'like_comment', 52, 47, 48, 48, 62, NULL, 'yes', '1579514031245');

-- --------------------------------------------------------

--
-- Table structure for table `profile_setting`
--

CREATE TABLE `profile_setting` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `bio` varchar(255) DEFAULT NULL,
  `lives` varchar(255) DEFAULT NULL,
  `come_from` varchar(255) DEFAULT NULL,
  `work_place` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `relationship_status` varchar(255) DEFAULT NULL,
  `hobbies` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_setting`
--

INSERT INTO `profile_setting` (`id`, `user_id`, `bio`, `lives`, `come_from`, `work_place`, `education`, `relationship_status`, `hobbies`, `birthday`, `email`) VALUES
(27, 48, 'only me', 'friend', 'friend', 'friend', 'friend', 'friend', 'friend', 'friend', 'friend');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `username`, `password`, `firstname`, `lastname`, `birthday`, `gender`) VALUES
(48, '1@1', 'sam.nob', '$2b$10$CGokdXLlpINfl7vhZ1Gos.as/Vsk/u/9cnE/QZV7V72TgUfn6UTPu', 'sam', 'nob', '11/11/11', 'male'),
(49, '2@2', 'sam.nob2', '$2b$10$4JvfY05u7uhP9qq3tiIhZ.FAw.wYIlu9KL.GShWVU6D4qO4OzLKnq', 'sam', 'nob2', '22/22/22', 'male'),
(50, '3@3', 'sam.c', '$2b$10$L9KRgRJpG/QMHCUSv5O15OCpgF0Qqsd8TNCRX18uv2k7Ssq8mlLXu', 'sam', 'c', '11/11/11', 'M'),
(51, '4@4', 'sam.c2', '$2b$10$YS0Ccgx4xKE50.04A5e1RuXsAPb2MJbKl3hhseV3TzunVfuL7WqJC', 'sam', 'c2', '11/11/11', 'M'),
(53, 'a@a', 'a.a', '$2b$10$dxhtE3eSt66Zx/lWLXABI.EiIYrIfepL4Kd60cCgsa9fIb/NpdFc2', 'a', 'a', '99/99/99', 'ไม่บอก'),
(54, 'b@b', 'b.b', '$2b$10$K1TDRlU4/zB4N9inrvYKkuMLQntOMETrjXxqQlv9jFhKsgUhAOz0S', 'b', 'b', '11/11/11222', 'M'),
(55, 'c@c', 'c.c', '$2b$10$iJKFe7XOjPn4VswTUiEjlO0pDrGIt7Wo7MIOXOffJxQA9b7fZyIby', 'c', 'c', '11/77/444', 'm'),
(56, 'z@z', 'ab.normal', '$2b$10$5xgsMNKsPzc8onyZaZxOqu5nbQDWG7/xSxSQng9CJ8yUV/5a24Bse', 'ab', 'normal', '99/99/99', 'men'),
(57, '007@gmail.com', '007.007', '$2b$10$HAh1CNI/3d97I3c5RtnzRenvLDDRNPb.HSv64odFkqU0lLAJfq/i6', '007', '007', '11/11/11', 'men'),
(58, '11@11', '11.11', '$2b$10$IIphA3D9uVviUnF2GqtuX.4kWqsCDllkiZsFl0zMypXPBGBEyZaGq', '11', '11', '11/11/11', 'M'),
(59, '22@22', '22.22', '$2b$10$6DxllZFJvwWki9ysyio0YuFzFpWqTnbhBJEu4BSHHMrf6/Aq0fb7i', '22', '22', '22/22/22', 'M'),
(60, '33@33', '33.33', '$2b$10$T3Rey8FeZoafF18wnXywrOz.1B8Ia75tHsG9ACmgRi0EuNh3pw5ZS', '33', '33', '11/11/2003', 'M'),
(61, '888@888', '888.888', '$2b$10$Sspv/u23HpG6kFgZTC8BGuVzHKbUunzFqBXOkZgNEDWpCYHmab.vm', '888', '888', '2019-12-31', 'female'),
(62, '99@99', '99.999', '$2b$10$/hLpkN1pFoKCIOqAlLux2uOItVs7KjfO.g7RrCvEffFZEqR9NnLNO', '99', '999', '2019-12-31', 'female'),
(63, 's@c', 'super.contra', '$2b$10$ePzj5qOXP1U1XOzXLXDiJOyTLzs4e2/MAT03EpOTRDg9HMuvKwg/a', 'super', 'contra', 'dasdas', 'dasdas'),
(64, 's@c1', 'contra.super', '$2b$10$ixxD05GWAqPl6GYdYq8H1.gxIEknfxhNYgRvYcJ8HiQObKFko6KZ.', 'contra', 'super', 'dasdas', 'dasdas'),
(65, 's@c12211', 'contra.contra', '$2b$10$Oiu2SdaETkiyq3uIRmGrJezYHdK0GAhjfJ53eu2PagNeuxIUhEAOK', 'contra', 'contra', 'dasdas', 'dasdas'),
(66, 's@c12211araerae', 'super.super', '$2b$10$ifeR/mymoe4QQ1IF3.1Q7uruEvJKnL4ZiLtSB45/iekMN6xzarmvq', 'super', 'super', 'dasdas', 'dasdas'),
(67, 's@c12211araerae33', 'super.1', '$2b$10$ecg8q7oK.zjSL4PEpIfWveshvSoXt7zetKbBS9c.6BJG.tnDN35Me', 'super', '1', 'dasdas', 'dasdas'),
(68, 's@c12211araerae33rew', 'super.2', '$2b$10$mKjx0o8z8leBsi.A7v2L2eTGE4L0YBJ7cLOgtVLUgQWfPPU1wDvum', 'super', '2', 'dasdas', 'dasdas'),
(69, 's@c12211araerae33rew1', 'contra.1', '$2b$10$CXjpzc0Im3Er6QReCcTbx.nA2F0ysdkdhT1el7RuwLZTFOUVh8M.6', 'contra', '1', 'dasdas', 'dasdas'),
(70, 's@c12211araerae33rew11', 'contra.2', '$2b$10$7jr2acnvNMjw1hcglhO3tuDJk4PoYBJdpXD1Pd9VyRe63ss3cn4f6', 'contra', '2', 'dasdas', 'dasdas'),
(71, 's@c12211araerae33rdassdew11', '1.super', '$2b$10$8d.4nRV2wR00GcCFI7fRveC.oYtyXrrLnpvRophnPxH9ouElSERHe', '1', 'super', 'dasdas', 'dasdas'),
(72, 's@c12211araerae33rdassddasew11', '2.super', '$2b$10$pLOg6tje31cV3ZPxBgJwDebgVH.T/GRlJPBk/Q7RSEZcgz9IOtRja', '2', 'super', 'dasdas', 'dasdas'),
(73, '77@dfsdfdxs', '1.contra', '$2b$10$XjP/s09KWE..hA.KMGD2LurEc3HzL4NvTv8warmnED4VJGg.XReXC', '1', 'contra', 'dasdas1', 'dasdas'),
(74, '77@dfsdfdxsdas', '2.contra', '$2b$10$7.UgKW5qRJLsFwsa0436qeDfQX5AB0saIgJ9Ygg8rQNPeWONPjU2u', '2', 'contra', 'dasdas1', 'dasdas'),
(75, 'aa@aa', 'aa.aa', '$2b$10$HL2rvQ3lETmoaIevjyuatuFa1yn5r1aHVa79Tiq87xpBkUtFOv.r6', 'aa', 'aa', 'dasdas', 'cdas');

-- --------------------------------------------------------

--
-- Table structure for table `user_cover_picture`
--

CREATE TABLE `user_cover_picture` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `cover_picture_url` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_cover_picture`
--

INSERT INTO `user_cover_picture` (`id`, `user_id`, `cover_picture_url`, `created_at`) VALUES
(1, 48, '/user_cover/4d9af45c-57b9-424d-ab56-83832a3aef3d231120192142323682.jpg', '2019 December 23 at 21:42'),
(2, 48, '/user_cover/1e7b53c3-aa47-45ea-a25d-3ec5ad6c0d8a23112019214334862.jpg', '2019 December 23 at 21:43'),
(3, 54, '/user_cover/426ba2db-3c90-4f5a-80a8-54ed761f939d231120192145337602.jpg', '2019 December 23 at 21:45'),
(4, 54, '/user_cover/2ca27278-ac3a-42b4-b3b1-e1f23d1ad9ff231120192145362932.jpg', '2019 December 23 at 21:45'),
(5, 54, '/user_cover/45f9ac0b-7439-4444-bde2-7cbb3321833f231120192145401152.jpg', '2019 December 23 at 21:45'),
(6, 59, '/user_cover/7ba44202-dafc-4ad1-a0f9-84c564ad4b3a15786741726882.jpg', '1578674172688'),
(7, 59, '/user_cover/6f29d3f0-6a39-4761-84f9-a6ccbe6edf1215786742033212.jpg', '1578674203321'),
(8, 62, '/user_cover/1da8b884-7a1b-4ef8-be85-83f991e8aaf115786742481882.jpg', '1578674248188'),
(9, 62, '/user_cover/e906136f-5abd-494c-94e1-ab51a594813a15786742751042.jpg', '1578674275104'),
(10, 62, '/user_cover/7ca2679e-0c75-4317-a429-137681657c7f15786742939552.jpg', '1578674293955'),
(11, 62, '/user_cover/b5216f3a-3c61-4f65-91d1-bdce94fcd74f15786743148282.jpg', '1578674314828'),
(12, 59, '/user_cover/56b4ef26-5918-4aad-9dec-3a8ffa6c325615786743919292.jpg', '1578674391929'),
(13, 59, '/user_cover/6a03d0ab-6463-4155-96ab-2cb13d1308e015786743996842.jpg', '1578674399684'),
(14, 48, '/user_cover/c9447bcd-ed4f-4a68-a093-0f14af3febcc15786747073742.jpg', '1578674707374');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `bio` varchar(255) NOT NULL,
  `lives` varchar(255) NOT NULL,
  `come_from` varchar(255) NOT NULL,
  `work_place` varchar(255) NOT NULL,
  `education` varchar(255) NOT NULL,
  `relationship_status` varchar(255) NOT NULL,
  `hobbies` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`id`, `user_id`, `bio`, `lives`, `come_from`, `work_place`, `education`, `relationship_status`, `hobbies`) VALUES
(15, 53, '1', '2', '3', '4', '5', '6', '7'),
(16, 51, '', '', '', '', '', '', ''),
(17, 52, 'dfsdfs', 'dsfdfs', 'dgfsdgfs', 'dgffdg', '', '', ''),
(25, 56, '1', '2', '3', '4', '5', '6', '7'),
(26, 55, 'bioha', '', '', '', '', '', ''),
(27, 48, '1ssss', '2', '3', '4', '5', '6', '7'),
(29, 59, '1ssss', '2', '3', '4', '5', '6', '7'),
(30, 62, '1ssss', '2', '3', '4', '5', '6', '7'),
(31, 49, 'ggh\ngrdcdgfdgfdgfdgf\n', '', 'dgfdgfdgfdgfdgf\n', 'man 99999\n', 'fddfsdfdfs\n', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile_picture`
--

CREATE TABLE `user_profile_picture` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `profile_picture_url` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_profile_picture`
--

INSERT INTO `user_profile_picture` (`id`, `user_id`, `profile_picture_url`, `created_at`) VALUES
(5, 48, '/user_profile/0693f358-c150-4888-87c4-615d7d54d89d231120192142323681.jpg', '2019 December 23 at 21:42'),
(6, 48, '/user_profile/a616cfd4-a518-45a5-b445-6e541c99f36623112019214334861.jpg', '2019 December 23 at 21:43'),
(7, 54, '/user_profile/d307ab1c-af54-4b39-8913-10ecb9ee339f231120192145337601.jpg', '2019 December 23 at 21:45'),
(8, 54, '/user_profile/18e39883-f9c2-4b5d-a7f2-ce18eb9d03ff231120192145362931.jpg', '2019 December 23 at 21:45'),
(9, 54, '/user_profile/697f8f9f-a75d-4217-b703-59425d92b123231120192145401151.jpg', '2019 December 23 at 21:45'),
(10, 50, '/user_profile/803fb12f-6622-4ba5-8f78-0c0512f3c7242511201910248147ดาวน์โหลด.jpg', '2019 December 25 at 10:24'),
(11, 48, '/user_profile/0c8176f1-676d-4d3d-a76c-4f5ea385faef2911201993617879shot(10-05-19)15;36;56.jpg', '2019 December 29 at 9:36'),
(12, 48, '/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg', '2019 December 29 at 9:36'),
(13, 59, '/user_profile/91da303f-1653-4196-9240-c18e42ae120c15786741726881.jpg', '1578674172688'),
(14, 59, '/user_profile/30a5e38f-0b27-403f-adc1-31ccba813ab715786742033211.jpg', '1578674203321'),
(15, 62, '/user_profile/e6f09ada-dc35-4cb5-83f0-aa1976c400bf15786742481881.jpg', '1578674248188'),
(16, 62, '/user_profile/8c43ea9f-5f33-4e7a-bd0f-a4031951814115786742751041.jpg', '1578674275104'),
(17, 62, '/user_profile/24584b95-18e6-4e1c-b0be-2b2a30ff19b415786742939551.jpg', '1578674293955'),
(18, 62, '/user_profile/3a0c3840-ff32-4c9d-be3a-0a4594359e7f15786743148281.jpg', '1578674314828'),
(19, 59, '/user_profile/fbdbd8e0-a177-4e34-b92d-b4ca975540c815786743919291.jpg', '1578674391929'),
(20, 59, '/user_profile/64b4e5ed-e0ad-48e4-8cc1-1a517c547f1515786743996841.jpg', '1578674399684'),
(21, 48, '/user_profile/1afba9e9-9488-4878-b902-0b194b8967f515786747073741.jpg', '1578674707374'),
(22, 51, '/user_profile/b293dc73-1b27-4361-acf3-addf778981ce1579505679503ดาวน์โหลด.jpg', '1579505679503'),
(23, 51, '/user_profile/57c1ba98-f900-478a-a8bc-9ca95984a39e1579505694528fffff.jpg', '1579505694528'),
(24, 49, '/user_profile/f5564ee0-b3c6-45fb-9c62-b1e2d0b015291580193731642images.png', '1580193731642');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `feed_comment`
--
ALTER TABLE `feed_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feed_post`
--
ALTER TABLE `feed_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feed_post_setting`
--
ALTER TABLE `feed_post_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friend_list`
--
ALTER TABLE `friend_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friend_list_setting`
--
ALTER TABLE `friend_list_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friend_request`
--
ALTER TABLE `friend_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `like_comment`
--
ALTER TABLE `like_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `like_post`
--
ALTER TABLE `like_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_setting`
--
ALTER TABLE `profile_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_cover_picture`
--
ALTER TABLE `user_cover_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_profile_picture`
--
ALTER TABLE `user_profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `feed_comment`
--
ALTER TABLE `feed_comment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `feed_post`
--
ALTER TABLE `feed_post`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `feed_post_setting`
--
ALTER TABLE `feed_post_setting`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `friend_list`
--
ALTER TABLE `friend_list`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `friend_list_setting`
--
ALTER TABLE `friend_list_setting`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `friend_request`
--
ALTER TABLE `friend_request`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `like_comment`
--
ALTER TABLE `like_comment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `like_post`
--
ALTER TABLE `like_post`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `profile_setting`
--
ALTER TABLE `profile_setting`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `user_cover_picture`
--
ALTER TABLE `user_cover_picture`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `user_profile_picture`
--
ALTER TABLE `user_profile_picture`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
