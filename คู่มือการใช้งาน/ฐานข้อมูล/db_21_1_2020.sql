-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2020 at 04:47 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codecamp3_project`
--

-- --------------------------------------------------------
create database codecamp3_project;
use codecamp3_project;
--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `post_id` int(255) DEFAULT NULL,
  `comment_id` int(255) DEFAULT NULL,
  `post_owner_user_id` int(255) NOT NULL,
  `comment_owner_user_id` int(255) DEFAULT NULL,
  `action_user_id` int(255) NOT NULL,
  `see_status` varchar(255) DEFAULT NULL,
  `read_status` varchar(255) DEFAULT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `type`, `post_id`, `comment_id`, `post_owner_user_id`, `comment_owner_user_id`, `action_user_id`, `see_status`, `read_status`, `created_at`) VALUES
(30, 'comment', 52, 47, 48, 48, 48, NULL, NULL, '1579510877293'),
(31, 'comment', 52, 48, 48, 48, 48, NULL, NULL, '1579510883091'),
(32, 'comment', 52, 49, 48, 48, 48, NULL, NULL, '1579510886885'),
(33, 'comment', 52, 50, 48, 50, 50, NULL, NULL, '1579511039773'),
(34, 'comment', 52, 51, 48, 50, 50, NULL, NULL, '1579511045668'),
(35, 'comment', 52, 52, 48, 50, 50, NULL, NULL, '1579511064024'),
(38, 'comment', 55, 55, 49, 48, 48, NULL, NULL, '1579513438305'),
(39, 'like_post', 52, NULL, 48, NULL, 62, NULL, NULL, '1579513570172'),
(40, 'like_post', 55, NULL, 49, NULL, 48, NULL, NULL, '1579513610156'),
(41, 'like_comment', 55, 55, 49, 48, 62, NULL, NULL, '1579513933836'),
(42, 'like_comment', 52, 47, 48, 48, 62, NULL, 'yes', '1579514031245');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
