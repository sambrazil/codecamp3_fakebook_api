# Fakebook API (52 API)




## 1. register
* คำอธิบาย :  ใช้สมัคร accound fakebook
* url : http://localhost:3001/api/register
* method : post 
* input :
  {

      firstname : ' ',
      lastname : ' ', 
      password : ' ',
      email : ' ',
      birthday : ' ',
      gender : ' '
  }
* output
  * status 400
    * {"status":"plase input all data"} // กรอกข้อมูลไม่ครบ
    * {"status":"email already exit"} // มีคนใช้ email นี้สมัครแล้ว
  * status 200
    * {"status":"register complete","email":"1@1dfdfgdsdgsdsdaas"}
// register สำเร็จ



 ## 2. login
* คำอธิบาย :  ใช้ตรวจสอบ email, password ที่หน้า login
* url : http://localhost:3001/api/login
* method : post 
* input :
  
  {
    
    email : ' ', password : ' '  
    
     }
* output
  * status 400
    * {"status":"wrong email"}  //ใส่ email ผิด
    * {"status":"wrong password"} //ใส่ password ผิด
  * status 200
     * {"status":"logged in","id":"48",
"email":"1@1","firstname":"sam","lastname":"nob","birthday":"11/11/11","gender":"m","
username":"sam.nob","profile_picture_url":"http://localhost:3001/user_profile/31c059bf-4e7c-437b-a402-35099135140f301020191341235111.jpg",
"bio":"ข้อความแนะนำตัวนะครับ"} 
// login สำเร็จ


## 3. logout
* คำอธิบาย :  ลบค่า session
* url : http://localhost:3001/api/logout
* method : post 
* input : {       }
* output
  
  * status 200
    * {"status":"logout complete"}


## 4. search
* คำอธิบาย :  ใช้ค้นหาบุคคล โดยใส่ชื่อเข้าไปใน form ค้นหาข้อมูล แล้วยิงมาที่apiนี้
* url : http://localhost:3001/api/post/search
* method : post
* input : {  

          fullname : ' ', //คือชื่อของคนที่เราจะค้นหา วิธีค้นหามี2วิธี
                     // 1. ชื่อจริง เว้นวรรค(เคาะspacebar 1ที เท่านั้น , ถ้าเกินจะหานามสกุลไม่เจอ) นามสกุล เช่น sam nob  
                     // 2. ชื่อจริง ไม่ใส่นามสกุล (ห้ามเคาะspacbarก่อนพิมพ์ จะstatus 400) เช่น sam

          start_row : ' ', // แถวเริ่มต้นข้อมูล มีค่าเริ่มต้นที่1
          per_page : ' ',  // จำนวนข้อมูลที่ให้แสดงต่อหน้า
      
      
       } 
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {"status":"wrong row start. It begin with 1"}
    * {status: 'please in put fullname or firstname'} // ไม่ได้ใส่ชื่อจริง หรือใส่ข้อมูล
    * {status: 'not found'}  //ค้นหาไม่เจอข้อมูล
  * status 200
    * {"row_number":1,"data":[{"username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"}]}


## 5. album

##  5.1 showCoverPicture
* คำอธิบาย :  เราต้อง login เพื่อดู album รูป cover ของตัวเอง
* url : http://localhost:3001/api/post/show_cover_picture
* method : post
* input  : { 

        start_row:'', //แถวเริ่มต้นของข้อมูล มีค่าเริ่มต้นเป็น 1
       per_page : '' // จำนวนข้อมูลที่ให้แสดงต่อหน้า

        }

  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'wrong row start. It begin with 1'}
    * {status: 'not found cover picture'}
  * status 200
    * {"row_number":2,"current_cover_picture_url":"/user_cover/1e7b53c3-aa47-45ea-a25d-3ec5ad6c0d8a23112019214334862.jpg",
"cover_picture_album":[{"picture_id":2,"username":"sam.nob","firstname":"sam","lastname":"nob",
"cover_picture_url":"/user_cover/1e7b53c3-aa47-45ea-a25d-3ec5ad6c0d8a23112019214334862.jpg",
"created_at":"2019 December 23 at 21:43"},{"picture_id":1,"username":"sam.nob","firstname":"sam",
"lastname":"nob","cover_picture_url":"/user_cover/4d9af45c-57b9-424d-ab56-83832a3aef3d231120192142323682.jpg",
"created_at":"2019 December 23 at 21:42"}]}

## 5.2 showPostPicture
* คำอธิบาย :  เราต้อง login เพื่อดู album รูป post บน timeline ของตัวเอง
* url :  http://localhost:3001/api/post/show_post_picture
* method : post
* input  : { 

        start_row:'', //แถวเริ่มต้นของข้อมูล มีค่าเริ่มต้นเป็น 1
       per_page : '' // จำนวนข้อมูลที่ให้แสดงต่อหน้า
        }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {"status":"not found post picture"}
  * status 200
    * {"row_number":6,
"lastest_post_picture_url":"/feed_post/eb55df6b-6dac-44d5-ab75-8461e578345f31112019134031558shot(10-05-19)15;36;56.jpg",
"post_picture_album":[{"post_id":38,
"picture_url":"/feed_post/eb55df6b-6dac-44d5-ab75-8461e578345f31112019134031558shot(10-05-19)15;36;56.jpg",
"caption":"","created_at":"2019 December 31 at 13:40","likes":"0","privacy":"only me","username":"sam.nob",
"firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":37,"picture_url":"/feed_post/77271926-05e2-46c0-8805-7a3aa16dd0e831112019134023390shot(10-05-19)15;36;56.jpg",
"caption":"","created_at":"2019 December 31 at 13:40","likes":"0","privacy":"friend","username":"sam.nob","firstname":"sam",
"lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"}],
"comment":[{"post_id":38,"comment_number":1},{"post_id":37,"comment_number":0}]}


## 5.3 showProfilePicture
* คำอธิบาย : เราต้อง login เพื่อดู album รูป profile ของตัวเอง 
* url : http://localhost:3001/api/post/show_profile_picture
* method : post
* input  : { 

        start_row:'', //แถวเริ่มต้นของข้อมูล มีค่าเริ่มต้นเป็น 1
       per_page : '' // จำนวนข้อมูลที่ให้แสดงต่อหน้า
        }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'wrong row start. It begin with 1'}
    * {status: 'not found profile picture'}
  * status 200
    * {"row_number":4,
"current_profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg",
"profile_picture_album":[{"picture_id":12,"username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg",
"created_at":"2019 December 29 at 9:36"},{"picture_id":11,"username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/0c8176f1-676d-4d3d-a76c-4f5ea385faef2911201993617879shot(10-05-19)15;36;56.jpg",
"created_at":"2019 December 29 at 9:36"},{"picture_id":6,"username":"sam.nob","firstname":"sam",
"lastname":"nob","profile_picture_url":"/user_profile/a616cfd4-a518-45a5-b445-6e541c99f36623112019214334861.jpg",
"created_at":"2019 December 23 at 21:43"},{"picture_id":5,"username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/0693f358-c150-4888-87c4-615d7d54d89d231120192142323681.jpg",
"created_at":"2019 December 23 at 21:42"}]}


## 6. feed


## 6.1 checkLikePost
* คำอธิบาย :   เช็คว่าเรากด like post ไหนบ้าง
* url : http://localhost:3001/api/post/check_like_post
* method : post
* input  : {
            post_id:' '
          }
  
* output
  * status 404
    * {session:'not found session'}

  * status 400
    * {"status":"not found post"} // post_id ผิด หรือไม่มีในdatabase
  * status 200
    *  {"status":"unlike"} // ไม่พบข้อมูลการ like return status unlike มาให้
    * {"status":"like"} // พบข้อมุลการกด like  return status like มาให้ 


## 6.2 checkLikeComment
* คำอธิบาย :  เช็คว่าเรากด like comment ไหนบ้าง
* url : http://localhost:3001/api/post/check_like_comment
* method : post
* input  :  {
            comment_id:''
          }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {"status":"not found comment"} // comment_id ผิด หรือไม่มีในdatabase
  * status 200
    *  {"status":"unlike"} // ไม่พบข้อมูลการ like return status unlike มาให้
    * {"status":"like"} // พบข้อมุลการกด like  return status like มาให้


## 6.3 createFeedComment
* คำอธิบาย :  add comment ลง database
* url :  http://localhost:3001/api/post/create_feed_comment
* method : post
* input  :{

         post_id: '',
         picture: '',
         comment : ''
        } 
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {"status":"not found this post"} // post_id ผิด
    * {"status":"please input comment or picture"} // ไม่ได้ใส่ข้อมูล
  * status 200
    * {"status":"comment complete"} // เมื่อใส่รูป หรือ comment , ใส่ comment และรูป


## 6.4 createFeedPost
* คำอธิบาย :  add post ลง database
* url :  http://localhost:3001/api/post/create_feed_post
* method : post
* input  : {

             picure: 'abc.jpg'
             caption: 'แคปชั่น',
             privacy: 'public' // ให้ใส่ public, friend, only me
             
        } 
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    *  {"status":"please input caption or picture"}  // ไม่ได้ใส่ข้อมูล
    *   {status: 'wrong privacy, please in put friend, public, only me'}
  * status 200
    * {"status":"post picture complete"} // กรณีใส่รูป แต่ไม่ใส่ caption
    * {"status":"post caption complete"}// ใส่caption แต่ไม่ใส่รูป
    * {"status":"post caption and picture complete"} //ใส่ทั้งรูป และ caption


## 6.5 editComment
* คำอธิบาย : แก้ไข comment 
* url : http://localhost:3001/api/post/edit_comment
* method : post
* input  :  { 
         
         comment_id:'',
         username:'',
         comment: ''
       }
  
* output
  * status 404
    * {session:'not found session'} 
  * status 400
    * {status: 'please input data'}
    * {status: 'not found username'}
    * {status: 'not found comment'}
  * status 200
    * {status: 'edit comment complete'}


## 6.6 editFeedPrivacy
* คำอธิบาย :  ใช้แก้ไข privacy (public, friend, only me) ที่ post ของเรา, แล้ว update ค่าใน database
* url : http://localhost:3001/api/post/edit_post_privacy
* method : post
* input  : { 
    
         post_id:'22',
         username:'sam.nob' //username ของเรา
         privacy : 'only me' // ตั้งค่า privacy ของpost ต้องเป็น public, friend, only me เท่านั้น
      }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {"status":"wrong privacy, please input public, friend, only me"}
    *  {"status":"not found username"}
    * {status: 'not found post'}
  * status 200
    *  {status: 'edit post privacy complete'}


## 6.7 editPost 
* คำอธิบาย :  แก้ไข post
* url : http://localhost:3001/api/post/edit_post
* method : post
* input  : { 
         
         post_id:'',
         username:'',
         caption: '',
       }

  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'please input data'}
    * {status: 'not found username'}
    * {status: 'not found post'}
  * status 200
    * {status: 'edit post complete'}


## 6.8 removeComment 
* คำอธิบาย :  update comment เป็นค่าว่าง เหลือแค่id 
* url : http://localhost:3001/api/post/remove_comment
* method : post
* input  : { 
        
        comment_id:'',
         username:''
       }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found username'}
    *  {status: 'not found comment'}
  * status 200
    * {status: 'remove comment complete'}


## 6.9 removePost 
* คำอธิบาย :  update post เป็นค่าว่าง เหลือแค่id 
* url : http://localhost:3001/api/post/remove_post
* method : post
* input  :  { 
        
        post_id:'',
         username:'',
         picture_url:'' // ส่งเพื่อไปลบรูปใน folder
       }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found username'} 
    * {status: 'not found post'}
  * status 200
    * {status: 'remove post complete'}


## 6.10 showAllFeedPost 
* คำอธิบาย : ที่หน้า all feed, show post ของเราทั้งหมด , และ post ของเพื่อนที่มี privacy เป็น public หรือ friend  
* url : http://localhost:3001/api/post/show_all_feed_post
* method : post
* input  : { 

          start_row :'', // แถวที่เริ่มต้นของข้อมูล, มีค่าเริ่มต้นที่ 1
          per_page:''    // จำนวนข้อมูลที่แสดงต่อหน้า 
       }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found post'}
  * status 200
    * {"row_number":15,"data":[{"post_id":41,"picture_url":"","caption":"friend","created_at":"2020 January 1 at 9:33"
,"likes":"0","privacy":"friend","username":"sam.nob2","firstname":"sam","lastname":"nob2","profile_picture_url":null},
{"post_id":40,"picture_url":"","caption":"public","created_at":"2020 January 1 at 9:32","likes":"0","privacy":"public",
"username":"sam.nob2","firstname":"sam","lastname":"nob2","profile_picture_url":null},
{"post_id":38,"picture_url":"/feed_post/eb55df6b-6dac-44d5-ab75-8461e578345f31112019134031558shot(10-05-19)15;36;56.jpg",
"caption":"","created_at":"2019 December 31 at 13:40","likes":"0","privacy":"only me","username":"sam.nob","firstname":"sam",
"lastname":"nob","profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":37,"picture_url":"/feed_post/77271926-05e2-46c0-8805-7a3aa16dd0e831112019134023390shot(10-05-19)15;36;56.jpg","caption":"",
"created_at":"2019 December 31 at 13:40","likes":"0","privacy":"friend","username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"}],
"comment":[{"post_id":41,"comment_number":0},{"post_id":40,"comment_number":0},{"post_id":38,"comment_number":0},
{"post_id":37,"comment_number":0}]}


## 6.11 showPostById 
* คำอธิบาย :   แสดงข้อมูล post ตาม post_id ดูได้เฉพาะของตัวเอง
* url : http://localhost:3001/api/post/show_post_by_id
* method : post
* input  : { 


         post_id, // post_id ของเราเท่านั้น , ถ้าเป็นของคนอื่นจะดูไม่ได้
          }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found post'} 
  * status 200
    * {"data":[{"post_id":52,"picture_url":"/feed_post/3b1f5259-0390-45ec-8926-0a70b9ac818115795103648031.jpg",
"caption":"id 48 post1","created_at":"1579510364803","likes":"2","privacy":"public","username":"sam.nob",
"firstname":"sam","lastname":"nob","profile_picture_url":"/user_profile/1afba9e9-9488-4878-b902-0b194b8967f515786747073741.jpg"}],
"comment":[{"post_id":52,"comment_number":6}]}


## 6.12 showUserFeedComment 
* คำอธิบาย :  แสดง comment ของแต่ละ post
* url : http://localhost:3001/api/post/show_user_feed_comment
* method : post
* input  : { 

          post_id: '' ,
          start_row:'', // ค่าเริ่มต้นของแถวข้อมูล เริ่มต้นที่1  
          per_page:'' // ให้แสดงจำนวนข้อมูลที่แถวต่อหน้า ถ้าใส่เกินจำนวนถวที่มี apiจะปรับให้เท่ากับจำนวนแถวที่เหลือเอง 
         } 
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {"status":"not found"} // ไม่พบ post_id หรือใส่ start_row ผิด (เกินจำนวนแถวที่มีใน database) 
  * status 200
    *  {"row_number":4,"data":[{"comment_id":2,"post_id":12,"picture_url":"","comment":"44444",
    "created_at":"2019 December 18 at 9:31","likes":"0","username":"sam.nob2","firstname":"sam",
    "lastname":"nob2","profile_picture_url":null},{"comment_id":3,"post_id":12,"picture_url":"",
    "comment":"44444","created_at":"2019 December 18 at 9:31","likes":"0","username":"sam.nob2",
    "firstname":"sam","lastname":"nob2","profile_picture_url":null},{"comment_id":4,"post_id":12,
    "picture_url":"","comment":"44444","created_at":"2019 December 18 at 9:31","likes":"0",
    "username":"sam.nob2","firstname":"sam","lastname":"nob2","profile_picture_url":null},
    {"comment_id":6,"post_id":12,"picture_url":"","comment":"333333","created_at":"2019 December 18 at 14:37",
    "likes":"0","username":"sam.nob","firstname":"sam","lastname":"nob",
    "profile_picture_url":"/user_profile/bf6e7f92-09fd-4a54-9f66-34d1359593ec1811201914116982images.jpg"}]}


    ## 6.13 showUserFeedPost
* คำอธิบาย :  แสดง post ของตัวเองในหน้า timeline
* url : http://localhost:3001/api/post/show_user_feed_post 
* method : post
* input  : {  

           start_row: '',  // คือ แถวที่เริ่มต้น ค่าเริ่มต้นตั้งแต่ 1 ขึ้นไป 
           per_page:''  // คือจะให้แสดงข้อมูลจำนวนกี่แถว กรณีใส่ per_page เกินจำนวนแถวที่มี ไม่error apiจะเปลี่ยนเป็นจำนวนแถวที่เหลือให้เอง
        }

  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {"status":"not found"} 
  * status 200
    * {"row_number":2,"data":[{"post_id":26,"picture_url":"/feed_post/65b3a1a5-f7bd-4a92-b4b2-0b117c76517823112019214059478b.jpg",
"caption":"hgghfghfghfghfghfghf","created_at":"2019 December 23 at 21:40","likes":"0","privacy":"public",
"username":"c.c","firstname":"c","lastname":"c","profile_picture_url":null}],"comment":[{"post_id":26,"comment_number":0}]}
// row_number คือ จำนวนแถวของข้อมูลใน database ทั้งหมด


## 6.14 likePost
* คำอธิบาย : ใช้ เมื่อกด like/unlike post 
* url : http://localhost:3001/api/post/like_post
* method : post
* input  : {  
               
                post_id:'' 
                status:'like' // ส่งได้2ค่า คือ like กับ unlike 
            } 

  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {"status":"not found input"} // ใส่ข้อมูลมาไม่ครบนะ
    * {"status":"wrong status"} // ใส่ค่า status มาผิด ต้องเป็น like หรือ unlike เท่านั้น  
    * {"status":"not found post"} // ใส่ post_id มาผิด หรือไม่มีในdatabase
    * {"status":"already like"} //  มีข้อมูลการกด like ใน database แล้ว ทำให้ไม่สามารถกดlike ได้อีก
    *  {"status":"can not unlike"} // ไม่มีข้อมูลการกด like ใน datase ทำให้ไม่สามารถกด unlike
  * status 200
    * {"status":"like","message":"insert new row in database"} // กดlike สำเร็จ และเพิ่มข้อมูลการกด like ลง database 
    * {"status":"unlike","message":"delete row in database"} // กด unlikeสำเร็จ และลบข้อมูลการกดlike ในระบบ


 ## 6.15 likeComment
* คำอธิบาย : ใช้ เมื่อกด like/unlike comment  
* url : http://localhost:3001/api/post/like_comment
* method : post
* input  : {  
                
                comment_id:'' 
                status:'like' // ส่งได้2ค่า คือ like กับ unlike 
            } 

  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {"status":"not found input"} // ใส่ข้อมูลมาไม่ครบนะ
    * {"status":"wrong status"} // ใส่ค่า status มาผิด ต้องเป็น like หรือ unlike เท่านั้น
    * {"status":"not found comment"} // ใส่ comment_id มาผิด หรือไม่มีในdatabase
    * {"status":"already like"} //  มีข้อมูลการกด like ใน database แล้ว ทำให้ไม่สามารถกดlike ได้อีก
    * {"status":"can not unlike"} // ไม่มีข้อมูลการกด like ใน datase ทำให้ไม่สามารถกด unlike
  * status 200
    * {"status":"like","message":"insert new row in database"} // กดlike สำเร็จ และเพิ่มข้อมูลการกด like ลง database
    * {"status":"unlike","message":"delete row in database"} // กด unlikeสำเร็จ และลบข้อมูลการกดlike ในระบบ


## 7. friend
## 7.1 checkFriendStatus
* คำอธิบาย :  ใช้เช็คว่าเป็นเพื่อนกันไหม
* url : http://localhost:3001/api/post/check_friend_status
* method : post
* input  : {  

        username:'' //username ของคนที่เราเข้าไปดู
       
        }

* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found target username'}
    * {"status":"can not put yourself username"}
  * status 200
    * {status: 'friend'} //เป็นเพื่อนกันแล้ว  
    * {status: 'requesting'} //เราขอเป็นเพื่อนไปแล้ว
    * {status: 'pending'} //มีคนกดขอเราเป็นเพื่อน
    * {status: 'none'} //ไม่พบข้อมูล request และ ไม่เป็นเพื่อนกัน

## 7.2 checkMutualFriend
* คำอธิบาย :  ใช้เช็คว่ามีเพื่อนที่เหมือนกันไหม
* url : http://localhost:3001/api/post/check_mutual_friend
* method : post
* input  : { 

        username:'' //username ของคนที่เราเข้าไปดู
        start_row: '',  // คือ แถวที่เริ่มต้น ค่าเริ่มต้นตั้งแต่ 1 ขึ้นไป 
       per_page: ''// คือจะให้แสดงข้อมูลจำนวนกี่แถว กรณีใส่ per_page เกินจำนวนแถวที่่มี ไม่error apiจะเปลี่ยนเป็นจำนวนแถวที่เหลือให้เอง
       }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found target username'}
    * {"status":"can not put yourself username"}
  * status 200
    * {"mutual_friend":"no","message":"you not have mutual friend"} //ไม่มีเพื่อนที่เหมือนกัน
    * {"mutual_friend":"yes","mutual_friend_number":4,"mutual_friend_data":[{"username":"sam.nob","firstname":"sam",
"lastname":"nob","profile_picture_url":"/user_profile/a616cfd4-a518-45a5-b445-6e541c99f36623112019214334861.jpg"},
{"username":"b.b","firstname":"b","lastname":"b",
"profile_picture_url":"/user_profile/697f8f9f-a75d-4217-b703-59425d92b123231120192145401151.jpg"},
{"username":"sam.c","firstname":"sam","lastname":"c",
"profile_picture_url":"/user_profile/803fb12f-6622-4ba5-8f78-0c0512f3c7242511201910248147ดาวน์โหลด.jpg"}
,{"username":"c.c","firstname":"c","lastname":"c","profile_picture_url":null}]} 

## 7.3 ShowFriendResponse
* คำอธิบาย :  ดึงรายชื่อเพื่อนที่ส่งคำขอมา เพื่อตอบรับเพื่อน , fetch มา แล้วทำปุ่ม confirm/delete 
* url : http://localhost:3001/api/post/show_friend_response
* method : post
* input  : {
           
           row_start:'', //แถวที่เริ่มต้น มีค่าเริ่มต้น = 1
           per_page: '', //จำนวนข้อมูลที่ให้แสดงต่อหน้า ถ้าใส่เกินจำนวนแถวข้อมูล apiจะคำนวณให้แสดงตามจำนวนแถวที่เหลือ ใส่เกินไม่เป็๋นไร 
         }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'please in put all data'} // ใส่ข้อมูลไม่ครบ
    * {status: 'no request'} // ไม่มีใครขอแอดเป็นเพื่อน
    * {status: 'wrong row start'} // ใส่ row_start ผิด
  * status 200
    * {"row_number":3,"data":[{"username":"sam.nob2","firstname":"sam","lastname":"nob2",
   "profile_picture_url":"http://localhost:3001/user_profile/a980dbdf-a5b6-4a41-9bad-db4651f302ed10112019102344458images.jpg"},
   {"username":"a.a","firstname":"a","lastname":"a","profile_picture_url":null},{"username":"b.b","firstname":"b",
   "lastname":"b","profile_picture_url":null}]}


## 7.4 friendResponse
* คำอธิบาย :  เมื่อกดปุ่ม confirm/delete เพื่อรับเป็นเพื่อน หรือ ลบrequest
* url : http://localhost:3001/api/post/friend_response
* method : post
* input  : { 

             request_username:'', // username ของคนที่ส่ง request มาหาเรา (จากapiด้านบน ข้อ1)
             status:'confirm' // มี2อย่าง 1. confirm คือ รับเป็นเพื่อน 2. delete คือ ไม่รับเป็นเพื่อน  ห้ามใส่ผิด
           }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'please input all data'} // ใส่ข้อมูลไม่ครบ
    * {status: 'already friend'} // เป็นเพื่อนกันแล้ว
    * {status: 'wrong status'} //ใส่ status ผิด
    * {status: ' username not found'} // ไม่มี username ของคนที่เราจะกด confirm/delete ใน database
    * {status : 'no request'} //ไม่สามารถ confirm/delete เนื่องจากไม่มีrequest ใน database
  * status 200
    * {status:' request confirmed'}  // แอดเพื่อนสำเร็จ
    * {status: 'request deleted'} // ไม่รับเป็นเพื่อน ลบrequest สำเร็จ

## 7.5 friendList
* คำอธิบาย : แสดงรายชื่อเพื่อนของเรา  
* url : http://localhost:3001/api/post/friend_list
* method : post
* input  : {

          row_start:'', //แถวเริ่มต้น ค่าเริ่มต้น =1
          per_page:'', // จำนวนที่ให้แสดงต่อหน้า ถ้าใส่เกินจำนวนแถวของข้อมูล apiจะคำนวณให้เอง แสดงจำนวนตามแถวที่เหลือ
       }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status : 'please in put all data'} // ใส่ข้อมูลำไม่ครบ
    * { status : 'wrong row start'} // ใส่ row_start ผิด
    * { status: 'no friend'} // ไม่มีเพื่อน
  * status 200
    * {"row_number":2,"data":[{"username":"sam.nob","firstname":"sam","lastname":"nob",
 "profile_picture_url":"http://localhost:3001/user_profile/18834da5-0ba2-4bb8-8295-04cde0c1f0cf4112019132945833images.png"},
 {"username":"sam.c","firstname":"sam","lastname":"c","profile_picture_url":null}]} 


## 7.6 friendRecommend
* คำอธิบาย :   ใช้ดึงรายชื่อเพื่อน ที่แนะนำให้แอด (ต้องไม่เป็นเพื่อนกับเรา และ ไม่ได้ส่งคำขอเป็นเพื่อนกัน) , พอ fetchค่ามา ให้ทำปุ่ม add เพื่อนด้วย
* url : http://localhost:3001/api/post/friend_recommend
* method : post
* input  : {

             row_start:'', // แถวที่เริ่มต้น ค่าเริ่มต้น = 1
             per_page:'', // จำนวนที่ให้แสดงต่อหน้า ถ้าใส่เกินจำนวนแถวของข้อมูล apiจะคำนวณให้เอง แสดงจำนวนตามแถวที่เหลือ
            }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'please in put all data'} // ใส่ข้อมูลไม่ครบ
    * {status: 'wrong row start'} // ใส่แถมเริ่มต้นผิด
    * {status: 'no friend recommend'} // ไม่มีเพื่อนให้แอดแล้ว เพราะ มีrequestแล้ว และ เป็นเพื่อนไป หมดแล้ว
  * status 200
    * {"row_nuber":3,"data":[{"username":"c.c","firstname":"c","lastname":"c","profile_picture_url":null},
  {"username":"b.b","firstname":"b","lastname":"b","profile_picture_url":null},{"username":"a.a","firstname":"a","lastname":"a",
   "profile_picture_url":null}]}  // แนะนำเพื่อน มาให้แอด



## 7.7 showFriendRequest
* คำอธิบาย :  ดึงค่าuser ที่เราเคยส่งคำขอแอดเพื่อน และเขายังไม่รับแอดเรา , พอ fetch ค่ามา ให้ทำปุ่ม cancel เพื่อยกเลิกคำขอเป็นเพื่อนได้ 
* url : http://localhost:3001/api/post/show_friend_request
* method : post
* input  : {
           
             row_start:'', // แถวที่เริ่มต้น ค่าเริ่มต้น = 1
             per_page:'', // จำนวนที่ให้แสดงต่อหน้า ถ้าใส่เกินจำนวนแถวของข้อมูล apiจะคำนวณให้เอง แสดงจำนวนตามแถวที่เหลือ
            }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'please in put all data'} ใส่ข้อมูลไม่ครบ 
    * {status: 'wrong row start'} // ใส่แถมเริ่มต้นผิด
    * {status: 'no request'} // ไม่มี show_friend_request
  * status 200
    * {"row_number":2,"data":[{"username":"sam.c2.6112019175412739","firstname":"sam","lastname":"c2","profile_picture_url":null},
     {"username":"sam.c2","firstname":"sam","lastname":"c2",
     "profile_picture_url":"http://localhost:3001/user_profile/d889f614-e2a4-41bd-aa8e-0f469bdc7b0a10112019134941801b.jpg"}]}


## 7.8 friendRequest
* คำอธิบาย :    add request /delete request ใน database
* url : http://localhost:3001/api/post/friend_request
* method : post
* input  : {   

                    response_username // usernameของที่จะเรา add หรือ cancel request 
                    status : 'add' // มี2อย่าง 1. add คือ กดแอดเพื่อน 2. cancel คือยกเลิกแอดเพื่อน
                }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'please input all data'} // ใส่ข้อมูลไม่ครบ
    * {status: 'input wrong status'}  // ใส่ status ผิด
    * {status: 'not found user'}  //ไม่มี usernameของคนนี้ ในระบบ
    * {status: 'can not put self username'} //ห้ามใส่ username ตัวเอง
    * {status: 'this user alreay friend'} //เป็นเพื่อนกันแล้ว
    * {status: 'another user already request to you'}  // userที่เราจะ add, เขาได้ add มาหาเราแล้ว
    * {status: ' alreay request'} // เราเคย add ไปแล้ว
    * {status: 'no request'} //ไม่ข้อมูลในระบบ ไม่สามารถ cancel ไม่ได้ส่งคำขอเป็นเพื่อนกัน
  * status 200
    * {status: 'request canceled'} // ยกเลิก  request สำเร็จ
    * {status: 'request sent'}  // ส่ง requestสำเร็จ 


## 7.9 removeFriend
* คำอธิบาย :  ใช้ลบเพื่อน  
* url : http://localhost:3001/api/post/remove_friend
* method : post
* input  :  { 

        username:'' //username ของเพื่อน
        
       }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found friend username'} //ไม่พบ username ของเพื่อน
    * {status: 'this username is not your friend'} // ไม่ได้เป็นเพื่อนกัน
  * status 200
    * {status : 'remove friend complete'}  //ลบเพื่อนสำเร็จ


## 8. Notification   
## 8.1 showNotification
* คำอธิบาย : ใช้แสดงรายการ notification
* url :  http://localhost:3001/api/post/show_notification
* method : post
* input  : {   
           start_row: '',  // คือ แถวที่เริ่มต้น ค่าเริ่มต้นตั้งแต่ 1 ขึ้นไป 
           per_page:''  // คือจะให้แสดงข้อมูลจำนวนกี่แถว กรณีใส่ per_page เกินจำนวนแถวที่มี ไม่error apiจะเปลี่ยนเป็นจำนวนแถวที่เหลือให้เอง
        }

  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found notification'}  
  * status 200
    * {"my_user_id":48,"notification_number":5,
"row_number":6,"data":[{"id":42,"post_owner_user_id":48,"type":"like_comment","post_id":52,"comment_id":47,
"read_status":null,"created_at":"1579514031245","username":"99.999","firstname":"99","lastname":"999",
"profile_picture_url":"/user_profile/3a0c3840-ff32-4c9d-be3a-0a4594359e7f15786743148281.jpg"}]}


## 8.2 updateNotificationNumber
* คำอธิบาย :   เมื่อกดที่ icon notification(ลูกโลก หรือ กระดิ่ง) จะยิงมาที่apiนี้ เพื่อให้ตัวเลขสีแดงที่อยู่บนicon notification มีค่าเป็น 0 
* url : http://localhost:3001/api/post/update_notification_number
* method : post
* input  : {}
* output
  * status 404
    * {session:'not found session'}
  * status 200
    * {status:'no update notification number is 0 '} //ไม่อัพเดท เพราะ ตัวเลขสีแดงบน nofication เป็น 0 อยู่แล้ว
    * {status: 'notification number update complete'} 


## 8.3 updateReadStatus
* คำอธิบาย :  เมื่อเข้าไปดูข้อมูล notification จะ update ว่าอ่านแล้ว
* url : http://localhost:3001/api/post/update_read_status
* method : post
* input  : input: { 


         notifaction_id // id nofication ที่มาจาก api showNofication
        }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found notification'}
  * status 200
    * status:'no update read status is yes'} //ไม่อัพเดท เพราะ ได้อ่านไปแล้ว
    * {status: 'read status update complete'}  


## 9. setting
## 9.1 setting_feed
## 9.1.1 postFeedPostSetting
* คำอธิบาย :  save ข้อมูลการตั้งค่า privacy ของ post ลงใน database
* url : http://localhost:3001/api/post/post_feed_post_setting
* method : post
* input  :{ 
            
            privacy:'' //ต้องมีค่าเป็น public, friend, only me เท่านั้น
        }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {"status":"wrong privacy, please input public, friend, only me"}
  * status 200
    * {"status":"setting feed post complete"}


## 9.1.2 showFeedPostSetting
* คำอธิบาย :   ตรงนี้จะอยู่ในส่วนของ menu setting, ดึงค่าpriacy ที่setting ไว้มาโชว์ ว่า post ในอนาคตจะให้ใครเข้าดูได้บ้าง 
* url :  http://localhost:3001/api/post/show_feed_post_setting
* method : post
* input  : { }
  
* output
  * status 404
    * {session:'not found session'}
  * status 200
    * {"privacy":"public"} // **หมายเหตุ ถ้าไม่พบข้อมูลใน database จะมีค่า default เป็น public 


## 9.2 setting_friend
## 9.2.1 postFriendSetting
* คำอธิบาย :  save ข้อมูลการตั้งค่า privacy ของ friend list ลง database
* url :  http://localhost:3001/api/post/post_friend_list_setting
* method : post
* input  : {  

           privacy:'only me' //มีได้3ค่า คือ public, friend, only me
           }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {"status":" wrong privacy, please input public, friend, only me"}
  * status 200
    * {"status":"setting friend list privacy complete"}

## 9.2.2 showFriendListSetting
* คำอธิบาย :  แสดงค่า privacy (public, friend, only me) ที่settingไว้ว่า จะอนุญาตให้ใครดู friend list ของเราได้บ้าง , ถ้าไม่มีข้อมูลใน database จะมีค่าdefault เป็นpublic
* url : http://localhost:3001/api/post/show_friend_list_setting
* method : post
* input  : { }
  
* output
  * status 404
    * session:'not found session'}
  * status 200
    * {"privacy":"only me"}


## 9.3 setting_profile
## 9.3.1 postProfileSetting
* คำอธิบาย :  ตั้งค่า privacy (public, friend, only me) profile ,save ลงdatabase ,สามารถเลือก setting ได้ทีละค่า (ไม่ต้องส่งinputไปทั้งหมดก็ได้)
* url : http://localhost:3001/api/post/post_profile_setting
* method : post
* input  : { 
          
           //ค่าที่ใส่ public, friend, only me
         bio:'public', 
         lives:'public',
         come_from:'friend',
         work_place:'friend'
         education : 'only me'
         relationship_status: 'only me'
         hobbies: 'only me',
         birthday: 'only me',
         email: 'only me' 
        }
  
* output
  * status 404
    * {session:'not found session'}
  * status 200
    * {status: 'setting privacy complete'} 


## 9.3.2 showProfileSetting 
* คำอธิบาย : show ค่า setting privacy(public, friend, only me) ของ profile ที่เราได้ตั้งค่าไว้ , ถ้าไม่ได้ตั้งค่าไว้ มีค่า dealfault คือ public  
* url :  http://localhost:3001/api/post/show_profile_setting
* method : post
* input  : { }
  
* output
  * status 404
    * {session:'not found session'}
  * status 200
    * {"bio":"public","lives":"public","come_from":"public","work_place":"public","education":"public",
    "relationship_status":"public","hobbies":"public","birthday":"public","email":"public"}


## 10. user_profile
## 10.1 editProfile
* คำอธิบาย :  save ข้อมูล profile ลง database
* url : http://localhost:3001/api/post/edit_profile 
* method : post
* input  : {

            //สามารถแก้ได้ทีละค่า ไม่ต้องส่งค่ามาทั้งหมดก็ได้
            profile_picture:'ใส่รูปโปรไฟล์.jpg', cover_picture:'ใส่รูปหน้าปก.jpg',
            bio:'ข้อความแนะนำตัว', 
            lives:'ที่อยู่', 
            come_from:'มาจากไหน', 
            work_place:'ที่ทำงาน', 
            education:'เรียนไหน',
            relationship_status: 'โสด', 
            hobbies: 'ดูบอล'    
           } 
  
* output
  * status 404
    * {session:'not found session'}
  * status 200
    * {"status":"update profile complete"}


## 10.2 showEditProfile
* คำอธิบาย :  แสดงข้อมูล profile ล่าสุด ที่หน้าedit profile
* url :  http://localhost:3001/api/post/show_edit_profile
* method : post
* input  : {  }
  
* output
  * status 404
    * {session:'not found session'}
  * status 200
    * {"profile_picture_url":"http://localhost:3001/user_profile/2b2c8338-0476-4154-9b0c-f8a9940020ef281020191840193351.jpg",
"cover_picture_url":"http://localhost:3001/user_cover/086c97cc-4dc8-47ca-b6ec-2966ac979867281020191840193352.jpg",
"bio":"ข้อความแนะนำตัวนะครับ","lives":"อยู่ที่ชอบ","come_from":"","work_place":"ทำงานที่ไหนก็ได้","education":"เรียนที่โลก",
"relationship_status":"โสด","hobbies":"ว่ายน้ำ"}

## 11 view
## 11.1 view_album
## 11.1.1 viewCoverPicture
* คำอธิบาย :  ดู album รูป cover ของคนอื่น
* url :  http://localhost:3001/api/get/view_cover_picture/:username/:start_row/:per_page
* method : get
* input  : 

           :username'', // username ของคนที่เราจะดู
           :start_row:'', //แถวเริ่มต้นของข้อมูล มีค่าเริ่มต้นเป็น 1
           :per_page : '' // จำนวนข้อมูลที่ให้แสดงต่อหน้า
  
* output
  
  * status 400
    * {"status":"not found username"}
    * {status: 'wrong row start. It begin with 1'}
    * {status: 'not found cover picture'}
  * status 200
    * {"row_number":2,"current_cover_picture_url":"/user_cover/1e7b53c3-aa47-45ea-a25d-3ec5ad6c0d8a23112019214334862.jpg",
"cover_picture_album":[{"picture_id":2,"username":"sam.nob","firstname":"sam","lastname":"nob",
"cover_picture_url":"/user_cover/1e7b53c3-aa47-45ea-a25d-3ec5ad6c0d8a23112019214334862.jpg",
"created_at":"2019 December 23 at 21:43"},{"picture_id":1,"username":"sam.nob","firstname":"sam",
"lastname":"nob","cover_picture_url":"/user_cover/4d9af45c-57b9-424d-ab56-83832a3aef3d231120192142323682.jpg",
"created_at":"2019 December 23 at 21:42"}]}


## 11.1.2 viewPostPicture
* คำอธิบาย :  ดู album รูป post บน timeline ของคนอื่น ที่มีprivacy เป็น public
* url : http://localhost:3001/api/get/view_post_picture/:username/:start_row/:per_page
* method : get
* input  : 
          
        :username'', // username ของคนที่เราจะดู
        :start_row:'', //แถวเริ่มต้นของข้อมูล มีค่าเริ่มต้นเป็น 1
        :per_page : '' // จำนวนข้อมูลที่ให้แสดงต่อหน้า
  
* output
  * status 400
    * {"status":"not found target username"}
    * {status: 'wrong row start. It begin with 1'}
    * {"status":"not found post picture "}
  * status 200
    * {"row_number":2,
"lastest_post_picture_url":"/feed_post/44d8b72d-9751-41f3-acb9-f234615e813e31112019134019470shot(10-05-19)15;36;56.jpg",
"post_picture_album"
:[{"post_id":36,"picture_url":"/feed_post/44d8b72d-9751-41f3-acb9-f234615e813e31112019134019470shot(10-05-19)15;36;56.jpg",
"caption":"","created_at":"2019 December 31 at 13:40","likes":"0","privacy":"public","username":"sam.nob","firstname":"sam",
"lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":31,"picture_url":"/feed_post/20ed92a2-9680-4822-9fd9-58d12bc936d531112019133827272shot(10-05-19)15;36;56.jpg",
"caption":"dsaasd","created_at":"2019 December 31 at 13:38","likes":"0","privacy":"public","username":"sam.nob",
"firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"}],
"comment":[{"post_id":36,"comment_number":0},{"post_id":31,"comment_number":2}]}


## 11.1.3 viewPrivatePostPicture
* คำอธิบาย :  เข้าไปดู รูปpost บน timeline ของคนอื่น ได้ตาม privacy ที่กำหนด (public, friend, only me )
* url : http://localhost:3001/api/post/view_private_post_picture
* method : post
* input  : { 

         username:'', // username ของคนที่เราจะดู
         start_row:'', //  แถวเริ่มต้นของข้อมูล เริ่มต้นที่1
         per_page //  จำนวนข้อมูลที่แสดงต่อหน้า
          }

  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found target username'} //ไม่พบ username ของคนที่เราจะดู
    * {status: 'can not put yourself username'}
    * {"status":"not found post picture"}
  * status 200
    * {"row_number":4,
"lastest_post_picture_url":"/feed_post/77271926-05e2-46c0-8805-7a3aa16dd0e831112019134023390shot(10-05-19)15;36;56.jpg",
"post_picture_album":[{"post_id":37,
"picture_url":"/feed_post/77271926-05e2-46c0-8805-7a3aa16dd0e831112019134023390shot(10-05-19)15;36;56.jpg",
"caption":"","created_at":"2019 December 31 at 13:40","likes":"0","privacy":"friend",
"username":"sam.nob",
"firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":36,"picture_url":"/feed_post/44d8b72d-9751-41f3-acb9-f234615e813e31112019134019470shot(10-05-19)15;36;56.jpg",
"caption":"","created_at":"2019 December 31 at 13:40","likes":"0","privacy":"public","username":"sam.nob","firstname":"sam",
"lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":31,"picture_url":"/feed_post/20ed92a2-9680-4822-9fd9-58d12bc936d531112019133827272shot(10-05-19)15;36;56.jpg",
"caption":"dsaasd","created_at":"2019 December 31 at 13:38","likes":"0","privacy":"public","username":"sam.nob",
"firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":30,"picture_url":"/feed_post/8b29d028-eec8-4f01-9cff-db6684b0350e31112019133817540shot(10-05-19)15;36;56.jpg",
"caption":"dsaasd","created_at":"2019 December 31 at 13:38","likes":"0","privacy":"friend","username":"sam.nob",
"firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"}],
"comment":[{"post_id":37,"comment_number":0},{"post_id":36,"comment_number":0},{"post_id":31,"comment_number":2},
{"post_id":30,"comment_number":3}]}


## 11.1.4 viewProfilePicture
* คำอธิบาย :  ดู album รูป profile ของคนอื่น
* url : http://localhost:3001/api/get/view_profile_picture/:username/:start_row/:per_page
* method : get
* input  : 
           :username'', // username ของคนที่เราจะดู
           :start_row:'', //แถวเริ่มต้นของข้อมูล มีค่าเริ่มต้นเป็น 1
           :per_page : '' // จำนวนข้อมูลที่ให้แสดงต่อหน้า
  
* output
  * status 400
    * {"status":"not found username"}
    * {status: 'wrong row start. It begin with 1'}
    * {status: 'not found profile picture'}
  * status 200
    * {"row_number":4,
"current_profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg",
"profile_picture_album":[{"picture_id":12,"username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg",
"created_at":"2019 December 29 at 9:36"},{"picture_id":11,"username":"sam.nob","firstname":"sam",
"lastname":"nob",
"profile_picture_url":"/user_profile/0c8176f1-676d-4d3d-a76c-4f5ea385faef2911201993617879shot(10-05-19)15;36;56.jpg",
"created_at":"2019 December 29 at 9:36"},{"picture_id":6,"username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/a616cfd4-a518-45a5-b445-6e541c99f36623112019214334861.jpg",
"created_at":"2019 December 23 at 21:43"},{"picture_id":5,"username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/0693f358-c150-4888-87c4-615d7d54d89d231120192142323681.jpg",
"created_at":"2019 December 23 at 21:42"}]}   


## 11.2 view_feed
## 11.2.1 viewPost
* คำอธิบาย :  เข้ามาดู post ของคนอื่น ได้เฉพาะอันที่ตั้ง privacy เป็น public เท่านั้น
* url : http://localhost:3001/api/get/view_post/:username/:start_row/:per_page
* method : get
* input  : 

           :username คือ username ของคนที่เราจะดู
           :start_row คือ แถวที่เริ่มต้นของข้อมูล, มีค่าเริ่มต้นที่ 1
           :per_page คือ จำนวนข้อมูลที่แสดงต่อหน้า 
  
* output
  
  * status 400
    * {status: 'please input username'}
    * {status: 'not found username'}
    * {status: 'not found post'}
  * status 200
    * {"row_number":5,"data":[{"post_id":36,
"picture_url":"/feed_post/44d8b72d-9751-41f3-acb9-f234615e813e31112019134019470shot(10-05-19)15;36;56.jpg","caption":"",
"created_at":"2019 December 31 at 13:40","likes":"0","privacy":"public","username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},{"post_id":33,
"picture_url":"","caption":"1","created_at":"2019 December 31 at 13:39","likes":"0","privacy":"public","username":"sam.nob",
"firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":31,"picture_url":"/feed_post/20ed92a2-9680-4822-9fd9-58d12bc936d531112019133827272shot(10-05-19)15;36;56.jpg",
"caption":"dsaasd","created_at":"2019 December 31 at 13:38","likes":"0","privacy":"public","username":"sam.nob","firstname":"sam",
"lastname":"nob","profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":27,"picture_url":"","caption":"5555","created_at":"2019 December 29 at 9:35","likes":"0","privacy":"public",
"username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":24,"picture_url":"","caption":"","created_at":"","likes":"","privacy":"public","username":"sam.nob",
"firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"}],
"comment":[{"post_id":36,"comment_number":0},{"post_id":33,"comment_number":0},{"post_id":31,"comment_number":2},
{"post_id":27,"comment_number":0},{"post_id":24,"comment_number":0}]}


## 11.2.2 viewPrivatePost
* คำอธิบาย : login เข้ามาดู post ของคนอื่น, ถ้าเป็นเพื่อนกัน ดูpost ที่มี privacy เป็น public, friend ได้, ถ้าไม่เป็นเพื่อนกันดูได้แค่  post ที่มี privacy เป็น public 
* url : http://localhost:3001/api/post/view_private_post
* method : post
* input  : { 


             username :'', //username ของคนที่เราจะดู
            start_row :'', // แถวที่เริ่มต้นของข้อมูล, มีค่าเริ่มต้นที่ 1
            per_page:''    // จำนวนข้อมูลที่แสดงต่อหน้า 
         }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found target username'}
    * {status:'can not view yourself'}
    * {status: 'wrong row start. It begin with 1'} 
    * {status: 'not found post'}
  * status 200
    * {"row_number":8,"data":[{"post_id":37,
"picture_url":"/feed_post/77271926-05e2-46c0-8805-7a3aa16dd0e831112019134023390shot(10-05-19)15;36;56.jpg"
,"caption":"","created_at":"2019 December 31 at 13:40","likes":"0","privacy":"friend","username":"sam.nob",
"firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":36,"picture_url":"/feed_post/44d8b72d-9751-41f3-acb9-f234615e813e31112019134019470shot(10-05-19)15;36;56.jpg",
"caption":"","created_at":"2019 December 31 at 13:40","likes":"0","privacy":"public","username":"sam.nob","firstname":"sam",
"lastname":"nob","profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":34,"picture_url":"","caption":"2","created_at":"2019 December 31 at 13:39","likes":"0","privacy":"friend",
"username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":33,"picture_url":"","caption":"1","created_at":"2019 December 31 at 13:39","likes":"0",
"privacy":"public","username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":31,"picture_url":"/feed_post/20ed92a2-9680-4822-9fd9-58d12bc936d531112019133827272shot(10-05-19)15;36;56.jpg",
"caption":"dsaasd","created_at":"2019 December 31 at 13:38","likes":"0","privacy":"public","username":"sam.nob",
"firstname":"sam","lastname":"nob",
profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":30,"picture_url":"/feed_post/8b29d028-eec8-4f01-9cff-db6684b0350e31112019133817540shot(10-05-19)15;36;56.jpg",
"caption":"dsaasd","created_at":"2019 December 31 at 13:38","likes":"0","privacy":"friend","username":"sam.nob",
"firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":27,"picture_url":"","caption":"5555","created_at":"2019 December 29 at 9:35","likes":"0",
"privacy":"public","username":"sam.nob","firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"},
{"post_id":24,"picture_url":"","caption":"","created_at":"","likes":"","privacy":"public","username":"sam.nob",
"firstname":"sam","lastname":"nob",
"profile_picture_url":"/user_profile/821b2eb1-ca03-469a-9e4c-b39c5a0e27e0291120199364448shot(10-05-19)18;44;16.jpg"}],
"comment":[{"post_id":37,"comment_number":0},{"post_id":36,"comment_number":0},{"post_id":34,"comment_number":0},
{"post_id":33,"comment_number":0},{"post_id":31,"comment_number":2},{"post_id":30,"comment_number":3},
{"post_id":27,"comment_number":0},{"post_id":24,"comment_number":0}]}


## 11.2.3 viewPrivatePostById
* คำอธิบาย : ดู post ของคนอื่น post ตาม post_id และ privacy ที่กำหนด
* url : http://localhost:3001/api/post/view_private_post_by_id
* method : post
* input  : {

            post_id, // post_id ของคนอื่น , ถ้าเป็นของเราจะดูไม่ได้
           }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found post'}
    * {status:'can not view yourself'}
  * status 200
    * {"data":[{"post_id":57,"picture_url":"/feed_post/3aeb9d87-1076-4bfd-9f40-06e04807b13615795105137041.jpg",
"caption":"","created_at":"1579510513704","likes":"0","privacy":"friend","username":"sam.nob2",
"firstname":"sam","lastname":"nob2","profile_picture_url":null}],"comment":[{"post_id":57,"comment_number":0}]}


## 11.3 view_friend
## 11.3.1 viewFriendList
* คำอธิบาย :  ดูรายชื่อเพื่อนคนอื่น แบบ public (ถ้าprivacy เป็น only me ,friend ดูไม่ได้)
* url :  http://localhost:3001/api/get/view_friend_list/:username/:start_row/:per_page
* method : get
* input  : 

           :username  คือ username ของคนที่เราจะดู
           :start_row  คือ แถวเริ่มต้นของข้อมูล เริ่มต้นที่1
           :per_page  คือ จำนวนข้อมูลที่แสดงต่อหน้า
  
* output
  * status 400
    * {status: 'not found target username'} //ไม่พบ username ของคนที่เราจะดู
    * {status: 'not found friend list'} // เข้าดูไม่ได้ เนื่องจาก privacy ไม่ใช่ public
    * {status : 'please in put all data'}
    *  { status: 'no friend'} // username ที่เราเข้าไปดู ไม่มีเพื่อนเลย
  * status 200
    * {"row_number":5,"data":[{"username":"22.22","firstname":"22","lastname":"22","profile_picture_url":null},
{"username":"b.b","firstname":"b","lastname":"b",
"profile_picture_url":"/user_profile/697f8f9f-a75d-4217-b703-59425d92b123231120192145401151.jpg"},
{"username":"sam.c","firstname":"sam","lastname":"c",
"profile_picture_url":"/user_profile/803fb12f-6622-4ba5-8f78-0c0512f3c7242511201910248147ดาวน์โหลด.jpg"}]}


## 11.3.2 viewPrivateFriendList
* คำอธิบาย :  เข้าไปดู รายชื่อเพื่อนของคนอื่น ได้ตาม privacy ที่กำหนด (public, friend, only me )
* url :  http://localhost:3001/api/post/view_private_friend_list
* method : post
* input  : { 
  
            username:'', // username ของคนที่เราจะดู
            start_row:'', //  แถวเริ่มต้นของข้อมูล เริ่มต้นที่1
            per_page //  จำนวนข้อมูลที่แสดงต่อหน้า
          }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {status: 'not found target username'} //ไม่พบ username ของคนที่เราจะดู
    * {status: 'can not put yourself username'} 
    * {status: 'not found friend list'} // เข้าดูไม่ได้
    * {status : 'please in put all data'}
    * { status: 'no friend'} // username ที่เราเข้าไปดู ไม่มีเพื่อนเลย
  * status 200
    * {"row_number":5,"data":[{"username":"22.22","firstname":"22","lastname":"22","profile_picture_url":null},
{"username":"b.b","firstname":"b","lastname":"b",
"profile_picture_url":"/user_profile/697f8f9f-a75d-4217-b703-59425d92b123231120192145401151.jpg"},
{"username":"sam.c","firstname":"sam","lastname":"c",
"profile_picture_url":"/user_profile/803fb12f-6622-4ba5-8f78-0c0512f3c7242511201910248147ดาวน์โหลด.jpg"}]}


## 11.4 view_profile
## 11.4.1 viewPrivateProfile
* คำอธิบาย : เรา login เข้าไปดู profile ของคนอื่น ได้ตาม privacy ที่กำหนดไว้
* url : http://localhost:3001/api/post/view_private_profile
* method : post
* input  : { 

             username: //คือusername ของคนที่เราจะดู profile 
           }
  
* output
  * status 404
    * {session:'not found session'}
  * status 400
    * {"status":"not found target username"}
    * {status:'can not view yourself'}
  * status 200
    * {"username":"sam.nob","firstname":"sam","lastname":"nob","bio":"","lives":"",
"come_from":"","work_place":"","education":"","relationship_status":"","hobbies":"","birthday":""
,"email":"","gender":"male","profile_picture_url":"/user_profile/1afba9e9-9488-4878-b902-0b194b8967f515786747073741.jpg",
"cover_picture_url":"/user_cover/c9447bcd-ed4f-4a68-a093-0f14af3febcc15786747073742.jpg"}


## 11.4.2 viewProfile
* คำอธิบาย :  ดู profile ของคนอื่น จะดูได้แค่ที่ตั้งค่าไว้เป็น public ,ถ้าเป็น friend, only me , ถ้าดูไม่ได้ จะส่งค่าว่างไปให้แทน
* url :  http://localhost:3001/api/get/view_profile/:username
* method : get
* input  : 

           : username คือ username ของคนที่เราจะเข้าไปดู
  
* output
  
  * status 400
    *  {"status":"not found this username"}
  * status 200
    * {"username":"sam.nob","firstname":"sam","lastname":"nob","bio":"","lives":"",
"come_from":"","work_place":"","education":"","relationship_status":"","hobbies":"","birthday":"","email":"",
"gender":"male","profile_picture_url":"/user_profile/1afba9e9-9488-4878-b902-0b194b8967f515786747073741.jpg",
"cover_picture_url":"/user_cover/c9447bcd-ed4f-4a68-a093-0f14af3febcc15786747073742.jpg"}

